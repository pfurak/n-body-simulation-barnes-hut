#include "RCubeBaseGPU.h"

RCubeBaseGPU::RCubeBaseGPU(const int& number_of_particles, const float& gravitational_constant, const float& theta, const float& lambda, const bool& is_3d, const bool& center_of_mass_logic, const int& device_index) :
BaseGPU(number_of_particles, gravitational_constant, theta, lambda, is_3d, device_index)
{
    this->center_of_mass_logic = center_of_mass_logic;

	gpu_inited = false;
    if(!is_3d)
    {
        blocks_in_row = cbrt(number_of_particles);
	    number_of_blocks = blocks_in_row * blocks_in_row;
		blocks_in_row_pow = number_of_blocks;
		SetAlgorithmName("RCube 2D GPU");
    }
    else
    {
        blocks_in_row = pow(number_of_particles, 1.0 / 6);
        blocks_in_row_pow = blocks_in_row * blocks_in_row;
        number_of_blocks = blocks_in_row * blocks_in_row_pow;
		SetAlgorithmName("RCube 3D GPU");
    }
}

void RCubeBaseGPU::InitGPU(const char* source_code_file, const float& min_x, const float& max_x, const float& min_y, const float& max_y, const float* positions, const float* velocities, const float* masses, const float& min_z, const float& max_z)
{
	std::stringstream ss;
	//ss << "-cl-std=CL1.1";
	ss << " -D NUMBER_OF_BLOCKS=" << number_of_blocks;
	ss << " -D BLOCKS_IN_ROW=" << blocks_in_row;
	ss << " -D BLOCKS_IN_ROW_POW=" << blocks_in_row_pow;

	std::vector<const char*> source_files;
	source_files.push_back(helper_cl);
	source_files.push_back(source_code_file);
	
	if (CreateProgarm(source_files, std::string(ss.str())))
	{
		// Make kernel
		kernel_reset_variables = cl::Kernel(cl_program, "ResetVariables");
		kernel_set_block_neighbors = cl::Kernel(cl_program, "SetBlockNeighbors");
		kernel_set_block_positions = cl::Kernel(cl_program, "SetBlockPositions");
		kernel_set_particles_data = cl::Kernel(cl_program, "SetParticlesData");
		kernel_set_block_center_of_mass = cl::Kernel(cl_program, "SetBlockCenterOfMass");
		kernel_calculate_particle_position_with_center_of_mass = cl::Kernel(cl_program, "CalculateParticlePositionWithCenterOfMass");
		kernel_calculate_particle_position = cl::Kernel(cl_program, "CalculateParticlePosition");
		kernel_calculate_particle_position_with_center_of_mass_via_solver = cl::Kernel(cl_program, "CalculateParticlePositionWithCenterOfMassViaSolver");
		kernel_calculate_particle_position_via_solver = cl::Kernel(cl_program, "CalculateParticlePositionViaSolver");

		SetBaseBuffers(min_x, max_x, min_y, max_y, positions, velocities, masses, min_z, max_z);
		SetCLBuffers();
		SetKernelArguments();

		gpu_inited = true;
	}
	else
	{
		gpu_inited = false;
	}
}

void RCubeBaseGPU::SetCLBuffers()
{
	const int max_neighbors = is_3d ? 6 : 4;

	// ================================================================== //

	// block coordinates
	size_t size = sizeof(float) * (blocks_in_row + 1);
	cl_block_x_positions = cl::Buffer(context, cl_mem_flag, size);
	cl_block_y_positions = cl::Buffer(context, cl_mem_flag, size);

	if (is_3d)
	{
		cl_block_z_positions = cl::Buffer(context, cl_mem_flag, size);
	}

	// ================================================================== //

	// Block - particles relation
	size = sizeof(int) * number_of_particles;
	cl_particle_block_index = cl::Buffer(context, cl_mem_flag, size);

	size = sizeof(int) * number_of_blocks;
	cl_block_particles_size = cl::Buffer(context, cl_mem_flag, size);

	cl_block_particles = cl::Buffer(context, cl_mem_flag, size * number_of_particles);

	// ================================================================== //

	// Center of Mass
	// In 2D: 1 center of mass object has X and Y coordinates and 1 mass
	// In 3D: 1 center of mass object has X, Y and Z coordinates and 1 mass
	size = sizeof(float) * number_of_blocks * (is_3d ? 4 : 3);
	cl_block_center_of_mass = cl::Buffer(context, cl_mem_flag, size);

	// ================================================================== //

	// block neighbors data
	size = sizeof(int) * number_of_blocks;
	// In 2D, 1 block only have 4 neighbors: 1 left, 1 up, 1 right, 1 down
	// In 3D, 1 block only have 6 neighbors: 1 left, 1 up, 1 right, 1 down, 1 front, 1 behind
	cl_block_neighbors_index = cl::Buffer(context, cl_mem_flag, size * max_neighbors);
	cl_block_neighbors_size = cl::Buffer(context, cl_mem_flag, size);
}

void RCubeBaseGPU::SetKernelArguments()
{
	kernel_set_block_neighbors.setArg(0, cl_block_neighbors_index);
	kernel_set_block_neighbors.setArg(1, cl_block_neighbors_size);

	// ================================================================== //

	kernel_reset_variables.setArg(0, cl_block_particles_size);

	// ================================================================== //

	kernel_set_block_positions.setArg(0, cl_min_x);
	kernel_set_block_positions.setArg(1, cl_max_x);
	kernel_set_block_positions.setArg(2, cl_min_y);
	kernel_set_block_positions.setArg(3, cl_max_y);
	kernel_set_block_positions.setArg(4, cl_block_x_positions);
	kernel_set_block_positions.setArg(5, cl_block_y_positions);
	if (is_3d)
	{
		kernel_set_block_positions.setArg(6, cl_min_z);
		kernel_set_block_positions.setArg(7, cl_max_z);
		kernel_set_block_positions.setArg(8, cl_block_z_positions);
	}

	// ================================================================== //

	kernel_set_particles_data.setArg(0, cl_positions);
	kernel_set_particles_data.setArg(1, cl_block_x_positions);
	kernel_set_particles_data.setArg(2, cl_block_y_positions);
	kernel_set_particles_data.setArg(3, cl_block_particles);
	kernel_set_particles_data.setArg(4, cl_block_particles_size);
	kernel_set_particles_data.setArg(5, cl_particle_block_index);
	if (is_3d)
	{
		kernel_set_particles_data.setArg(6, cl_block_z_positions);
	}

	// ================================================================== //

	kernel_set_block_center_of_mass.setArg(0, cl_positions);
	kernel_set_block_center_of_mass.setArg(1, cl_masses);
	kernel_set_block_center_of_mass.setArg(2, cl_block_center_of_mass);
	kernel_set_block_center_of_mass.setArg(3, cl_block_particles);
	kernel_set_block_center_of_mass.setArg(4, cl_block_particles_size);

	// ================================================================== //

	kernel_calculate_particle_position_with_center_of_mass.setArg(0, cl_positions);
	kernel_calculate_particle_position_with_center_of_mass.setArg(1, cl_velocities);
	kernel_calculate_particle_position_with_center_of_mass.setArg(2, cl_masses);
	kernel_calculate_particle_position_with_center_of_mass.setArg(3, cl_block_center_of_mass);
	kernel_calculate_particle_position_with_center_of_mass.setArg(4, cl_block_x_positions);
	kernel_calculate_particle_position_with_center_of_mass.setArg(5, cl_min_x);
	kernel_calculate_particle_position_with_center_of_mass.setArg(6, cl_max_x);
	kernel_calculate_particle_position_with_center_of_mass.setArg(7, cl_min_y);
	kernel_calculate_particle_position_with_center_of_mass.setArg(8, cl_max_y);
	kernel_calculate_particle_position_with_center_of_mass.setArg(9, cl_block_particles_size);
	kernel_calculate_particle_position_with_center_of_mass.setArg(10, cl_block_particles);
	if (is_3d)
	{
		kernel_calculate_particle_position_with_center_of_mass.setArg(11, cl_min_z);
		kernel_calculate_particle_position_with_center_of_mass.setArg(12, cl_max_z);
	}

	// ================================================================== //

	kernel_calculate_particle_position_with_center_of_mass_via_solver.setArg(0, cl_positions);
	kernel_calculate_particle_position_with_center_of_mass_via_solver.setArg(1, cl_masses);
	kernel_calculate_particle_position_with_center_of_mass_via_solver.setArg(2, cl_block_particles_size);
	kernel_calculate_particle_position_with_center_of_mass_via_solver.setArg(3, cl_block_particles);
	kernel_calculate_particle_position_with_center_of_mass_via_solver.setArg(4, cl_block_center_of_mass);
	kernel_calculate_particle_position_with_center_of_mass_via_solver.setArg(5, cl_block_x_positions);
	kernel_calculate_particle_position_with_center_of_mass_via_solver.setArg(6, cl_state);
	kernel_calculate_particle_position_with_center_of_mass_via_solver.setArg(7, cl_step);

	// ================================================================== //

	kernel_calculate_particle_position.setArg(0, cl_particle_block_index);
	kernel_calculate_particle_position.setArg(1, cl_positions);
	kernel_calculate_particle_position.setArg(2, cl_velocities);
	kernel_calculate_particle_position.setArg(3, cl_masses);
	kernel_calculate_particle_position.setArg(4, cl_block_neighbors_size);
	kernel_calculate_particle_position.setArg(5, cl_block_neighbors_index);
	kernel_calculate_particle_position.setArg(6, cl_min_x);
	kernel_calculate_particle_position.setArg(7, cl_max_x);
	kernel_calculate_particle_position.setArg(8, cl_min_y);
	kernel_calculate_particle_position.setArg(9, cl_max_y);
	kernel_calculate_particle_position.setArg(10, cl_block_particles_size);
	kernel_calculate_particle_position.setArg(11, cl_block_particles);
	if (is_3d)
	{
		kernel_calculate_particle_position.setArg(12, cl_min_z);
		kernel_calculate_particle_position.setArg(13, cl_max_z);
	}

	// ================================================================== //

	kernel_calculate_particle_position_via_solver.setArg(0, cl_positions);
	kernel_calculate_particle_position_via_solver.setArg(1, cl_masses);
	kernel_calculate_particle_position_via_solver.setArg(2, cl_block_particles_size);
	kernel_calculate_particle_position_via_solver.setArg(3, cl_block_particles);
	kernel_calculate_particle_position_via_solver.setArg(4, cl_particle_block_index);
	kernel_calculate_particle_position_via_solver.setArg(5, cl_block_neighbors_size);
	kernel_calculate_particle_position_via_solver.setArg(6, cl_block_neighbors_index);
	kernel_calculate_particle_position_via_solver.setArg(7, cl_state);
	kernel_calculate_particle_position_via_solver.setArg(8, cl_step);
}

void RCubeBaseGPU::SetBlockNeighbours()
{
    RunClKernel("[Set Block Neighbours]", kernel_set_block_neighbors, number_of_blocks);
}

void RCubeBaseGPU::ResetVariables()
{
    RunClKernel("[Reset Variables]", kernel_reset_variables, number_of_blocks);
}

void RCubeBaseGPU::SetBlockPositions()
{
    RunClKernel("[Set Block Positions]", kernel_set_block_positions, 1);
}

void RCubeBaseGPU::SetParticlesData()
{
    RunClKernel("[Set Particles Data]", kernel_set_particles_data, number_of_particles);
}

void RCubeBaseGPU::SetBlockCenterOfMass()
{
    RunClKernel("[Set Block Center Of Mass]", kernel_set_block_center_of_mass, number_of_blocks);
}

void RCubeBaseGPU::CalculateParticlePositionWithCenterOfMass(const int& index, const float& delta_time)
{
	kernel_calculate_particle_position_with_center_of_mass.setArg(index, delta_time);
	RunClKernel("[Calculate Particle Position With Center Of Mass]", kernel_calculate_particle_position_with_center_of_mass, number_of_particles);
}

void RCubeBaseGPU::CalculateParticlePosition(const int& index, const float& delta_time)
{
	kernel_calculate_particle_position.setArg(index, delta_time);
	RunClKernel("[Calculate Particle Position]", kernel_calculate_particle_position, number_of_particles);
}

void RCubeBaseGPU::CalculateParticlePositionWithCenterOfMassViaSolver()
{
	RunClKernel("[Calculate Particle Position With Center Of Mass Via Solver]", kernel_calculate_particle_position_with_center_of_mass_via_solver, number_of_particles);
}

void RCubeBaseGPU::CalculateParticlePositionViaSolver()
{
	RunClKernel("[Calculate Particle Position Via Solver]", kernel_calculate_particle_position_via_solver, number_of_particles);
}