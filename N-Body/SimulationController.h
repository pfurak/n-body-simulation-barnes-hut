#pragma once

#include "Model.h"
#include "BHTree.h"
#include "BHTree3D.h"
#include "BHTree_GPU.h"
#include "BHTree3D_GPU.h"
#include "RCube.h"
#include "RCube3D.h"
#include "RCube_GPU.h"
#include "RCube3D_GPU.h"
#include "AdamsBashforth6.h"

class SimulationController
{
public:
    SimulationController(const int&, const int&, const float&, const float&, const float&, const bool&, const bool&);
    ~SimulationController();
    void Init(const int&);
    void RegisterParticle(float, float, float, float, float, float, float);
    void InitSolverState();
    void ReverseSolver();
    void InitGPU();
    bool is3D();
    bool isGPU();
    bool GPUInited();
    bool GetCenterOfMassLogic();
    bool SolverInUsed();
    void Update(float);
    const float* GetPositions();
    std::vector<glm::vec3> GetAxis();

    static std::vector<Algorithm> algorithms;
private:
    int selected_algorithm_index;
    int number_of_particles;

    float gravitational_constant;
    float theta;
    float lambda;

    bool gpu_inited;
    bool center_of_mass_logic;
    bool use_solver;

    Model* model;
    AdamsBashforth6* solver;
};