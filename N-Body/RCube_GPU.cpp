#include "RCube_GPU.h"

RCube_GPU::RCube_GPU(const bool& center_of_mass_logic, const int& number_of_particles, const float& gravitational_constant, const float& theta, const float& lambda, const int& device_index) :
Model(number_of_particles, gravitational_constant, theta, lambda, false),
RCubeBaseGPU(number_of_particles, gravitational_constant, theta, lambda, false, center_of_mass_logic, device_index)
{}

bool RCube_GPU::InitGPU()
{
	RCubeBaseGPU::InitGPU(source_code_file, min_x, max_x, min_y, max_y, this->GetPositions(), this->GetVelocities(), this->GetMasses());
	if (gpu_inited)
	{
		if (!center_of_mass_logic)
		{
			SetBlockNeighbours();
		}
	}
	return gpu_inited;
}

void RCube_GPU::Update(float delta_time)
{
	if (gpu_inited)
	{
		ResetVariables();
		SetBlockPositions();
		SetParticlesData();
		if (center_of_mass_logic)
		{
			SetBlockCenterOfMass();
			CalculateParticlePositionWithCenterOfMass(11, delta_time);
		}
		else
		{
			CalculateParticlePosition(12, delta_time);
		}
		ReadBackPositions(this->GetPositions());
	}
}

void RCube_GPU::UpdateViaSolver(float* state, float* step, bool skip)
{
	if (gpu_inited)
	{
		if (!skip)
		{
			this->CopyDataFromSolverState(state);
		}
		WriteIntoPositionsBuffer(this->GetPositions());
		ResetMinMaxBuffers(min_x, max_x, min_y, max_y);
		SetSolverBuffers(state, step);

		ResetVariables();
		SetBlockPositions();
		SetParticlesData();

		if (center_of_mass_logic)
		{
			SetBlockCenterOfMass();
			CalculateParticlePositionWithCenterOfMassViaSolver();
		}
		else
		{
			CalculateParticlePositionViaSolver();
		}

		ReadBackPositions(this->GetPositions());
		ReadBackSolverSteps(step);
	}
}

std::vector<glm::vec3> RCube_GPU::GetAxis()
{
	std::vector<glm::vec3> axis;

	if (gpu_inited)
	{
		const int vector_size = blocks_in_row + 1;
		const size_t size = sizeof(float) * vector_size;
		std::vector<float> block_x_positions = std::vector<float>(vector_size);
		command_queue.enqueueReadBuffer(cl_block_x_positions, CL_TRUE, 0, size, &block_x_positions[0]);

		std::vector<float> block_y_positions = std::vector<float>(vector_size);
		command_queue.enqueueReadBuffer(cl_block_y_positions, CL_TRUE, 0, size, &block_y_positions[0]);

		for (int i = 0; i < number_of_blocks; i++)
		{
			const int x_index = i % blocks_in_row;
			const int y_index = i / blocks_in_row;

			axis.push_back(glm::vec3(block_x_positions[x_index], block_y_positions[y_index], 0));
			axis.push_back(glm::vec3(block_x_positions[x_index + 1], block_y_positions[y_index + 1], 0));
		}
	}

	return axis;
}