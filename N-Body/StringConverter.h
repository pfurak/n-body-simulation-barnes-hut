#pragma once

#include <string>

class StringConverter
{
public:
	static std::string ISO_8859_2_TO_UTF_8(std::string);
};