#pragma once

#include "BaseGPU.h"

class BHTreeBaseGPU : public BaseGPU
{
public:
	BHTreeBaseGPU(const int&, const float&, const float&, const float&, const bool&, const int&);
	~BHTreeBaseGPU() {}
private:
    void SetNumberOfNodes();
    void SetCLBuffers();
    void SetKernelArguments();

    const char* helper_cl = "BHTree_functions.cl";
    const int EMPTY_NODE = -1;
    int MAX_COMPUTE_UNITS;
    int WORK_GROUPS;
    int WRAP_SIZE;

    int global;
    int local;
protected:
    void InitGPU(const char*, const float&, const float&, const float&, const float&, const float*, const float*, const float*, const float& = NULL, const float& = NULL);

    void BuildTree(const int& = 0);
    void CalculateParticlePosition(const int&, const float&);
    void CalculateParticlePositionViaSolver();

    int number_of_nodes;

    bool gpu_inited;

    cl::Kernel kernel_reset_tree;
    cl::Kernel kernel_build_tree;
    cl::Kernel kernel_summarize_tree;
    cl::Kernel kernel_calculate_force;
    cl::Kernel kernel_set_particle_nodes_axis;
    cl::Kernel kernel_calculate_particle_position;
    cl::Kernel kernel_calculate_particle_position_via_solver;

    cl::Kernel kernel_update_via_solver;
    cl::Kernel kernel_update;

    cl::Buffer cl_nodes_min_x_positions;			//	nodes possible min X coordinates [number_of_nodes + 1]
    cl::Buffer cl_nodes_max_x_positions;			//	nodes possible max X coordinates [number_of_nodes + 1]
    cl::Buffer cl_nodes_min_y_positions;			//	nodes possible min Y coordinates [number_of_nodes + 1]
    cl::Buffer cl_nodes_max_y_positions;			//	nodes possible max Y coordinates [number_of_nodes + 1]
    cl::Buffer cl_nodes_min_z_positions;			//	nodes possible min Z coordinates [number_of_nodes + 1]
    cl::Buffer cl_nodes_max_z_positions;			//	nodes possible min Z coordinates [number_of_nodes + 1]

    cl::Buffer cl_nodes_data;         		        //	nodes index: [max_nodes][LOCKED: -2, EMPTY: -1, OTHERWISE: particle_index]
    cl::Buffer cl_out_of_tree_particle_counter;     //  [1 * sizeof(int)]
    cl::Buffer cl_out_of_tree_particles;            //  particle indexes: [number_of_particles * sizeof(int)]
    cl::Buffer cl_bottom;                           //  [1 * sizeof(int)]

    cl::Buffer cl_nodes_center_of_mass_x;      		//	nodes center of mass x [number_of_nodes + 1]
    cl::Buffer cl_nodes_center_of_mass_y;      		//	nodes center of mass y [number_of_nodes + 1]
    cl::Buffer cl_nodes_center_of_mass_z;      		//	nodes center of mass z [number_of_nodes + 1]
    cl::Buffer cl_nodes_center_of_mass_mass;      	//	nodes center of mass mass [number_of_nodes + 1]

    cl::Buffer cl_forces;                           //  calculated force per particle [number_of_particles * {2 or 3} * sizeof(float)]
    cl::Buffer cl_current_node;                     //  [number_of_nodes - *bottom] * number_of_particles * sizeof(int)
    cl::Buffer cl_node_path;                        //  [number_of_nodes - *bottom] * number_of_particles * sizeof(int)
};