#pragma once

// openCL
#include <iostream>
#include <fstream>
#include <sstream>

#define __CL_DEBUG__ 1

#define __NO_STD_VECTOR
#define __CL_ENABLE_EXCEPTIONS

#include <CL/cl.hpp>
#if defined(__APPLE__) || defined(__LINUX__)
#include <CL/cl_gl.h>
#endif

#include "CLUtils.hpp"
#include "Logger.h"

class BaseGPU
{
public:
    BaseGPU(const int&, const float&, const float&, const float&, const bool&, const int&);
    ~BaseGPU() {}
    static void SetAvailableDevices();
    static cl::vector<cl::Device> available_devices;
private:
    const char* algorithm_name;

    float gravitational_constant;
    float theta;
    float lambda;
protected:
    void SetAlgorithmName(const char*);
    bool CreateProgarm(const std::vector<const char*>&, std::string = "");
    void SetBaseBuffers(const float&, const float&, const float&, const float&, const float*, const float*, const float*, const float&, const float&);
    void RunClKernel(const char*, const cl::Kernel&, const int& global, const int& offset = -1, const int& local = -1);
    void LogError(const char*, const cl::Error&);
    void ReadBackPositions(float*);
    void WriteIntoPositionsBuffer(const float*);
    void ResetMinMaxBuffers(const float&, const float&, const float&, const float&, const float& = NULL, const float& = NULL);
    void SetSolverBuffers(const float*, const float*);
    void ReadBackSolverSteps(float*);

    const char* base_source_code = "base_functions.cl";

    const cl_mem_flags cl_mem_flag = CL_MEM_ALLOC_HOST_PTR;

    int number_of_particles;

    bool is_3d;

    cl::Device device;
    cl::Context context;
    cl::Program cl_program;

    cl::CommandQueue command_queue;

    cl::Buffer cl_min_x;							//	min x coordinate value
    cl::Buffer cl_max_x;							//	max x coordinate value
    cl::Buffer cl_min_y;							//	min y coordinate value
    cl::Buffer cl_max_y;							//	max y coortinate value
    cl::Buffer cl_min_z;							//	min z coordinate value
    cl::Buffer cl_max_z;							//	max z coortinate value

    cl::Buffer cl_positions;						//	positions of particles		(float* array)
    cl::Buffer cl_velocities;						//	velocities of particles		(float* array)
    cl::Buffer cl_masses;							//	masses of particles			(float* array)

    cl::Buffer cl_state;
    cl::Buffer cl_step;
};