#!/bin/bash

clear

BASE_PATH="$(cd "$( dirname "$0" )" && pwd)/../Resources/"
APP_NAME="N-Body simulation"
LIB_PATH="${BASE_PATH}lib"

export DYLD_LIBRARY_PATH=${LIB_PATH}

echo "Running app: ${APP_NAME}"
(cd "${BASE_PATH}"; ./"${APP_NAME}")
