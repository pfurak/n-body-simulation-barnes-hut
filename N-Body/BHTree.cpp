#include "BHTree.h"

BHTree::BHTree(const int& number_of_particles, const float& gravitational_constant, const float& theta, const float& lambda):
Model(number_of_particles, gravitational_constant, theta, lambda, false)
{
	root_node = new BHTreeNode(theta, lambda);
}

BHTree::~BHTree()
{
	root_node->ResetTree();
	delete root_node;
	root_node = NULL;
}

void BHTree::Update(const float delta_time)
{
    BuildTree();
    UpdateParticlePositions(delta_time);
}

void BHTree::UpdateViaSolver(float* state, float* step, bool skip)
{
	if (!skip)
	{
		this->CopyDataFromSolverState(state);
	}

	BuildTree();
	for (int i = 0; i < number_of_particles; i++)
	{
		const int start_index = i * 4;
		glm::vec2 force = root_node->GetForce(i, state[start_index], state[start_index + 1], 1, gravitational_constant);
		step[start_index] = state[start_index + 2];
		step[start_index + 1] = state[start_index + 3];
		step[start_index + 2] = force.x;
		step[start_index + 3] = force.y;
	}
}

void BHTree::BuildTree()
{
	root_node->ResetTree();
	float* positions = this->GetPositions();
	float* masses = this->GetMasses();
	root_node->SetMinMaxCoordinates(min_x, max_x, min_y, max_y);
	for (int i = 0; i < number_of_particles; i++)
	{
		const int particle_start_position = i * 2;
		root_node->AddParticle(i, Particle(positions[particle_start_position], positions[particle_start_position + 1], masses[i]));
	}
	root_node->SetCenterOfMass();
}

void BHTree::UpdateParticlePositions(const float delta_time)
{
    float* positions = this->GetPositions();
    float* velocities = this->GetVelocities();
    float* masses = this->GetMasses();

    for(int i = 0; i < number_of_particles; i++)
	{
		const int particle_start_position = i * 2;
		const glm::vec2 force = root_node->GetForce(i, positions[particle_start_position], positions[particle_start_position + 1], masses[i], gravitational_constant);
		const glm::vec2 acc = force / masses[i];

		velocities[particle_start_position] += acc.x;
		velocities[particle_start_position + 1] += acc.y;
		
		const float x = positions[particle_start_position] + velocities[particle_start_position] * delta_time;
		const float y = positions[particle_start_position + 1] + velocities[particle_start_position + 1] * delta_time;

		this->SetParticlePositions(i, x, y);
	}
}

std::vector<glm::vec3> BHTree::GetAxis()
{
	return root_node->GetAxis();
}