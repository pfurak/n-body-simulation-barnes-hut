#include "SimulationController.h"

std::vector<Algorithm> SimulationController::algorithms = std::vector<Algorithm>({
    Algorithm("Barnes-Hut (2D - CPU)", false, false),
    Algorithm("Barnes-Hut (3D - CPU)", true, false),
    Algorithm("Barnes-Hut (2D - GPU)", false, true),
    Algorithm("Barnes-Hut (3D - GPU)", true, true),
    Algorithm("RCube (2D - CPU)", false, false),
    Algorithm("RCube (3D - CPU)", true, false),
    Algorithm("RCube (2D - GPU)", false, true),
    Algorithm("RCube (3D - GPU)", true, true)
});

SimulationController::SimulationController(
    const int& selected_algorithm_index, const int& number_of_particles,
    const float& gravitational_constant, const float& theta, const float& lambda,
    const bool& center_of_mass_logic, const bool& use_solver)
{
    this->selected_algorithm_index = selected_algorithm_index;
    this->number_of_particles = number_of_particles;

    this->gravitational_constant = gravitational_constant;
    this->theta = theta;
    this->lambda = lambda;

    this->center_of_mass_logic = center_of_mass_logic;
    this->use_solver = use_solver;
}

SimulationController::~SimulationController()
{
    if (use_solver)
    {
        delete solver;
    }
    delete model;
}

void SimulationController::Init(const int& current_device_index)
{
    if (selected_algorithm_index == 0) // Barnes-Hut (2D - CPU)
	{
		model = new BHTree(number_of_particles, gravitational_constant, theta, lambda);
	}
    else if (selected_algorithm_index == 1) // Barnes-Hut (3D - CPU)
    {
        model = new BHTree3D(number_of_particles, gravitational_constant, theta, lambda);
    }
    else if (selected_algorithm_index == 2) // Barnes-Hut (2D - GPU)
    {
        model = new BHTree_GPU(number_of_particles, gravitational_constant, theta, lambda, current_device_index);
    }
    else if (selected_algorithm_index == 3) // Barnes-Hut (3D - GPU)
    {
        model = new BHTree3D_GPU(number_of_particles, gravitational_constant, theta, lambda, current_device_index);
    }
	else if(selected_algorithm_index == 4) // RCube (2D - CPU)
	{
		model = new RCube(center_of_mass_logic, number_of_particles, gravitational_constant, theta, lambda);
	}
	else if (selected_algorithm_index == 5) // RCube (3D - CPU)
	{
		model = new RCube3D(center_of_mass_logic, number_of_particles, gravitational_constant, theta, lambda);
	}
	else if (selected_algorithm_index == 6) // RCube (2D - GPU)
	{
		model = new RCube_GPU(center_of_mass_logic, number_of_particles, gravitational_constant, theta, lambda, current_device_index);
	}
	else // RCube (3D - GPU)
	{
		model = new RCube3D_GPU(center_of_mass_logic, number_of_particles, gravitational_constant, theta, lambda, current_device_index);
	}

    if (use_solver)
    {
        solver = new AdamsBashforth6(number_of_particles, model, algorithms[selected_algorithm_index].is_3d);
    }
}

void SimulationController::RegisterParticle(float x, float y, float z, float mass, float velocity_x, float velocity_y, float velocity_z)
{
    model->RegisterParticle(x, y, z, mass, velocity_x, velocity_y, velocity_z);
}

void SimulationController::InitSolverState()
{
    if(use_solver)
    {
        solver->InitState();
    }
}

void SimulationController::ReverseSolver()
{
    if (use_solver)
    {
        solver->Reverse();
    }
}

void SimulationController::InitGPU()
{
    if(algorithms[selected_algorithm_index].is_gpu)
    {
        gpu_inited = model->InitGPU();
    }
}

bool SimulationController::is3D()
{
    return algorithms[selected_algorithm_index].is_3d;
}

bool SimulationController::isGPU()
{
    return algorithms[selected_algorithm_index].is_gpu;
}

bool SimulationController::GPUInited()
{
    return gpu_inited;
}

bool SimulationController::GetCenterOfMassLogic()
{
    return center_of_mass_logic;
}

bool SimulationController::SolverInUsed()
{
    return use_solver;
}

void SimulationController::Update(float delta_time)
{
    if(use_solver)
    {
        if (delta_time < 1)
        {
            delta_time = 1;
        }

        solver->SingleStep(delta_time);
    }
    else
    {
        model->Update(delta_time);
    }
}

const float* SimulationController::GetPositions()
{
    return model->GetPositions();
}

std::vector<glm::vec3> SimulationController::GetAxis()
{
    return model->GetAxis();
}