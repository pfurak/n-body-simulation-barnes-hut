#include "Camera.h"

Camera::Camera() {}

Camera::~Camera() {}

void Camera::ResetCamera()
{
	cameraPos_default_x = 0.0f;
	cameraPos_default_y = 0.0f;
	cameraPos_default_z = 3.0f;

	cameraFront_default_x = 0.0f;
	cameraFront_default_y = 0.0f;
	cameraFront_default_z = -1.0f;

	cameraUp_default_x = 0.0f;
	cameraUp_default_y = 1.0f;
	cameraUp_default_z = 0.0f;

	cameraPos = glm::vec3(cameraPos_default_x, cameraPos_default_y, cameraPos_default_z);
	cameraFront = glm::vec3(cameraFront_default_x, cameraFront_default_y, cameraFront_default_z);
	cameraUp = glm::vec3(cameraUp_default_x, cameraUp_default_y, cameraUp_default_z);

	yaw = -90.0f;
	pitch = 0;

	mouseDown = false;
}

void Camera::SetCameraPos(float x, float y, float z)
{
	cameraPos = glm::vec3(x, y, z);
}

glm::mat4 Camera::GetView()
{
	return glm::lookAt(cameraPos, cameraPos + cameraFront, cameraUp);
}

void Camera::ZoomOut()
{
	MouseWheel(-1);
}

void Camera::ZoomIn()
{
	MouseWheel(1);
}

void Camera::KeyboardDown(SDL_KeyboardEvent& key)
{
	if (key.keysym.sym == SDLK_w)
		cameraPos += cameraUp * cameraSpeed;
	if (key.keysym.sym == SDLK_s)
		cameraPos -= cameraUp * cameraSpeed;
	if (key.keysym.sym == SDLK_a)
		cameraPos -= glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraSpeed;
	if (key.keysym.sym == SDLK_d)
		cameraPos += glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraSpeed;
	if (key.keysym.sym == SDLK_UP)
		ZoomIn();
	if (key.keysym.sym == SDLK_DOWN)
		ZoomOut();
}

void Camera::MouseMove(float mouse_x, float mouse_y)
{
	if (mouseDown)
	{
		float xoffset = mouse_x - lastX;
		float yoffset = lastY - mouse_y;

		lastX = mouse_x;
		lastY = mouse_y;

		xoffset *= sensitivity;
		yoffset *= sensitivity;

		yaw -= xoffset;
		pitch -= yoffset;

		if (pitch > 89.0f) pitch = 89.0f;
		if (pitch < -89.0f) pitch = -89.0f;

		glm::vec3 direction;
		direction.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
		direction.y = sin(glm::radians(pitch));
		direction.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
		cameraFront = glm::normalize(direction);
	}
}

void Camera::MouseDown(int mouseButton, float mouse_x, float mouse_y)
{
	if (mouseButton == 1)
	{
		lastX = mouse_x;
		lastY = mouse_y;

		mouseDown = true;
	}
}

void Camera::MouseUp(int mouseButton)
{
	if (mouseButton == 1) mouseDown = false;
}

void Camera::MouseWheel(int wheel_y)
{
	// zoom in
	if (wheel_y > 0) cameraPos += cameraSpeed * cameraFront;
	// zoom out
	if (wheel_y < 0) cameraPos -= cameraSpeed * cameraFront;
}