#include "BaseGPU.h"

cl::vector<cl::Device> BaseGPU::available_devices = cl::vector<cl::Device>();

BaseGPU::BaseGPU(const int& number_of_particles, const float& gravitational_constant, const float& theta, const float& lambda, const bool& is_3d, const int& device_index)
{
	this->number_of_particles = number_of_particles;
	this->gravitational_constant = gravitational_constant;
	this->theta = theta;
	this->lambda = lambda;
	this->is_3d = is_3d;
	this->device = available_devices[device_index];
}

void BaseGPU::SetAvailableDevices()
{
	try
	{
		cl::vector<cl::Platform> platforms;
		cl::Platform::get(&platforms);
		cl::vector<cl::Device> dev;
		cl::Platform platform;

		if (platforms.size() > 0)
		{
			for (int i = 0; i < platforms.size(); i++)
			{
				platforms[i].getDevices(CL_DEVICE_TYPE_GPU, &dev);
				for (int j = 0; j < dev.size(); j++)
				{
					available_devices.push_back(dev[j]);
				}
			}
		}
	}
	catch (cl::Error error)
	{
		std::stringstream ss;
		ss << "[Init openCL] " << error.what() << "(" << oclErrorString(error.err()) << ")";
		Logger::LogError(ss.str().c_str());
	}
}

void BaseGPU::SetAlgorithmName(const char* algorithm_name)
{
	this->algorithm_name = algorithm_name;
}

bool BaseGPU::CreateProgarm(const std::vector<const char*>& source_codes, std::string options)
{
	try
	{
		cl::vector<cl::Device> dev;
		dev.push_back(device);
		context = cl::Context(dev);

		cl::vector<cl::Device> devices = context.getInfo<CL_CONTEXT_DEVICES>();
		command_queue = cl::CommandQueue(context, device);

		// Read source file
		std::string helper_file = FileSystem::bin_path + std::string(base_source_code);
		std::ifstream helper_source_file(helper_file.c_str());
		std::string helper_source_code(std::istreambuf_iterator<char>(helper_source_file), (std::istreambuf_iterator<char>()));

		std::string source_code = std::string(helper_source_code + "\n");
		for (int i = 0; i < source_codes.size(); i++)
		{
			std::string source_file = FileSystem::bin_path + std::string(source_codes[i]);
			std::ifstream kernel_source_file(source_file.c_str());
			std::string kernel_source_code(std::istreambuf_iterator<char>(kernel_source_file), (std::istreambuf_iterator<char>()));

			if (kernel_source_code.length() == 0)
			{
				std::stringstream ss;
				ss << "[Init GPU] Empty Source Codes\n";
				ss << "[Source file] " << source_file << "\n";
				Logger::LogError(ss.str().c_str());
				return false;
			}

			source_code += std::string(kernel_source_code + "\n");
		}
		
		cl::Program::Sources source(1, std::make_pair(source_code.c_str(), source_code.length() + 1));

		// Make program of the source code in the context
		cl_program = cl::Program(context, source);
		
		std::stringstream ss;
		/*
		std::string name = std::string(device.getInfo<CL_DEVICE_NAME>());
		std::transform(name.begin(), name.end(), name.begin(), [](unsigned char c) { return std::tolower(c); });
		if (name.find("nvidia") != std::string::npos)
		{
			ss << " -D IS_NVIDIA";
		}
		else if (name.find("intel") != std::string::npos)
		{
			ss << " -D IS_INTEL";
		}
		*/
		ss << options;
		ss << " -D NUMBER_OF_PARTICLES=" << number_of_particles;
		ss << " -D THETA=" << theta << "f";
		ss << " -D LAMBDA=" << lambda << "f";
		ss << " -D GRAVITATIONAL_CONSTANT=" << gravitational_constant << "f";
		if (is_3d)
		{
			ss << " -D IS_3D";
		}

#ifdef __CL_DEBUG__
		ss << " -D DEBUG";
#endif
		cl_program.build(devices, ss.str().c_str());

		return true;
	}
	catch (cl::Error error)
	{
		std::stringstream ss;
		ss << "[Init GPU] " << cl_program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(device);
		ss << " Build options: " << cl_program.getBuildInfo<CL_PROGRAM_BUILD_OPTIONS>(device);
		Logger::LogError(ss.str().c_str());
		return false;
	}
}

void BaseGPU::SetBaseBuffers(const float& min_x, const float& max_x, const float& min_y, const float& max_y, const float* positions, const float* velocities, const float* masses, const float& min_z, const float& max_z)
{
	// min and max coordinates
	size_t size = sizeof(float);
	cl_min_x = cl::Buffer(context, cl_mem_flag, size);
	command_queue.enqueueWriteBuffer(cl_min_x, CL_TRUE, 0, size, &min_x);

	cl_max_x = cl::Buffer(context, cl_mem_flag, size);
	command_queue.enqueueWriteBuffer(cl_max_x, CL_TRUE, 0, size, &max_x);

	cl_min_y = cl::Buffer(context, cl_mem_flag, size);
	command_queue.enqueueWriteBuffer(cl_min_y, CL_TRUE, 0, size, &min_y);

	cl_max_y = cl::Buffer(context, cl_mem_flag, size);
	command_queue.enqueueWriteBuffer(cl_max_y, CL_TRUE, 0, size, &max_y);

	if (is_3d)
	{
		cl_min_z = cl::Buffer(context, cl_mem_flag, size);
		command_queue.enqueueWriteBuffer(cl_min_z, CL_TRUE, 0, size, &min_z);

		cl_max_z = cl::Buffer(context, cl_mem_flag, size);
		command_queue.enqueueWriteBuffer(cl_max_z, CL_TRUE, 0, size, &max_z);
	}

	// ================================================================== //

	// particles coordinates and mass
	size = sizeof(float) * number_of_particles * (is_3d ? 3 : 2);
	cl_positions = cl::Buffer(context, cl_mem_flag, size);
	command_queue.enqueueWriteBuffer(cl_positions, CL_TRUE, 0, size, positions);

	cl_velocities = cl::Buffer(context, cl_mem_flag, size);
	command_queue.enqueueWriteBuffer(cl_velocities, CL_TRUE, 0, size, velocities);

	size = sizeof(float) * number_of_particles;
	cl_masses = cl::Buffer(context, cl_mem_flag, size);
	command_queue.enqueueWriteBuffer(cl_masses, CL_TRUE, 0, size, masses);

	// ================================================================== //

	// solver buffers
	size = sizeof(float) * number_of_particles * (is_3d ? 6 : 4);
	cl_state = cl::Buffer(context, cl_mem_flag, size);
	cl_step = cl::Buffer(context, cl_mem_flag, size);
}

void BaseGPU::RunClKernel(const char* function, const cl::Kernel& kernel, const int& global, const int& offset, const int& local)
{
	try
	{
		cl::NDRange o = offset == -1 ? cl::NullRange : offset;
		cl::NDRange l = local == -1 ? cl::NullRange : local;

		// std::cout << function << std::endl;
		command_queue.enqueueNDRangeKernel(kernel, o, global, l);
		command_queue.finish();
		// std::cout << function << " ended" << std::endl;
	}
	catch (cl::Error error)
	{
		LogError(function, error);
		exit(1);
	}
}

void BaseGPU::LogError(const char* function, const cl::Error& error)
{
	std::stringstream ss;
	
	ss << '[' << algorithm_name << ']' << function << " " << error.what() << "(" << oclErrorString(error.err()) << ")";
	Logger::LogError(ss.str().c_str());
}

void BaseGPU::ReadBackPositions(float* positions)
{
	const size_t size = sizeof(float) * number_of_particles * (is_3d ? 3 : 2);
	command_queue.enqueueReadBuffer(cl_positions, CL_TRUE, 0, size, positions);
}

void BaseGPU::WriteIntoPositionsBuffer(const float* positions)
{
	const size_t size = sizeof(float) * number_of_particles * (is_3d ? 3 : 2);
	command_queue.enqueueWriteBuffer(cl_positions, CL_TRUE, 0, size, positions);
}

void BaseGPU::ResetMinMaxBuffers(const float& min_x, const float& max_x, const float& min_y, const float& max_y, const float& min_z, const float& max_z)
{
	size_t size = sizeof(float);
	command_queue.enqueueWriteBuffer(cl_min_x, CL_TRUE, 0, size, &min_x);
	command_queue.enqueueWriteBuffer(cl_max_x, CL_TRUE, 0, size, &max_x);
	command_queue.enqueueWriteBuffer(cl_min_y, CL_TRUE, 0, size, &min_y);
	command_queue.enqueueWriteBuffer(cl_max_y, CL_TRUE, 0, size, &max_y);
	if (is_3d)
	{
		command_queue.enqueueWriteBuffer(cl_min_z, CL_TRUE, 0, size, &min_z);
		command_queue.enqueueWriteBuffer(cl_max_z, CL_TRUE, 0, size, &max_z);
	}
}

void BaseGPU::SetSolverBuffers(const float* state, const float* step)
{
	size_t size = sizeof(float) * number_of_particles * (is_3d ? 6 : 4);
	command_queue.enqueueWriteBuffer(cl_state, CL_TRUE, 0, size, state);
	command_queue.enqueueWriteBuffer(cl_step, CL_TRUE, 0, size, step);
}

void BaseGPU::ReadBackSolverSteps(float* step)
{
	size_t size = sizeof(float) * number_of_particles * (is_3d ? 6 : 4);
	command_queue.enqueueReadBuffer(cl_step, CL_TRUE, 0, size, step);
}