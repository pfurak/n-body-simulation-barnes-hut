/*
 * DEFINED VARIABLES
 * NUMBER_OF_NODES number_of_nodes
 * NUMBER_OF_PARTICLES number_of_particles
 * THETA theta
 * LAMBDA lambda
 * GRAVITATIONAL_CONSTANT gravitational_constant
 */

#define EMPTY_NODE -1
#define LOCKED_NODE -2

#ifdef IS_3D
    #define NUMBER_OF_CHILD_NODES 8

    // ================================================================== //
    inline int GetNodePath(const float3 node_min_coordinates, const float3 node_max_coordinates, const float3 particle_coordinates)
    {
        const float node_min_x = node_min_coordinates.x;
        const float node_max_x = node_max_coordinates.x;
        const float node_middle_x = node_min_x + (node_max_x - node_min_x) / 2;

        const float node_min_y = node_min_coordinates.y;
        const float node_max_y = node_max_coordinates.y;
        const float node_middle_y = node_min_y + (node_max_y - node_min_y) / 2;

        const float node_min_z = node_min_coordinates.z;
        const float node_max_z = node_max_coordinates.z;
        const float node_middle_z = node_min_z + (node_max_z - node_min_z) / 2;

        const float particle_x = particle_coordinates.x;
        const float particle_y = particle_coordinates.y;
        const float particle_z = particle_coordinates.z;

        if(node_min_z <= particle_z && particle_z <= node_middle_z)
        {
            if (node_min_x <= particle_x && particle_x <= node_middle_x)
            {
                // NW (index: 0)
                if (node_middle_y <= particle_y && particle_y <= node_max_y)
                {
                    return 0;
                }

                // SW (index: 2)
                if (node_min_y <= particle_y && particle_y <= node_middle_y)
                {
                    return 2;
                }
            }

            if (node_middle_x <= particle_x && particle_x <= node_max_x)
            {
                // NE (index: 1)
                if (node_middle_y <= particle_y && particle_y <= node_max_y)
                {
                    return 1;
                }

                // SE (index: 3)
                if (node_min_y <= particle_y && particle_y <= node_middle_y)
                {
                    return 3;
                }
            }
        }
        
        if (node_middle_z <= particle_z && particle_z <= node_max_z)
        {
            if (node_min_x <= particle_x && particle_x <= node_middle_x)
            {
                // NW (index: 4)
                if (node_middle_y <= particle_y && particle_y <= node_max_y)
                {
                    return 4;
                }

                // SW (index: 6)
                if (node_min_y <= particle_y && particle_y <= node_middle_y)
                {
                    return 6;
                }
            }

            if (node_middle_x <= particle_x && particle_x <= node_max_x)
            {
                // NE (index: 5)
                if (node_middle_y <= particle_y && particle_y <= node_max_y)
                {
                    return 5;
                }

                // SE (index: 7)
                if (node_min_y <= particle_y && particle_y <= node_middle_y)
                {
                    return 7;
                }
            }
        }

        return -1;
    }

    // ================================================================== //
    inline void CreateCell(
        volatile __global int* nodes_data,
        __global float* nodes_center_of_mass_mass,
        __global float* nodes_min_x_positions,
        __global float* nodes_max_x_positions,
        __global float* nodes_min_y_positions,
        __global float* nodes_max_y_positions,
        __global float* nodes_min_z_positions,
        __global float* nodes_max_z_positions,
        const float3 particle_coordinates,
        const int node_index,
        const int new_node_index
    )
    {
        const float3 node_min_coordinates = (float3) { nodes_min_x_positions[node_index], nodes_min_y_positions[node_index], nodes_min_z_positions[node_index] };
        const float3 node_max_coordinates = (float3) { nodes_max_x_positions[node_index], nodes_max_y_positions[node_index], nodes_max_z_positions[node_index] };
        const float middle_x = node_min_coordinates.x + (node_max_coordinates.x - node_min_coordinates.x) / 2;
        const float middle_y = node_min_coordinates.y + (node_max_coordinates.y - node_min_coordinates.y) / 2;
        const float middle_z = node_min_coordinates.z + (node_max_coordinates.z - node_min_coordinates.z) / 2;

        // X
        if(particle_coordinates.x <= middle_x)
        {
            nodes_min_x_positions[new_node_index] = node_min_coordinates.x;
            nodes_max_x_positions[new_node_index] = middle_x;
        }
        else
        {
            nodes_min_x_positions[new_node_index] = middle_x;
            nodes_max_x_positions[new_node_index] = node_max_coordinates.x;
        }

        // Y
        if(particle_coordinates.y <= middle_y)
        {
            nodes_min_y_positions[new_node_index] = node_min_coordinates.y;
            nodes_max_y_positions[new_node_index] = middle_y;
        }
        else
        {
            nodes_min_y_positions[new_node_index] = middle_y;
            nodes_max_y_positions[new_node_index] = node_max_coordinates.y;
        }

        // Z
        if(particle_coordinates.z <= middle_z)
        {
            nodes_min_z_positions[new_node_index] = node_min_coordinates.z;
            nodes_max_z_positions[new_node_index] = middle_z;
        }
        else
        {
            nodes_min_z_positions[new_node_index] = middle_z;
            nodes_max_z_positions[new_node_index] = node_max_coordinates.z;
        }

        for (int i = 0; i < NUMBER_OF_CHILD_NODES; i++)
        {
            nodes_data[new_node_index * NUMBER_OF_CHILD_NODES + i] = EMPTY_NODE;
        }

        nodes_center_of_mass_mass[new_node_index] = -1.0f;
    }

    // ================================================================== //
    inline bool SameParticles(const __global float* positions, const int particle_index, const int other_particle_indes)
    {
        const float3 p1 = GetParticleCoordinates(positions, particle_index);
        const float3 p2 = GetParticleCoordinates(positions, other_particle_indes);
        return p1.x == p2.x && p1.y == p2.y && p1.z == p2.z;
    }
#else
    #define NUMBER_OF_CHILD_NODES 4

    // ================================================================== //
    inline int GetNodePath(const float2 node_min_coordinates, const float2 node_max_coordinates, const float2 particle_coordinates)
    {
        const float node_min_x = node_min_coordinates.x;
        const float node_max_x = node_max_coordinates.x;
        const float node_middle_x = node_min_x + (node_max_x - node_min_x) / 2;

        const float node_min_y = node_min_coordinates.y;
        const float node_max_y = node_max_coordinates.y;
        const float node_middle_y = node_min_y + (node_max_y - node_min_y) / 2;

        const float particle_x = particle_coordinates.x;
        const float particle_y = particle_coordinates.y;

        if (node_min_x <= particle_x && particle_x <= node_middle_x)
        {
            // NW (index: 0)
            if (node_middle_y <= particle_y && particle_y <= node_max_y)
            {
                return 0;
            }

            // SW (index: 2)
            if (node_min_y <= particle_y && particle_y <= node_middle_y)
            {
                return 2;
            }
        }

        if (node_middle_x <= particle_x && particle_x <= node_max_x)
        {
            // NE (index: 1)
            if (node_middle_y <= particle_y && particle_y <= node_max_y)
            {
                return 1;
            }

            // SE (index: 3)
            if (node_min_y <= particle_y && particle_y <= node_middle_y)
            {
                return 3;
            }
        }

       return -1;
    }

    // ================================================================== //
    inline void CreateCell(
        volatile __global int* nodes_data,
        __global float* nodes_center_of_mass_mass,
        __global float* nodes_min_x_positions,
        __global float* nodes_max_x_positions,
        __global float* nodes_min_y_positions,
        __global float* nodes_max_y_positions,
        const float2 particle_coordinates,
        const int node_index,
        const int new_node_index
    )
    {
        const float2 node_min_coordinates = (float2) { nodes_min_x_positions[node_index], nodes_min_y_positions[node_index] };
        const float2 node_max_coordinates = (float2) { nodes_max_x_positions[node_index], nodes_max_y_positions[node_index] };
        const int node_path = GetNodePath(node_min_coordinates, node_max_coordinates, particle_coordinates);
        const float middle_x = node_min_coordinates.x + (node_max_coordinates.x - node_min_coordinates.x) / 2;
        const float middle_y = node_min_coordinates.y + (node_max_coordinates.y - node_min_coordinates.y) / 2;

        if(node_path % 2 == 0)
        {
            nodes_min_x_positions[new_node_index] = node_min_coordinates.x;
            nodes_max_x_positions[new_node_index] = middle_x;
        }
        else
        {
            nodes_min_x_positions[new_node_index] = middle_x;
            nodes_max_x_positions[new_node_index] = node_max_coordinates.x;
        }

        if(node_path < 2)
        {
            nodes_min_y_positions[new_node_index] = middle_y;
            nodes_max_y_positions[new_node_index] = node_max_coordinates.y;
        }
        else
        {
            nodes_min_y_positions[new_node_index] = node_min_coordinates.y;
            nodes_max_y_positions[new_node_index] = middle_y;
        }

        for (int i = 0; i < NUMBER_OF_CHILD_NODES; i++)
        {
            nodes_data[new_node_index * NUMBER_OF_CHILD_NODES + i] = EMPTY_NODE;
        }

        nodes_center_of_mass_mass[new_node_index] = -1.0f;
    }

    // ================================================================== //
    inline bool SameParticles(const __global float* positions, const int particle_index, const int other_particle_indes)
    {
        const float2 p1 = GetParticleCoordinates(positions, particle_index);
        const float2 p2 = GetParticleCoordinates(positions, other_particle_indes);
        return p1.x == p2.x && p1.y == p2.y;
    }
#endif