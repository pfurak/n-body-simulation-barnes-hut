#pragma once

// GLEW
#include <GL/glew.h>

// SDL
#include <SDL.h>

// GLM
#include <glm/glm.hpp>

#ifndef STRUCTS_H
#include "Structs.h"
#endif

class Orb
{
public:
	Orb(float r);
	Orb();
	~Orb(void);
	void Clean();

	Vertex* getVertex();
	int getVertexSize();

	GLushort* getBuffer();
	int getBufferSize();
private:
	void Init();
	float r;
	
	static const int N = 20;
	static const int M = 10;

	Vertex* vert;
	int vertex_size = (N + 1) * (M + 1);

	GLushort* indices;
	int buffer_size = 3 * 2 * (N) * (M);

	glm::vec3 GetPos(float u, float v);
};

