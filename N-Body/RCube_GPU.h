#pragma once

#include "Model.h"
#include "RCubeBaseGPU.h"

class RCube_GPU : public Model, public RCubeBaseGPU
{
public:
	RCube_GPU(const bool&, const int&, const float&, const float&, const float&, const int&);
	~RCube_GPU() {}
	virtual bool InitGPU();
	virtual void Update(const float);
	virtual void UpdateViaSolver(float*, float*, bool = false);
	virtual std::vector<glm::vec3> GetAxis();
private:
	// CL
	const char* source_code_file = "RCube_GPU.cl";
};