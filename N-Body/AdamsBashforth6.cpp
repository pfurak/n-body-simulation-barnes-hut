#include "AdamsBashforth6.h"

AdamsBashforth6::AdamsBashforth6(int number_of_particles, Model* model, bool is_3d, float timestep_size)
{
    this->number_of_particles = number_of_particles;
    this->model = model;
    this->is_3d = is_3d;
    this->timestep_size = timestep_size;
    timestep_half_size = timestep_size * 0.5;
    divided_timestep = timestep_size / order;
    dimension = number_of_particles * (is_3d ? 6 : 4); // 4 -> because 1 particle has 4 coordinates (x, y, velocity_x, velocity_y);

    velocity_constants[0] = 4277.0 / 1440.0;
    velocity_constants[1] = -7923.0 / 1440.0;
    velocity_constants[2] = 9982.0 / 1440.0;
    velocity_constants[3] = -7298.0 / 1440.0;
    velocity_constants[4] = 2877.0 / 1440.0;
    velocity_constants[5] = -475.0 / 1440.0;

    state = std::vector<float>(dimension, 0);
    deriv = std::vector<std::vector<float>>(order);
    for (int i = 0; i < order; i++)
    {
        deriv[i] = std::vector<float>(dimension, 0);
    }
}

AdamsBashforth6::~AdamsBashforth6()
{
    state.clear();
    for (int i = 0; i < order; i++)
    {
        deriv[i].clear();
    }
    deriv.clear();
}

void AdamsBashforth6::InitState()
{
    void (*fn)(const int&, const float*, float*, const float&, const float*, Model*);
    if (is_3d)
    {
        Init3DState();
        fn = &AdamsBashforth6::Init3DTempState;
    }
    else
    {
        Init2DState();
        fn = &AdamsBashforth6::Init2DTempState;
    }

    std::vector<float> step_1 = std::vector<float>(dimension, 0);
    std::vector<float> step_2 = std::vector<float>(dimension, 0);
    std::vector<float> step_3 = std::vector<float>(dimension, 0);
    std::vector<float> step_4 = std::vector<float>(dimension, 0);
    std::vector<float> temp_state = std::vector<float>(dimension, 0);

    for (int i = 0; i < order - 1; i++)
    {
        model->UpdateViaSolver(&state[0], &step_1[0], true);
        fn(dimension, &state[0], &temp_state[0], timestep_half_size, &step_1[0], model);

        model->UpdateViaSolver(&temp_state[0], &step_2[0], true);
        fn(dimension, &state[0], &temp_state[0], timestep_half_size, &step_2[0], model);

        model->UpdateViaSolver(&temp_state[0], &step_3[0], true);
        fn(dimension, &state[0], &temp_state[0], timestep_size, &step_3[0], model);

        model->UpdateViaSolver(&temp_state[0], &step_4[0], true);
        for (int j = 0; j < dimension; j++)
        {
            state[j] += divided_timestep * (step_1[j] + 2 * (step_2[j] + step_3[j]) + step_4[j]);
            deriv[i][j] = step_1[j];
        }
    }
    
    step_1.clear();
    step_2.clear();
    step_3.clear();
    step_4.clear();
    temp_state.clear();

    model->UpdateViaSolver(&state[0], &deriv[5][0]);
}

void AdamsBashforth6::Init2DState()
{
    float* positions = model->GetPositions();
    float* velocities = model->GetVelocities();

    for (int i = 0; i < number_of_particles; i++)
    {
        const int pos_index = i * 2;
        const int state_index = i * 4;

        state[state_index] = positions[pos_index];
        state[state_index + 1] = positions[pos_index + 1];

        state[state_index + 2] = velocities[pos_index];
        state[state_index + 3] = velocities[pos_index + 1];
    }
}

void AdamsBashforth6::Init2DTempState(const int& dimension, const float* state, float* temp_state, const float& timestep, const float* step, Model* model)
{
    for (int i = 0; i < dimension; i += 4)
    {
        temp_state[i] = state[i] + timestep * step[i];
        temp_state[i + 1] = state[i + 1] + timestep * step[i + 1];
        temp_state[i + 2] = state[i + 2] + timestep * step[i + 2];
        temp_state[i + 3] = state[i + 3] + timestep * step[i + 3];

        const int particle_index = i / 4;
        model->SetParticlePositions(particle_index, temp_state[i], temp_state[i + 1]);
        model->SetParticleVelocities(particle_index, temp_state[i + 2], temp_state[i + 3]);
    }
}

void AdamsBashforth6::Init3DState()
{
    float* positions = model->GetPositions();
    float* velocities = model->GetVelocities();

    for (int i = 0; i < number_of_particles; i++)
    {
        const int pos_index = i * 3;
        const int state_index = i * 6;

        state[state_index] = positions[pos_index];
        state[state_index + 1] = positions[pos_index + 1];
        state[state_index + 2] = positions[pos_index + 2];

        state[state_index + 3] = velocities[pos_index];
        state[state_index + 4] = velocities[pos_index + 1];
        state[state_index + 5] = velocities[pos_index + 2];
    }
}

void AdamsBashforth6::Init3DTempState(const int& dimension, const float* state, float* temp_state, const float& timestep, const float* step, Model* model)
{
    for (int i = 0; i < dimension; i += 6)
    {
        temp_state[i] = state[i] + timestep * step[i];
        temp_state[i + 1] = state[i + 1] + timestep * step[i + 1];
        temp_state[i + 2] = state[i + 2] + timestep * step[i + 2];
        temp_state[i + 3] = state[i + 3] + timestep * step[i + 3];
        temp_state[i + 4] = state[i + 4] + timestep * step[i + 4];
        temp_state[i + 5] = state[i + 5] + timestep * step[i + 5];

        const int particle_index = i / 6;
        model->SetParticlePositions(particle_index, temp_state[i], temp_state[i + 1], temp_state[i + 2]);
        model->SetParticleVelocities(particle_index, temp_state[i + 3], temp_state[i + 4], temp_state[i + 5]);
    }
}

void AdamsBashforth6::SingleStep(const float delta_time)
{
    for (int i = 0; i < dimension; i++)
    {
        state[i] += timestep_size * delta_time * (
                velocity_constants[0] * deriv[5][i] +
                velocity_constants[1] * deriv[4][i] +
                velocity_constants[2] * deriv[3][i] +
                velocity_constants[3] * deriv[2][i] +
                velocity_constants[4] * deriv[1][i] +
                velocity_constants[5] * deriv[0][i] );
        
        deriv[0][i] = deriv[1][i];
        deriv[1][i] = deriv[2][i];
        deriv[2][i] = deriv[3][i];
        deriv[3][i] = deriv[4][i];
        deriv[4][i] = deriv[5][i];
    }
    model->UpdateViaSolver(&state[0], &deriv[5][0]);
}

void AdamsBashforth6::Reverse()
{
    timestep_size *= -1;
    timestep_half_size *= -1;
    divided_timestep *= -1;
    InitState();
}