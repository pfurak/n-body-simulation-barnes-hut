#pragma once

#include "Model.h"
#include <vector>

class AdamsBashforth6
{
public:
    AdamsBashforth6(int, Model*, bool, float = 100.0f);
    ~AdamsBashforth6();
    void InitState();
    void SingleStep(const float);
    void Reverse();
private:
    const int order = 6;
    int number_of_particles;
    int dimension;

    bool is_3d;

    float timestep_size;
    float timestep_half_size;
    float divided_timestep;

    float velocity_constants[6];
    std::vector<float> state;
    std::vector<std::vector<float>> deriv;
    Model* model;

    void Init2DState();
    static void Init2DTempState(const int&, const float*, float*, const float&, const float*, Model*);
    void Init3DState();
    static void Init3DTempState(const int&, const float*, float*, const float&, const float*, Model*);
};