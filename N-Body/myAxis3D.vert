#version 150

// values to be passed on in the pipeline
out vec3 vs_out_col;

uniform mat4 MVP;

const int offset_size = 500;
uniform vec3 min_coordinates[offset_size];
uniform vec3 max_coordinates[offset_size];

const vec3 color = vec3(0, 1, .2);

void main()
{
    vec3 min_offset = min_coordinates[gl_VertexID / 32];
    vec3 max_offset = max_coordinates[gl_VertexID / 32];

	int current_position = gl_VertexID % 32;
	if(current_position == 0 || current_position == 7)
	{
		gl_Position = MVP * vec4(min_offset.x, min_offset.y, min_offset.z, 1);
	}
	else if(current_position == 1 || current_position == 2)
	{
		gl_Position = MVP * vec4(max_offset.x, min_offset.y, min_offset.z, 1);
	}
	else if(current_position == 3 || current_position == 4)
	{
		gl_Position = MVP * vec4(max_offset.x, max_offset.y, min_offset.z, 1);
	}
	else if(current_position == 5 || current_position == 6)
	{
		gl_Position = MVP * vec4(min_offset.x, max_offset.y, min_offset.z, 1);
	}

    else if(current_position == 8 || current_position == 15)
    {
        gl_Position = MVP * vec4(min_offset.x, min_offset.y, max_offset.z, 1);
    }
    else if(current_position == 9 || current_position == 10)
    {
        gl_Position = MVP * vec4(max_offset.x, min_offset.y, max_offset.z, 1);
    }
    else if(current_position == 11 || current_position == 12)
    {
        gl_Position = MVP * vec4(max_offset.x, max_offset.y, max_offset.z, 1);
    }
    else if(current_position == 13 || current_position == 14)
    {
        gl_Position = MVP * vec4(min_offset.x, max_offset.y, max_offset.z, 1);
    }

    else if(current_position == 16 || current_position == 23)
    {
        gl_Position = MVP * vec4(min_offset.x, min_offset.y, min_offset.z, 1);
    }
    else if(current_position == 17 || current_position == 18)
    {
        gl_Position = MVP * vec4(min_offset.x, max_offset.y, min_offset.z, 1);
    }
    else if(current_position == 19 || current_position == 20)
    {
        gl_Position = MVP * vec4(min_offset.x, max_offset.y, max_offset.z, 1);
    }
    else if(current_position == 21 || current_position == 22)
    {
        gl_Position = MVP * vec4(min_offset.x, min_offset.y, max_offset.z, 1);
    }

    else if(current_position == 24 || current_position == 31)
    {
        gl_Position = MVP * vec4(max_offset.x, min_offset.y, min_offset.z, 1);
    }
    else if(current_position == 25 || current_position == 26)
    {
        gl_Position = MVP * vec4(max_offset.x, max_offset.y, min_offset.z, 1);
    }
    else if(current_position == 27 || current_position == 28)
    {
        gl_Position = MVP * vec4(max_offset.x, max_offset.y, max_offset.z, 1);
    }
    else if(current_position == 29 || current_position == 30)
    {
        gl_Position = MVP * vec4(max_offset.x, min_offset.y, max_offset.z, 1);
    }

	vs_out_col = color;
}