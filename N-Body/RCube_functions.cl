/*
 * DEFINED VARIABLES
 * NUMBER_OF_PARTICLES number_of_particles
 * THETA theta
 * LAMBDA lambda
 * GRAVITATIONAL_CONSTANT gravitational_constant
 * NUMBER_OF_BLOCKS number_of_blocks
 * BLOCKS_IN_ROW blocks_in_row
 * BLOCKS_IN_ROW_POW blocks_in_row_pow
 */

// ================================================================== //
__kernel void ResetVariables(__global int* block_particles_size)
{
	const int block_index = get_global_id(0);
	block_particles_size[block_index] = 0;
}

#ifdef IS_3D
    // ================================================================== /
    inline float3 GetBlockIndexes(int block_index)
    {
        const int x_index = block_index % BLOCKS_IN_ROW;
        int y_index = block_index / BLOCKS_IN_ROW;
        if (y_index >= BLOCKS_IN_ROW)
        {
            y_index %= BLOCKS_IN_ROW;
        }
        const int z_index = block_index / BLOCKS_IN_ROW_POW;

        return (float3) { x_index, y_index, z_index };
    }

    // ================================================================== //
    inline float3 CalculateBlockForceByParticle(
        __global float* positions,
        __global float* masses,
        __global int* block_particles_size,
        __global int* block_particles,
        const int current_particle_index,
        const int block_index,
        const float pos_x,
        const float pos_y,
        const float pos_z,
        const float gravitational_constant_mass
    )
    {
        float3 force = {0, 0, 0};
        const int block_particles_start_index = block_index * NUMBER_OF_PARTICLES;
        for(int i = 0; i < block_particles_size[block_index]; i++)
        {
            const int particle_index = block_particles[block_particles_start_index + i];
            if (current_particle_index == particle_index)
            {
                continue;
            }
            
            const float3 particle_coordinates = GetParticleCoordinates(positions, particle_index);
            const float x = pos_x - particle_coordinates.x;
            const float y = pos_y - particle_coordinates.y;
            const float z = pos_z - particle_coordinates.z;
            const float distance = sqrt((float)(x * x + y * y + z * z + LAMBDA));
            if (distance > 0)
            {
                const float F = (gravitational_constant_mass * masses[particle_index]) / (distance * distance * distance);
                force.x += F * (particle_coordinates.x - pos_x);
                force.y += F * (particle_coordinates.y - pos_y);
                force.z += F * (particle_coordinates.z - pos_z);
            }
        }
        
        return force;
    }
#else
    // ================================================================== //
    inline float2 GetBlockIndexes(int block_index)
    {
        const int x_index = block_index % BLOCKS_IN_ROW;
        const int y_index = block_index / BLOCKS_IN_ROW;
        return (float2) { x_index, y_index };
    }

    // ================================================================== //
    inline float2 CalculateBlockForceByParticle(
        __global float* positions,
        __global float* masses,
        __global int* block_particles_size,
        __global int* block_particles,
        const int current_particle_index,
        const int block_index,
        const float pos_x,
        const float pos_y,
        const float gravitational_constant_mass
    )
    {
        float2 force = {0, 0};
        const int block_particles_start_index = block_index * NUMBER_OF_PARTICLES;
        for(int i = 0; i < block_particles_size[block_index]; i++)
        {
            const int particle_index = block_particles[block_particles_start_index + i];
            if (current_particle_index == particle_index)
            {
                continue;
            }
            
            const float2 particle_coordinates = GetParticleCoordinates(positions, particle_index);
            const float x = pos_x - particle_coordinates.x;
            const float y = pos_y - particle_coordinates.y;
            const float distance = sqrt((float)(x * x + y * y + LAMBDA));
            if (distance > 0)
            {
                const float F = (gravitational_constant_mass * masses[particle_index]) / (distance * distance * distance);
                force.x += F * (particle_coordinates.x - pos_x);
                force.y += F * (particle_coordinates.y - pos_y);
            }
        }
        return force;
    }
#endif