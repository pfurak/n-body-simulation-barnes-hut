﻿// GLEW
#include <GL/glew.h>

// SDL
#include <SDL.h>
#include <SDL_opengl.h>
#include <SDL_image.h>

// ImGui
#include "imgui/imgui.h"
#include "imgui/imgui_impl_sdl_gl3.h"

#include <iostream>
#include <sstream>

#include "Logger.h"
#include "NBody.h"
#include "gTimer.h"
#include "FileSystem.h"

int main(int argc, char* args[])
{
	FileSystem::SetPathVariables();

	//
	// 1. step: Initialize SDL
	//

	if (SDL_Init(SDL_INIT_VIDEO) == -1)
	{
		std::stringstream ss;
		ss << "[Start SDL] Error during initialize SDL: " << SDL_GetError();
		Logger::LogError(ss.str().c_str());
		return 1;
	}

	//
	// 2. step: Set up necessary OpenGL demands, create your window, start OpenGL
	//

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE, 32);
	SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);


	SDL_Window* win = 0;
	win = SDL_CreateWindow("Hello SDL&OpenGL!",
		100,
		100,
		640,
		480,
		SDL_WINDOW_OPENGL | /* SDL_WINDOW_FULLSCREEN_DESKTOP*/ SDL_WINDOW_MAXIMIZED |  SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);

	if (win == 0)
	{
		std::stringstream ss;
		ss << "[Generate window] Error during initialize SDL: " << SDL_GetError();
		Logger::LogError(ss.str().c_str());
		return 1;
	}

	SDL_GLContext context = SDL_GL_CreateContext(win);
	if (context == 0)
	{
		std::stringstream ss;
		ss << "[Init OGL context] Error during initialize SDL: " << SDL_GetError();
		Logger::LogError(ss.str().c_str());
		return 1;
	}

	SDL_GL_SetSwapInterval(1);

	GLenum error = glewInit();
	if (error != GLEW_OK)
	{
		Logger::LogError("[GLEW] Error during initialize!");
		return 1;
	}

	int glVersion[2] = { -1, -1 };
	glGetIntegerv(GL_MAJOR_VERSION, &glVersion[0]);
	glGetIntegerv(GL_MINOR_VERSION, &glVersion[1]);

	if (glVersion[0] == -1 && glVersion[1] == -1)
	{
		SDL_GL_DeleteContext(context);
		SDL_DestroyWindow(win);

		Logger::LogError("[Init OGL context] Failed to create OpenGL context! One of the settings may be incorrect for SDL_GL_SetAttribute (...) calls.");
		return 1;
	}

	SDL_Surface* icon_surface = IMG_Load(std::string(FileSystem::bin_path + "../icon.png").c_str());
	if(icon_surface)
	{
		SDL_SetWindowIcon(win, icon_surface);
		SDL_FreeSurface(icon_surface);
	}

	std::stringstream window_title;
	window_title << "N-Body Simulation -- OpenGL v " << glVersion[0] << "." << glVersion[1];
	SDL_SetWindowTitle(win, window_title.str().c_str());

	ImGui_ImplSdlGL3_Init(win);

	bool quit = false;
	SDL_Event ev;

	SDL_HideWindow(win);

	NBody app;

	if (!app.Init())
	{
		SDL_GL_DeleteContext(context);
		SDL_DestroyWindow(win);

		Logger::LogError("[app.Init] Error during initialize the app!");
		return 1;
	}

	SDL_ShowWindow(win);
	int width, heigth;
	SDL_GetWindowSize(win, &width, &heigth);
	app.SetWindowDimension(width, heigth);
	gTimer nanoTimer;
	app.StartSimulation();

#ifdef __WINDOWS__
	std::string imGuiIniPath = StringConverter::ISO_8859_2_TO_UTF_8(FileSystem::bin_path + "..\\imgui.ini");
#else
	std::string imGuiIniPath = FileSystem::bin_path + "../imgui.ini";
#endif

	ImGuiIO& g = ImGui::GetIO();
	g.IniFilename = imGuiIniPath.c_str();

	while (!quit)
	{
		while (SDL_PollEvent(&ev))
		{
			ImGui_ImplSdlGL3_ProcessEvent(&ev);
			bool is_mouse_captured = ImGui::GetIO().WantCaptureMouse; // does ImGui need the mouse
			bool is_keyboard_captured = ImGui::GetIO().WantCaptureKeyboard;	// does ImGui need the keyboard
			
			switch (ev.type)
			{
			case SDL_QUIT:
				quit = true;
				break;
			case SDL_KEYDOWN:
				if (ev.key.keysym.sym == SDLK_ESCAPE)
					quit = true;
				app.KeyboardDown(ev.key, is_keyboard_captured);
				break;
			case SDL_KEYUP:
				app.KeyboardUp(ev.key, is_keyboard_captured);
				break;
			case SDL_MOUSEBUTTONDOWN:
				app.MouseDown(ev.button, is_mouse_captured);
				break;
			case SDL_MOUSEBUTTONUP:
				app.MouseUp(ev.button);
				break;
			case SDL_MOUSEWHEEL:
				app.MouseWheel(ev.wheel, is_mouse_captured);
				break;
			case SDL_MOUSEMOTION:
				app.MouseMove(ev.motion);
				break;
			case SDL_WINDOWEVENT:
				if (ev.window.event == SDL_WINDOWEVENT_SIZE_CHANGED)
				{
					app.Resize(ev.window.data1, ev.window.data2);
				}
				break;
			}
		}
		ImGui_ImplSdlGL3_NewFrame(win);
		
		nanoTimer.Start();
		
		app.Update();
		app.Render();
		
		ImGui::Render();

		window_title.str(std::string());
		window_title.precision(4);
		window_title << "N-Body Simulation -- OpenGL " << glVersion[0] << "." << glVersion[1] << ", last frame: " << nanoTimer.StopMillis() << "ms";
		SDL_SetWindowTitle(win, window_title.str().c_str());

		SDL_GL_SwapWindow(win);
	}

	app.Clean();

	ImGui_ImplSdlGL3_Shutdown();
	SDL_GL_DeleteContext(context);
	SDL_DestroyWindow(win);
	SDL_Quit();

	return 0;
}