#pragma once

#include "Model.h"
#include "BHTreeBaseGPU.h"

class BHTree3D_GPU : public Model, public BHTreeBaseGPU
{
public:
	BHTree3D_GPU(const int&, const float&, const float&, const float&, const int&);
	~BHTree3D_GPU() {}
	virtual bool InitGPU();
	virtual void Update(const float);
	virtual void UpdateViaSolver(float*, float*, bool = false);
	virtual std::vector<glm::vec3> GetAxis();
private:
	// CL
	const char* source_code_file = "BHTree3D_GPU.cl";
};

