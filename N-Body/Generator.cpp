#include "Generator.h"

void Generator::SetIs3D(bool is_3d)
{
	this->is_3d = is_3d;
}

void Generator::SetGravitationalConstant(float gravitational_constant)
{
	this->gravitational_constant = gravitational_constant;
}

void Generator::SetBlackholeData(float x, float y, float z, float mass)
{
	blackhole_x = x;
	blackhole_y = y;
	blackhole_z = z;
	blackhole_mass = mass;
}

void Generator::SetMinMaxRange(float min, float max)
{
	this->min = min;
	this->max = max;
}

float Generator::GenerateUniformRandom()
{
	static std::default_random_engine generator;
	std::uniform_real_distribution<float> distribution(min, max);
	return distribution(generator);
}

float Generator::GenerateNonUniformRandom()
{
	static std::default_random_engine generator;
	std::normal_distribution<float> distribution(min, max);
	return distribution(generator);
}

void Generator::GenerateUniformRandomPoints(float& x, float& y, float& z, float& mass)
{
	x = GenerateUniformRandom();
	y = GenerateUniformRandom();
	if(is_3d)
	{
		z = GenerateUniformRandom();
	}
	mass = GenerateMass();
}

void Generator::GenerateNonUniformRandomPoints(float& x, float& y, float& z, float& mass)
{
	x = GenerateNonUniformRandom();
	y = GenerateNonUniformRandom();
	if(is_3d)
	{
		z = GenerateNonUniformRandom();
	}
	mass = GenerateMass();
}

void Generator::SetRingRadian(float radian)
{
	this->radian = radian;
}

void Generator::GenerateRingPoints(float& x, float& y, float& mass)
{
	float r = 0.1 + 0.8 * (radian * ((double)rand() / RAND_MAX));
	float a = 2.0 * M_PI * ((double)rand() / RAND_MAX);
	x = r * sin(a);
	y = r * cos(a);

	mass = GenerateMass();
}

void Generator::SetTorusData(int n, int m)
{
	this->torus_n = n;
	this->torus_m = m;
}

void Generator::SetTorusRadians(float torus_R, float torus_r)
{
	this->torus_R = torus_R;
	this->torus_r = torus_r;
}

void Generator::GenerateTorusPoints(float& x, float& y, float& z, float& mass, const int& i)
{
	const float u = (float(i / torus_n) / torus_n) * (2 * M_PI);
	const float v = (float(i % torus_m) / torus_m) * (2 * M_PI);

	const float su = sinf(u);
	const float cu = cosf(u);
	const float sv = sinf(v);
	const float cv = cosf(v);

	x = (torus_R + torus_r * cu) * cv;
	y = (torus_R + torus_r * cu) * sv;
	z = torus_r * su;
	
	mass = GenerateMass();
}

void Generator::GenerateVelocityPoints(const float& x, const float& y, const float& z, float& velocity_x, float& velocity_y, float& velocity_z)
{
	const float tmp_x = blackhole_x - x;
	const float tmp_y = blackhole_y - y;
	float tmp_z, distance;

	if (is_3d)
	{
		tmp_z = blackhole_z - z;
		distance = sqrt(tmp_x * tmp_x + tmp_y * tmp_y + tmp_z * tmp_z);
	}
	else
	{
		distance = sqrt(tmp_x * tmp_x + tmp_y * tmp_y);
	}

	const float orbital_velocity = sqrt(gravitational_constant * blackhole_mass / distance);

	velocity_x = (tmp_y / distance) * orbital_velocity;
	velocity_y = (-tmp_x / distance) * orbital_velocity;
	if (is_3d)
	{
		velocity_z = abs((tmp_z / distance) * orbital_velocity);
		if (z > blackhole_z)
		{
			velocity_z *= -1;
		}
	}
}

float Generator::GenerateMass()
{
	return 0.03 + 20 * ((double)rand() / RAND_MAX);
}