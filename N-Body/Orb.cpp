#include "Orb.h"

Orb::Orb(float r)
{
	this->r = r;
	Init();
}

Orb::Orb()
{
	this->r = 0.1f;
	Init();
}

Orb::~Orb() {}

void Orb::Clean()
{
	delete[] vert;
	delete[] indices;
}

/*
 * Get vertex array
 * @author Patrik Zsolt Furak
 * @return Vertex*
 */
Vertex* Orb::getVertex()
{
	return vert;
}

/*
 * Get vertex array size
 * @author Patrik Zsolt Furak
 * @return int
 */
int Orb::getVertexSize()
{
	return sizeof(Vertex) * vertex_size;
}

/*
 * Get vertex array index buffer
 * @author Patrik Zsolt Furak
 * @return GLushort*
 */
GLushort* Orb::getBuffer()
{
	return indices;
}

/*
 * Get vertex array index buffer size
 * @author Patrik Zsolt Furak
 * @return int
 */
int Orb::getBufferSize()
{
	return sizeof(GLushort) * buffer_size;
}

void Orb::Init()
{
	vert = new Vertex[vertex_size];
	for (int i = 0; i <= N; ++i) {
		for (int j = 0; j <= M; ++j)
		{
			int index = i + j * (N + 1);

			float u = i / (float)N;
			float v = j / (float)M;

			vert[index].p = GetPos(u, v);
			vert[index].c = glm::normalize(vert[index].p);
		}
	}

	indices = new GLushort[buffer_size];
	for (int i = 0; i < N; ++i)
	{
		for (int j = 0; j < M; ++j)
		{
			indices[6 * i + j * 3 * 2 * (N)+0] = (i)+(j) * (N + 1);
			indices[6 * i + j * 3 * 2 * (N)+1] = (i + 1) + (j) * (N + 1);
			indices[6 * i + j * 3 * 2 * (N)+2] = (i)+(j + 1) * (N + 1);
			indices[6 * i + j * 3 * 2 * (N)+3] = (i + 1) + (j) * (N + 1);
			indices[6 * i + j * 3 * 2 * (N)+4] = (i + 1) + (j + 1) * (N + 1);
			indices[6 * i + j * 3 * 2 * (N)+5] = (i)+(j + 1) * (N + 1);
		}
	}
}

/*
 * Generate parametric sphere positions
 * @author Patrik Zsolt Furak
 * @param float u
 * @param float v
 * @return glm::vec3
 */
glm::vec3 Orb::GetPos(float u, float v)
{
	u *= 2 * M_PI;
	v *= M_PI;
	float cu = cosf(u), su = sinf(u), cv = cosf(v), sv = sinf(v);
	return glm::vec3(r * cu * sv, r * cv, r * su * sv);
}