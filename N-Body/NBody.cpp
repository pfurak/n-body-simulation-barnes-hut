﻿#include "NBody.h"
#include "GLUtils.hpp"

NBody::NBody()
{	
	m_programID = 0;
	orb = Orb(body_radius);

	m_matWorld = glm::mat4(1.0f);

	randomizations = std::vector<const char*>();
	randomizations.push_back("Uniform random");
	randomizations.push_back("Non-Uniform random");
	randomizations.push_back("Ring");

	velocities_logic = std::vector<const char*>();
	velocities_logic.push_back("No initial velocity");
	velocities_logic.push_back("Orbital velocity");

	SetDefaultSettings();
	BaseGPU::SetAvailableDevices();
}

NBody::~NBody() {}

#pragma region Initialize
/*
 * Init openGL
 * @author Patrik Zsolt Furak
 */
bool NBody::Init()
{
	// window background color
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	glCullFace(GL_BACK);

	vertex = orb.getVertex();
	vertex_size = orb.getVertexSize();

	buffer = orb.getBuffer();
	buffer_size = orb.getBufferSize();

	glGenVertexArrays(1, &sphere_vao);
	glBindVertexArray(sphere_vao);
	glGenBuffers(1, &sphere_vbo);

	glBindBuffer(GL_ARRAY_BUFFER, sphere_vbo);
	glBufferData(GL_ARRAY_BUFFER, vertex_size, vertex, GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);

	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(sizeof(glm::vec3)));

	glGenBuffers(1, &sphere_buffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, sphere_buffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, buffer_size, buffer, GL_STATIC_DRAW);

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	
	m_matProj = glm::perspective(45.0f, 640 / 480.0f, 1.0f, 100000.0f);

	return true;
}

void NBody::InitGLProgram()
{
	std::string vertex_path = FileSystem::bin_path + (simulation_controller->is3D() ? "my3DVert.vert" : "myVert.vert");
	GLuint vs_ID = loadShader(GL_VERTEX_SHADER, vertex_path.c_str());

	std::string fragment_path = FileSystem::bin_path + "myFrag.frag";
	GLuint fs_ID = loadShader(GL_FRAGMENT_SHADER, fragment_path.c_str());

	m_programID = glCreateProgram();

	glAttachShader(m_programID, vs_ID);
	glAttachShader(m_programID, fs_ID);

	glBindAttribLocation(m_programID, 0, "vs_in_pos");
	glBindAttribLocation(m_programID, 1, "vs_in_col");

	glLinkProgram(m_programID);

	GLint infoLogLength = 0, result = 0;

	glGetProgramiv(m_programID, GL_LINK_STATUS, &result);
	glGetProgramiv(m_programID, GL_INFO_LOG_LENGTH, &infoLogLength);
	if (GL_FALSE == result || infoLogLength != 0)
	{
		std::vector<char> error_massage(infoLogLength);
		glGetProgramInfoLog(m_programID, infoLogLength, NULL, error_massage.data());

		std::stringstream ss;
		ss << "[Init openGL] Error during generate shaders";
		if (error_massage.size() > 0)
		{
			ss << ": " << error_massage.data();
		}
		Logger::LogError(ss.str().c_str());
	}

	glDeleteShader(vs_ID);
	glDeleteShader(fs_ID);

	m_loc_mvp = glGetUniformLocation(m_programID, "MVP");
	m_loc_offset = glGetUniformLocation(m_programID, "offsets");

	// ============================================================ //

	vertex_path = FileSystem::bin_path + (simulation_controller->is3D() ? "myAxis3D.vert" : "myAxis2D.vert");
	vs_ID = loadShader(GL_VERTEX_SHADER, vertex_path.c_str());

	fragment_path = FileSystem::bin_path + "myFrag.frag";
	fs_ID = loadShader(GL_FRAGMENT_SHADER, fragment_path.c_str());

	axis_program = glCreateProgram();

	glAttachShader(axis_program, vs_ID);
	glAttachShader(axis_program, fs_ID);

	glLinkProgram(axis_program);

	infoLogLength = result = 0;

	glGetProgramiv(axis_program, GL_LINK_STATUS, &result);
	glGetProgramiv(axis_program, GL_INFO_LOG_LENGTH, &infoLogLength);
	if (GL_FALSE == result || infoLogLength != 0)
	{
		std::vector<char> error_massage(infoLogLength);
		glGetProgramInfoLog(axis_program, infoLogLength, NULL, error_massage.data());

		std::stringstream ss;
		ss << "[Init openGL] Error during generate shaders";
		if (error_massage.size() > 0)
		{
			ss << ": " << error_massage.data();
		}
		Logger::LogError(ss.str().c_str());
	}

	glDeleteShader(vs_ID);
	glDeleteShader(fs_ID);

	axis_mvp = glGetUniformLocation(axis_program, "MVP");
	axis_min_coordinates = glGetUniformLocation(axis_program, "min_coordinates");
	axis_max_coordinates = glGetUniformLocation(axis_program, "max_coordinates");
}
#pragma endregion

#pragma region Simulation functions
/*
 * Generate positions, velocities and masses of particles
 * @author Patrik Zsolt Furak
 */
void NBody::GeneratePoints()
{
	float blackhole_xyz, second_blackhole_xy, second_blackhole_mass, x, y, z, velocity_x, velocity_y, velocity_z, mass;
	blackhole_xyz = x = y = z = velocity_x = velocity_y = velocity_z = mass = 0.0f;
	int stored_particle_counter = 1;

	generator.SetIs3D(simulation_controller->is3D());
	generator.SetGravitationalConstant(gravitational_constant);
	generator.SetBlackholeData(blackhole_xyz, blackhole_xyz, blackhole_xyz, blackhole_mass);

	// Register the first supermassive black hole
	RegisterParticle(blackhole_xyz, blackhole_xyz, blackhole_xyz, velocity_x, velocity_y, velocity_z, blackhole_mass);

	if(current_randomization_index == 2) // RING
	{
		int multiplier = number_of_particles / 5000 / 2;
		if (multiplier < 1)
		{
			multiplier = 1;
		}

		const int n = number_of_particles / 5;
		const int first_blackhole_particles = n * 4;
		second_blackhole_mass = blackhole_mass / 10;

		if(simulation_controller->is3D())
		{
			int torus_n = sqrt(first_blackhole_particles - 1);
			int torus_m = (first_blackhole_particles - 1) / torus_n;
			generator.SetTorusData(torus_n, torus_m);

			const float R = 3.75 * multiplier;
			const float r = 2.75 * multiplier;
			generator.SetTorusRadians(R, r);

			while(stored_particle_counter < first_blackhole_particles)
			{
				generator.GenerateTorusPoints(x, y, z, mass, stored_particle_counter - 1);
				if (current_velocity_logic_index == 1)
				{
					generator.GenerateVelocityPoints(x, y, z, velocity_x, velocity_y, velocity_z);
				}
				RegisterParticle(x, y, z, velocity_x, velocity_y, velocity_z, mass);

				stored_particle_counter++;
			}

			// Register the second supermassive black hole
			second_blackhole_xy = 8 * multiplier;
			
			if (current_velocity_logic_index == 1)
			{
				generator.GenerateVelocityPoints(second_blackhole_xy, second_blackhole_xy, 0, velocity_x, velocity_y, velocity_z);
				velocity_x *= 0.9;
				velocity_y *= 0.9;
				velocity_z *= 0.9;
			}
			RegisterParticle(second_blackhole_xy, second_blackhole_xy, 0, velocity_x, velocity_y, velocity_z, second_blackhole_mass);
			stored_particle_counter++;
			generator.SetBlackholeData(second_blackhole_xy, second_blackhole_xy, 0, second_blackhole_mass);

			int second_blackhole_particles = number_of_particles - stored_particle_counter;
			torus_n = sqrt(second_blackhole_particles);
			torus_m = second_blackhole_particles / torus_n;
			generator.SetTorusData(torus_n, torus_m);

			generator.SetTorusRadians((R / 10) * 3, (r / 10) * 3);

			while(stored_particle_counter < number_of_particles)
			{
				generator.GenerateTorusPoints(x, y, z, mass, stored_particle_counter);
				x += second_blackhole_xy;
				y += second_blackhole_xy;

				if (current_velocity_logic_index == 1)
				{
					generator.GenerateVelocityPoints(x, y, z, velocity_x, velocity_y, velocity_z);
				}
				RegisterParticle(x, y, z, velocity_x, velocity_y, velocity_z, mass);

				stored_particle_counter++;
			}
		}
		else
		{
			generator.SetRingRadian(10 * multiplier);
			while(stored_particle_counter < first_blackhole_particles)
			{
				generator.GenerateRingPoints(x, y, mass);

				if (current_velocity_logic_index == 1)
				{
					generator.GenerateVelocityPoints(x, y, z, velocity_x, velocity_y, velocity_z);
				}
				RegisterParticle(x, y, z, velocity_x, velocity_y, velocity_z, mass);

				stored_particle_counter++;
			}

			// Register the second supermassive black hole
			second_blackhole_xy = 10 * multiplier;
			if (current_velocity_logic_index == 1)
			{
				generator.GenerateVelocityPoints(second_blackhole_xy, second_blackhole_xy, 0, velocity_x, velocity_y, velocity_z);
				velocity_x *= 0.9;
				velocity_y *= 0.9;
			}
			RegisterParticle(second_blackhole_xy, second_blackhole_xy, 0, velocity_x, velocity_y, velocity_z, second_blackhole_mass);
			stored_particle_counter++;
			generator.SetBlackholeData(second_blackhole_xy, second_blackhole_xy, 0, second_blackhole_mass);

			generator.SetRingRadian(3 * multiplier);
			
			while(stored_particle_counter < number_of_particles)
			{
				generator.GenerateRingPoints(x, y, mass);
				x += second_blackhole_xy;
				y += second_blackhole_xy;

				if (current_velocity_logic_index == 1)
				{
					generator.GenerateVelocityPoints(x, y, z, velocity_x, velocity_y, velocity_z);
				}
				RegisterParticle(x, y, z, velocity_x, velocity_y, velocity_z, mass);

				stored_particle_counter++;
			}
		}
	}
	else
	{
		float limit = number_of_particles;
		if (number_of_particles > 10000)
		{
			limit /= 10000;
		}
		else if (number_of_particles > 1000)
		{
			limit /= 1000;
		}
		else
		{
			limit /= 100;
		}
		generator.SetMinMaxRange(-limit, limit);

		for (int i = stored_particle_counter; i < number_of_particles; i++)
		{
			if (current_randomization_index == 0) // Uniform random
			{
				generator.GenerateUniformRandomPoints(x, y, z, mass);
			}
			else if (current_randomization_index == 1) // Non-Uniform random
			{
				generator.GenerateNonUniformRandomPoints(x, y, z, mass);
			}

			if (current_velocity_logic_index == 1)
			{
				generator.GenerateVelocityPoints(x, y, z, velocity_x, velocity_y, velocity_z);
			}

			RegisterParticle(x, y, z, velocity_x, velocity_y, velocity_z, mass);
		}
	}
}

void NBody::RegisterParticle(float x, float y, float z, float velocity_x, float velocity_y, float velocity_z, float mass)
{
	simulation_controller->RegisterParticle(x, y, z, mass, velocity_x, velocity_y, velocity_z);
	SetMinMaxCoordinates(x, y, z);
}

/*
 * Set min and max x y coordinates
 * @author Patrik Zsolt Furak
 * @param float x
 * @param float y
 */
void NBody::SetMinMaxCoordinates(float x, float y, float z = 0.0f)
{
	if (x > max_x) max_x = x;
	if (x < min_x) min_x = x;
	if (y > max_y) max_y = y;
	if (y < min_y) min_y = y;
	if (z > max_z) max_z = z;
	if (z < min_z) min_z = z;
}

void NBody::SetDefaultSettings()
{
	number_of_particles = 5000;
	theta = 0.9f;
	gravitational_constant = (float)(default_grav_constant / pow(parsec_to_meter, 3) * mass_of_sun * pow(days_in_a_year * seconds_in_a_day, 2));

	current_algorithm_index = new_algorithm_index = 0;
	current_randomization_index = new_randomization_index = 2;
	current_velocity_logic_index = new_velocity_logic_index = 1;
	current_device_index = new_device_index = 0;
	
	render_imgui = center_of_mass_logic = use_solver = true;
	paused = render_GPU_information = false;
}

/*
 * Start simulation
 * @author Patrik Zsolt Furak
 */
void NBody::StartSimulation()
{
	max_x = min_x = max_y = min_y = max_z = min_z = 0;

	instanced_draw_particles = number_of_particles / instanced_draw_max_size;
	instanced_draw_mod_particles = number_of_particles % instanced_draw_max_size;

	current_algorithm_index = new_algorithm_index;
	current_randomization_index = new_randomization_index;
	current_velocity_logic_index = new_velocity_logic_index;
	current_device_index = new_device_index;
	
	delta_time_speed = 1;

	bodies_input_text_buffer = std::to_string(number_of_particles);
	//gravitational_constant_input_text_buffer = std::to_string(gravitational_constant);
	std::ostringstream ss;
	ss << gravitational_constant;
	std::string s(ss.str());
	gravitational_constant_input_text_buffer = s;

	ss.str("");
	ss.clear();
	ss << theta;
	s = std::string(ss.str());
	theta_input_text_buffer = s;

	simulation_controller = new SimulationController(
		current_algorithm_index, number_of_particles,
		gravitational_constant, theta, lambda,
		center_of_mass_logic, use_solver);
	simulation_controller->Init(current_device_index);

	InitGLProgram();
	GeneratePoints();
	if (simulation_controller->isGPU())
	{
		render_GPU_information = true;
		simulation_controller->InitGPU();
	}
	else
	{
		render_GPU_information = false;
	}
	if (use_solver)
	{
		simulation_controller->InitSolverState();
	}
	camera.ResetCamera();
	float z;
	if(simulation_controller->is3D())
	{
		if (current_randomization_index == 2) // RING
		{
			z = max_z + (max_z - min_z) * 4;
		}
		else
		{
			z = max_z;
		}
	}
	else
	{
		z = 20;
	}
	camera.SetCameraPos(max_x - (max_x - min_x) / 2, max_y - (max_y - min_y) / 2, z);

	particle_draw_multiplier = simulation_controller->is3D() ? 3 : 2;
	render_axis = false;
}
#pragma endregion

#pragma region Update
/*
 * Set min and max coordinates, set blocks particles, update particles positions
 * @author Patrik Zsolt Furak
 */
void NBody::Update()
{
	m_matView = camera.GetView();

	if(simulation_controller->isGPU() && !simulation_controller->GPUInited())
	{
		return;
	}

	if(!paused)
	{
		static Uint32 last_time = SDL_GetTicks();
		float delta_time = (SDL_GetTicks() - last_time) / 1000.0f;
		if (delta_time < 0.0001f) delta_time = 0.0001f;
		if (delta_time > 0.1f) delta_time = 0.1f;

		simulation_controller->Update(delta_time * delta_time_speed);

		last_time = SDL_GetTicks();
	}
}
#pragma endregion

#pragma region Render
/*
 * Render particles and imGUI
 * @author Patrik Zsolt Furak
 */
void NBody::Render()
{
	glm::mat4 mvp = m_matProj * m_matView * m_matWorld;

	// Reset buffer 
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// enable shader and VAO
	glUseProgram(m_programID);
	glBindVertexArray(sphere_vao);

	glUniformMatrix4fv(m_loc_mvp, 1, GL_FALSE, &mvp[0][0]);
	RenderParticles();

	// disable VAO and shader program
	glBindVertexArray(0);
	glUseProgram(0);

	if(render_imgui)
	{
		RenderImGui();
	}

	if (render_axis)
	{
		RenderAxis(mvp);
	}
}

/*
 * Render particles
 * @author Patrik Zsolt Furak
 */
void NBody::RenderParticles()
{
	auto (*fn) = simulation_controller->is3D() ? glUniform3fv : glUniform2fv;
	const float* positions = simulation_controller->GetPositions();

	if (number_of_particles <= instanced_draw_max_size)
	{
		fn(m_loc_offset, number_of_particles, &positions[0]);
		glDrawElementsInstanced(GL_TRIANGLES, buffer_size, GL_UNSIGNED_SHORT, 0, number_of_particles);
	}
	else
	{
		int pos_index;
		for (int i = 0; i < instanced_draw_particles; i++)
		{
			pos_index = i * instanced_draw_max_size * particle_draw_multiplier;
			fn(m_loc_offset, instanced_draw_max_size, &positions[pos_index]);
			glDrawElementsInstanced(GL_TRIANGLES, buffer_size, GL_UNSIGNED_SHORT, 0, instanced_draw_max_size);
		}

		if (instanced_draw_mod_particles > 0)
		{
			pos_index += instanced_draw_max_size * particle_draw_multiplier;
			fn(m_loc_offset, instanced_draw_mod_particles, &positions[pos_index]);
			glDrawElementsInstanced(GL_TRIANGLES, buffer_size, GL_UNSIGNED_SHORT, 0, instanced_draw_mod_particles);
		}
	}
}

/*
 * Render imGUI
 * @author Patrik Zsolt Furak
 */
void NBody::RenderImGui()
{
	if (!ImGui::Begin("Configuration window"))
	{
		ImGui::End();
	}
	else
	{
		// Speed
		ImGui::SliderInt("Simulation Speed", &delta_time_speed, 1, 10000);

		// Number of particles
		ImGui::InputText("Number of bodies", bodies_input_text_buffer, sizeof(bodies_input_text_buffer));
		ImGui::Text("Min bodies: %i, max bodies: %i", min_number_of_particles, max_number_of_particles);

		// Gravitational constant
		ImGui::InputText("Gravitational constant", gravitational_constant_input_text_buffer, sizeof(gravitational_constant_input_text_buffer));

		// Theta
		if(new_algorithm_index < 2 || (new_algorithm_index > 1 && center_of_mass_logic))
		{
			ImGui::InputText("Theta", theta_input_text_buffer, sizeof(theta_input_text_buffer));
		}

		// Algorithm
		ImGui::Text("Algorithms: ");
		for (int i = 0; i < SimulationController::algorithms.size(); i++)
		{
			if(SimulationController::algorithms[i].is_gpu)
			{
				if(BaseGPU::available_devices.size() > 0)
				{
					ImGui::RadioButton(SimulationController::algorithms[i].name, &new_algorithm_index, i);
				}
			}
			else
			{
				ImGui::RadioButton(SimulationController::algorithms[i].name, &new_algorithm_index, i);
			}
			
			if (i == 3)
			{
				ImGui::NewLine();
			}
			ImGui::SameLine();
		}
		
		render_GPU_information = (new_algorithm_index > 1 && new_algorithm_index < 4) || new_algorithm_index > 5;
		ImGui::NewLine();
		if (current_algorithm_index != new_algorithm_index)
		{
			ImGui::Text("Algorithm will be changed after restart...");
		}

		// Randomizations
		ImGui::Text("Randomization: ");
		ImGui::SameLine();
		for (int i = 0; i < randomizations.size(); i++)
		{
			ImGui::RadioButton(randomizations[i], &new_randomization_index, i);
			if (i + 1 < randomizations.size())
			{
				ImGui::SameLine();
			}
		}
		if (current_randomization_index != new_randomization_index)
		{
			ImGui::Text("Randomization will be changed after restart...");
		}

		// Velocity logic
		ImGui::Text("Velocity logic: ");
		ImGui::SameLine();
		for (int i = 0; i < velocities_logic.size(); i++)
		{
			ImGui::RadioButton(velocities_logic[i], &new_velocity_logic_index, i);
			if (i + 1 < velocities_logic.size())
			{
				ImGui::SameLine();
			}
		}
		if (current_velocity_logic_index != new_velocity_logic_index)
		{
			ImGui::Text("Velocity logic will be changed after restart...");
		}

		// Center of Mass logic
		if (new_algorithm_index > 3)
		{
			ImGui::Text("Particle positions calculate with: ");
			ImGui::SameLine();

			int render_center_of_mass_logic = center_of_mass_logic ? 1 : 0;
			ImGui::RadioButton("Center of Mass", &render_center_of_mass_logic, 1);
			ImGui::SameLine();
			ImGui::RadioButton("Block's neighbors", &render_center_of_mass_logic, 0);
			center_of_mass_logic = render_center_of_mass_logic == 1;
			if (center_of_mass_logic != simulation_controller->GetCenterOfMassLogic())
			{
				ImGui::Text("Particle positions calculate logic will be changed after restart...");
			}
		}

		// Solver
		ImGui::Text("Use Adams-Bashforth 6 steps method: ");
		ImGui::SameLine();
		int render_solver = use_solver ? 1 : 0;
		ImGui::RadioButton("Yes", &render_solver, 1);
		ImGui::SameLine();
		ImGui::RadioButton("No", &render_solver, 0);
		use_solver = render_solver == 1;
		if (use_solver != simulation_controller->SolverInUsed())
		{
			ImGui::Text("Adams-Bashforth method will be enabled/disabled after restart...");
		}

		if (BaseGPU::available_devices.size() > 0)
		{
			if (render_GPU_information)
			{
				ImGui::Text("GPU device: ");
				if(BaseGPU::available_devices.size() == 1)
				{
					ImGui::SameLine();
				}

				for (int i = 0; i < BaseGPU::available_devices.size(); i++)
				{
					const std::string device_name = std::string(BaseGPU::available_devices[i].getInfo<CL_DEVICE_NAME>() + "(" + BaseGPU::available_devices[i].getInfo<CL_DEVICE_VERSION>() + " - " + BaseGPU::available_devices[i].getInfo<CL_DEVICE_OPENCL_C_VERSION>() + ")");
					ImGui::RadioButton(device_name.c_str(), &new_device_index, i);
					/*
					if (i + 1 < BaseGPU::available_devices.size())
					{
						ImGui::SameLine();
					}
					*/
				}

				if (current_device_index != new_device_index)
				{
					ImGui::Text("Device will be changed after restart...");
				}
			}
		}
		else
		{
			ImGui::TextColored(ImColor(255, 100, 100), "Cannot find GPU device");
		}

		if(simulation_controller->isGPU() && !simulation_controller->GPUInited())
		{
			ImGui::TextColored(ImColor(255,100,100), "Cannot init GPU device");
		}

		if (ImGui::Button("Start simulation with the following parameters"))
		{
			int a = atoi(bodies_input_text_buffer.c_str());
			if (a != number_of_particles)
			{
				if (a > max_number_of_particles)
				{
					number_of_particles = max_number_of_particles;
				}
				else if (a < min_number_of_particles)
				{
					number_of_particles = min_number_of_particles;
				}
				else
				{
					number_of_particles = a;
				}
			}

			float g = atof(gravitational_constant_input_text_buffer.c_str());
			if (g != gravitational_constant && g != 0)
			{
				gravitational_constant = g;
			}

			g = atof(theta_input_text_buffer.c_str());
			if (g != theta && g != 0)
			{
				theta = g;
			}

			paused = false;

			ReStartSimulation();
		}

		ImGui::SameLine();
		if (ImGui::Button("Reset to default"))
		{
			SetDefaultSettings();
			ReStartSimulation();
		}

		if(simulation_controller->SolverInUsed())
		{
			ImGui::TextColored(ImColor(0, 255, 63), "Press \"R\" to reverse the orbital velocity");
		}
		ImGui::TextColored(ImColor(0, 255, 63), "Press \"T\" to show/hide the axis of cells");
		ImGui::TextColored(ImColor(0, 255, 63), "Press \"C\" to show/hide the configuration window");
		ImGui::TextColored(ImColor(0, 255, 63), "Press \"P\" to pause/continue the simulation");
		ImGui::TextColored(ImColor(0, 255, 63), "Press \"SPACE\" to create print screen");

		if (ImGui::Button("Open Print Screens"))
		{
			FileSystem::OpenWindowExplorer(FileSystem::print_screen_path);
		}
		ImGui::End();
	}
}

void NBody::RenderAxis(glm::mat4 mvp)
{
	std::vector<glm::vec3> axis = simulation_controller->GetAxis();
	if (axis.size() > 0)
	{
		const int draw_counter = simulation_controller->is3D() ? 32 : 8;

		std::vector<glm::vec3> min_coordinates = std::vector<glm::vec3>();
		std::vector<glm::vec3> max_coordinates = std::vector<glm::vec3>();

		for (int i = 0; i < axis.size(); i += 2)
		{
			min_coordinates.push_back(axis[i]);
			max_coordinates.push_back(axis[i + 1]);
		}

		// enable shader and VAO
		glUseProgram(axis_program);
#ifdef __APPLE__
		glBindVertexArray(sphere_vao);
#endif

		glUniformMatrix4fv(axis_mvp, 1, GL_FALSE, &mvp[0][0]);

		const int offset_size = instanced_draw_max_size / 2;
		if (max_coordinates.size() < offset_size)
		{
			glUniform3fv(axis_min_coordinates, max_coordinates.size(), &min_coordinates[0].x);
			glUniform3fv(axis_max_coordinates, max_coordinates.size(), &max_coordinates[0].x);
			glDrawArrays(GL_LINES, 0, draw_counter * max_coordinates.size());
		}
		else
		{
			const int n = max_coordinates.size() / offset_size;
			for (int i = 0; i < n; i++)
			{
				glUniform3fv(axis_min_coordinates, offset_size, &min_coordinates[i * offset_size].x);
				glUniform3fv(axis_max_coordinates, offset_size, &max_coordinates[i * offset_size].x);
				glDrawArrays(GL_LINES, 0, draw_counter * offset_size);
			}

			const int m = max_coordinates.size() % 500;
			if (m > 0)
			{
				glUniform3fv(axis_min_coordinates, m, &min_coordinates[n * offset_size].x);
				glUniform3fv(axis_max_coordinates, m, &max_coordinates[n * offset_size].x);
				glDrawArrays(GL_LINES, 0, draw_counter * m);
			}
		}

#ifdef __APPLE__
		glBindVertexArray(0);
#endif
		glUseProgram(0);

		axis.clear();
		min_coordinates.clear();
		max_coordinates.clear();
	}
}

void NBody::ReStartSimulation()
{
	StopSimulation();
	StartSimulation();
}

#pragma region etc
/*
 * Clean app variables
 * @author Patrik Zsolt Furak
 */
void NBody::StopSimulation()
{
	delete simulation_controller;
}

void NBody::Clean()
{
	StopSimulation();
	SimulationController::algorithms.clear();
	BaseGPU::available_devices.clear();

	orb.Clean();
	randomizations.clear();
	velocities_logic.clear();
	vertex = NULL;
	buffer = NULL;

	glDeleteBuffers(1, &sphere_vbo);
	glDeleteBuffers(1, &sphere_buffer);
	glDeleteVertexArrays(1, &sphere_vao);

	glDeleteProgram(m_programID);
	glDeleteProgram(axis_program);
}

void NBody::PrintScreen()
{
	if (FileSystem::CreateDir(FileSystem::print_screen_path) == 0)
	{
		const std::time_t now = std::time(nullptr);
		std::stringstream time;
		
		time << std::put_time(std::localtime(&now), "%Y_%m_%d_%H_%M_%S");
		std::string image = FileSystem::print_screen_path + "print_screen_" + time.str() + ".bmp";

#ifdef __WINDOWS__
		image = StringConverter::ISO_8859_2_TO_UTF_8(image);
#endif

		SDL_Surface* surface = SDL_CreateRGBSurface(SDL_SWSURFACE, window_width, window_height, 24, 0x000000FF, 0x0000FF00, 0x00FF0000, 0);
		glReadBuffer(GL_FRONT);
		glReadPixels(0, 0, window_width, window_height, GL_RGB, GL_UNSIGNED_BYTE, surface->pixels);
		InvertSurfaceVertically(surface);
		if (SDL_SaveBMP(surface, image.c_str()) != 0)
		{
			std::string error_message("[PrintScreen] Cannot create print screen. Error message: " + std::string(SDL_GetError()));
			Logger::LogError(error_message.c_str());
		}
		SDL_FreeSurface(surface);
	}
}

void NBody::InvertSurfaceVertically(SDL_Surface* surface)
{
	Uint8 *a, *b, *last;

	Uint16 pitch = surface->pitch;
	Uint8* row = (Uint8*)malloc(pitch);

	memcpy(row, surface->pixels, pitch);

	a = (Uint8*)surface->pixels;
	last = a + pitch * (surface->h - 1);
	b = last;

	while (a < b) {
		memcpy(a, b, pitch);
		a += pitch;
		memcpy(b, a, pitch);
		b -= pitch;
	}

	memmove(b, b + pitch, last - b);
	memcpy(last, row, pitch);
	free(row);
}

void NBody::KeyboardDown(SDL_KeyboardEvent& key, bool in_imGui)
{
	if (!in_imGui)
	{
		if (key.keysym.sym == SDLK_c)
		{
			render_imgui = !render_imgui;
		}
		else if (key.keysym.sym == SDLK_r)
		{
			simulation_controller->ReverseSolver();
		}
		else if (key.keysym.sym == SDLK_t)
		{
			render_axis = !render_axis;
		}
		else if(key.keysym.sym == SDLK_p)
		{
			paused = !paused;
		}
		else
		{
			camera.KeyboardDown(key);
		}
	}
}

void NBody::KeyboardUp(SDL_KeyboardEvent& key, bool in_imGui)
{
	if(!in_imGui)
	{
		if(key.keysym.sym == SDLK_SPACE)
		{
			PrintScreen();
		}
	}
}

void NBody::MouseMove(SDL_MouseMotionEvent& mouse)
{
	camera.MouseMove(mouse.x, mouse.y);
}

void NBody::MouseDown(SDL_MouseButtonEvent& mouse, bool in_imGui)
{
	if (!in_imGui)
	{
		camera.MouseDown(mouse.button, mouse.x, mouse.y);
	}
}

void NBody::MouseUp(SDL_MouseButtonEvent& mouse)
{
	camera.MouseUp(mouse.button);
}

void NBody::MouseWheel(SDL_MouseWheelEvent& wheel, bool in_imGui)
{
	if (!in_imGui)
	{
		camera.MouseWheel(wheel.y);
	}
}

void NBody::SetWindowDimension(int width, int height)
{
	window_width = width;
	window_height = height;
}

/*
 * Set viewport and prespective
 * @author Patrik Zsolt Furak
 * @param int width
 * @param int height
 */
void NBody::Resize(int width, int height)
{
	glViewport(0, 0, width, height);
	SetWindowDimension(width, height);

	m_matProj = glm::perspective(45.0f, width / (float)height, 0.01f, 100000.0f);
}
#pragma endregion