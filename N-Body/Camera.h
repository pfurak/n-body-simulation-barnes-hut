#pragma once

// SDL
#include <SDL.h>

// GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

class Camera
{
public:
	Camera();
	~Camera(void);

	void ResetCamera();
	void SetCameraPos(float x, float y, float z);
	glm::mat4 GetView();
	void ZoomOut();
	void ZoomIn();
	void KeyboardDown(SDL_KeyboardEvent& key);
	void MouseMove(float mouse_x, float mouse_y);
	void MouseDown(int mouseButton, float mouse_x, float mouse_y);
	void MouseUp(int mouseButton);
	void MouseWheel(int wheel_y);

private:
	float cameraPos_default_x;			// camera eye default x position
	float cameraPos_default_y;			// camera eye default y position
	float cameraPos_default_z;			// camera eye default z position

	float cameraFront_default_x;		// camera front default x position
	float cameraFront_default_y;		// camera front default y position
	float cameraFront_default_z;		// camera front default z position
	
	float cameraUp_default_x;			// camera up default x position
	float cameraUp_default_y;			// camera up default y position
	float cameraUp_default_z;			// camera up default z position

	const float cameraSpeed = 5.0f;		// camera speed
	const float sensitivity = 0.1f;		// sensitivity of camera

	glm::vec3 cameraPos;				// camera eye
	glm::vec3 cameraFront;				// camera front
	glm::vec3 cameraUp;					// camera normalized up vector

	bool mouseDown = false;
	float lastX;						// mouse last x position
	float lastY;						// mouse last y position
	float yaw;							// radian. used in MouseMove function
	float pitch;						// radian. used in MouseMove function
};