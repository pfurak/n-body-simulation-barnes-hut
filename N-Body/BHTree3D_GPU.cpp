#include "BHTree3D_GPU.h"

BHTree3D_GPU::BHTree3D_GPU(const int& number_of_particles, const float& gravitational_constant, const float& theta, const float& lambda, const int& device_index) :
	Model(number_of_particles, gravitational_constant, theta, lambda, true),
	BHTreeBaseGPU(number_of_particles, gravitational_constant, theta, lambda, true, device_index)
{}

bool BHTree3D_GPU::InitGPU()
{
	BHTreeBaseGPU::InitGPU(source_code_file, min_x, max_x, min_y, max_y, this->GetPositions(), this->GetVelocities(), this->GetMasses(), min_z, max_z);
	return gpu_inited;
}

void BHTree3D_GPU::Update(float delta_time)
{
	if (gpu_inited)
	{
		BuildTree(1);
		CalculateParticlePosition(10, delta_time);
		ReadBackPositions(this->GetPositions());
	}
}

void BHTree3D_GPU::UpdateViaSolver(float* state, float* step, bool skip)
{
	if (gpu_inited)
	{
		if (!skip)
		{
			this->CopyDataFromSolverState(state);
		}

		WriteIntoPositionsBuffer(this->GetPositions());
		ResetMinMaxBuffers(min_x, max_x, min_y, max_y, min_z, max_z);
		SetSolverBuffers(state, step);

		BuildTree();
		CalculateParticlePositionViaSolver();
		ReadBackPositions(this->GetPositions());

		ReadBackSolverSteps(step);
	}
}

std::vector<glm::vec3> BHTree3D_GPU::GetAxis()
{
	std::vector<glm::vec3> axis;
	if (gpu_inited)
	{
		int bottom;
		command_queue.enqueueReadBuffer(cl_bottom, CL_TRUE, 0, sizeof(int), &bottom);
		if (bottom > 0)
		{
			int vector_size = number_of_nodes + 1;
			size_t size = sizeof(float) * vector_size;

			std::vector<float> nodes_min_x_positions = std::vector<float>(vector_size);
			command_queue.enqueueReadBuffer(cl_nodes_min_x_positions, CL_TRUE, 0, size, &nodes_min_x_positions[0]);

			std::vector<float> nodes_max_x_positions = std::vector<float>(vector_size);
			command_queue.enqueueReadBuffer(cl_nodes_max_x_positions, CL_TRUE, 0, size, &nodes_max_x_positions[0]);

			std::vector<float> nodes_min_y_positions = std::vector<float>(vector_size);
			command_queue.enqueueReadBuffer(cl_nodes_min_y_positions, CL_TRUE, 0, size, &nodes_min_y_positions[0]);

			std::vector<float> nodes_max_y_positions = std::vector<float>(vector_size);
			command_queue.enqueueReadBuffer(cl_nodes_max_y_positions, CL_TRUE, 0, size, &nodes_max_y_positions[0]);

			std::vector<float> nodes_min_z_positions = std::vector<float>(vector_size);
			command_queue.enqueueReadBuffer(cl_nodes_min_z_positions, CL_TRUE, 0, size, &nodes_min_z_positions[0]);

			std::vector<float> nodes_max_z_positions = std::vector<float>(vector_size);
			command_queue.enqueueReadBuffer(cl_nodes_max_z_positions, CL_TRUE, 0, size, &nodes_max_z_positions[0]);

			for (int i = bottom; i < vector_size; i++)
			{
				axis.push_back(glm::vec3(nodes_min_x_positions[i], nodes_min_y_positions[i], nodes_min_z_positions[i]));
				axis.push_back(glm::vec3(nodes_max_x_positions[i], nodes_max_y_positions[i], nodes_max_z_positions[i]));
			}

			nodes_min_x_positions.clear();
			nodes_max_x_positions.clear();
			nodes_min_y_positions.clear();
			nodes_max_y_positions.clear();
			nodes_min_z_positions.clear();
			nodes_max_z_positions.clear();
		}
	}
	return axis;
}