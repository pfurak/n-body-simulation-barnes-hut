#pragma once

#include "Model.h"

class BHTree3D : public Model
{
public:
	BHTree3D(const int&, const float&, const float&, const float&);
	~BHTree3D();
	virtual void Update(const float);
	virtual void UpdateViaSolver(float*, float*, bool = false);
	virtual std::vector<glm::vec3> GetAxis();
private:
	BHTreeNode3D* root_node;

    void BuildTree();
    void UpdateParticlePositions(const float);
};