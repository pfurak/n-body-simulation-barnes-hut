#pragma once

#include <SDL_stdinc.h>
#include <random>

class Generator
{
public:
	void SetIs3D(bool);
	void SetGravitationalConstant(float);
	void SetBlackholeData(float, float, float, float);

	void SetMinMaxRange(float, float);
	void GenerateUniformRandomPoints(float&, float&, float&, float&);
	void GenerateNonUniformRandomPoints(float&, float&, float&, float&);

	void SetRingRadian(float);
	void GenerateRingPoints(float&, float&, float&);

	void SetTorusData(int, int);
	void SetTorusRadians(float, float);
	void GenerateTorusPoints(float&, float&, float&, float&, const int&);
	
	void GenerateVelocityPoints(const float&, const float&, const float&, float&, float&, float&);
private:
	float blackhole_x;
	float blackhole_y;
	float blackhole_z;
	float blackhole_mass;
	float gravitational_constant;

	float min;
	float max;

	float radian;

	int torus_n;
	int torus_m;
	float torus_R;
	float torus_r;

	bool is_3d;

	float GenerateUniformRandom();		//	generate uniform random between min and max
	float GenerateNonUniformRandom();	//	generate non-uniform random between min and max
	float GenerateMass();
};

