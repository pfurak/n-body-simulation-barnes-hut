#include "FileSystem.h"

std::string FileSystem::app_path = "";
std::string FileSystem::bin_path = "";
std::string FileSystem::print_screen_path = "";

void FileSystem::SetPathVariables()
{
	app_path = GetAppPath();
#ifdef  __WINDOWS__
	bin_path = app_path + "bin\\";
	print_screen_path = app_path + "print_screen\\";

#elif defined __APPLE__
	#ifdef __BUNDLE__	// compiler flag -> comes from makefile
		std::string::size_type pos = app_path.find_last_of("Contents/");
		if (pos != std::string::npos)
		{
			bin_path = std::string(app_path).substr(0, pos);
			pos = bin_path.find_last_of("/");
			bin_path = std::string(bin_path).substr(0, pos) + "/Resources/bin/";
		}

		pos = app_path.find(".app/");
		if (pos != std::string::npos)
		{
			app_path = std::string(app_path).substr(0, pos);
			pos = app_path.find_last_of("/");
			app_path = std::string(app_path).substr(0, pos) + "/";
		}
	#else
		bin_path = app_path + "bin/";
	#endif
	print_screen_path = app_path + "print_screen/";
#else
	bin_path = app_path + "bin/";
	print_screen_path = app_path + "print_screen/";
#endif
}

std::string FileSystem::GetAppPath()
{
#ifdef __APPLE__
	uint32_t MAX_PATH = 4096;
	char buffer[MAX_PATH];
	std::string path = std::string("");
	if (!_NSGetExecutablePath(buffer, &MAX_PATH))
	{
		path = std::string(buffer);
		std::string::size_type pos = path.find_last_of("/");
		path = std::string(path).substr(0, pos) + "/";
	}
	return path;
#elif defined __WINDOWS__
	char buffer[MAX_PATH];
	GetModuleFileNameA(NULL, buffer, MAX_PATH);
	std::string::size_type pos = std::string(buffer).find_last_of("\\/");
	std::string path = std::string(buffer).substr(0, pos);
	return path + "\\";
#else
	char result[PATH_MAX];
	ssize_t count = readlink("/proc/self/exe", result, PATH_MAX);
	std::string path = std::string("");
	if (count > 0)
	{
		path = std::string(result, count);
		std::string::size_type pos = path.find_last_of("/");
		path = std::string(path).substr(0, pos) + "/";
	}
	return path;
#endif;
}

int FileSystem::CreateDir(const std::string& path)
{
#ifdef __WINDOWS__
	int string_length = (int)path.length() + 1;
	int length = MultiByteToWideChar(CP_ACP, 0, path.c_str(), string_length, 0, 0);
	wchar_t* buf = new wchar_t[length];
	MultiByteToWideChar(CP_ACP, 0, path.c_str(), string_length, buf, length);
	std::wstring folder(buf);
	delete[] buf;

	int result = CreateDirectory(folder.c_str(), NULL);
	if (result == 1)
	{
		return 0;
	}

	DWORD error = GetLastError();
	if (error == ERROR_ALREADY_EXISTS)
	{
		return 0;
	}

	std::stringstream ss;
	ss << "[CreateDir] Cannot create folder (" << path << "). Error code: " << error;
	Logger::LogError(ss.str().c_str());
	return -1;
#elif __LINUX__
	try
	{
		struct stat st;
		if (stat(path.c_str(), &st) == 0)
		{
			return 0;
		}

		int result = mkdir(path.c_str(), 0755);
		if (result == 0)
		{
			return 0;
		}

		std::stringstream ss;
		ss << "[CreateDir] Cannot create folder (" << path << "). Error code: " << result;
		Logger::LogError(ss.str().c_str());
		return -1;
	}
	catch (const std::exception& e)
	{
		std::string error_message = std::string("[CreateDir] Cannot create folder (" + path + "). Error message: " + e.what());
		Logger::LogError(error_message.c_str());
		return -1;
	}

#else
	try
	{
		std::__fs::filesystem::create_directory(path.c_str());
		return 0;
	}
	catch (const std::exception& e)
	{
		std::string error_message = std::string("[CreateDir] Cannot create folder (" + path + "). Error message: " + e.what());
		Logger::LogError(error_message.c_str());
		return -1;
	}
#endif
}

void FileSystem::OpenWindowExplorer(const std::string& path)
{
	if (CreateDir(path) == 0)
	{
#ifdef __WINDOWS__
		ShellExecuteA(NULL, "open", path.c_str(), NULL, NULL, SW_SHOWDEFAULT);
#else
	#ifdef __LINUX__
		std::string command = "xdg-open " + path;
	#else
		std::string command = "open " + path;
	#endif
		system(command.c_str());
#endif
	}
}