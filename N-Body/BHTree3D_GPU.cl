// ================================================================== //
__kernel void ResetTree(
    __global int* nodes_data,
    __global int* bottom,
    __global int* out_of_tree_particle_counter,
    __global float* nodes_center_of_mass_mass,
    __global float* min_x,
    __global float* max_x,
    __global float* nodes_min_x_positions,
    __global float* nodes_max_x_positions,
    __global float* min_y,
    __global float* max_y,
    __global float* nodes_min_y_positions,
    __global float* nodes_max_y_positions,
    __global float* min_z,
    __global float* max_z,
    __global float* nodes_min_z_positions,
    __global float* nodes_max_z_positions
)
{
    nodes_min_x_positions[NUMBER_OF_NODES] = *min_x;
    nodes_max_x_positions[NUMBER_OF_NODES] = *max_x;

    nodes_min_y_positions[NUMBER_OF_NODES] = *min_y;
    nodes_max_y_positions[NUMBER_OF_NODES] = *max_y;

    nodes_min_z_positions[NUMBER_OF_NODES] = *min_z;
    nodes_max_z_positions[NUMBER_OF_NODES] = *max_z;

    for(int i = 0; i < NUMBER_OF_CHILD_NODES; i++)
    {
        nodes_data[NUMBER_OF_NODES * NUMBER_OF_CHILD_NODES + i] = EMPTY_NODE;
    }

    *bottom = NUMBER_OF_NODES;
    *out_of_tree_particle_counter = 0;
    nodes_center_of_mass_mass[NUMBER_OF_NODES] = -1.0f;
}

// ================================================================== //
__attribute__ ((reqd_work_group_size(WORKGROUP_SIZE, 1, 1)))
__kernel void BuildTree(
    volatile __global int* nodes_data,
    volatile __global int* bottom,
    __global float* positions,
    volatile __global int* out_of_tree_particle_counter,
    __global int* out_of_tree_particles,
    __global float* nodes_center_of_mass_mass,
    __global float* nodes_min_x_positions,
    __global float* nodes_max_x_positions,
    __global float* nodes_min_y_positions,
    __global float* nodes_max_y_positions,
    __global float* nodes_min_z_positions,
    __global float* nodes_max_z_positions
)
{
    const int step_size = get_local_size(0) * get_num_groups(0);
    int particle_index = get_global_id(0);
    int node_index;
    int node_path;
    bool new_body = true;
    float3 node_min_coordinates;
    float3 node_max_coordinates;
    
    while (particle_index < NUMBER_OF_PARTICLES)
    {
        const float3 particle_coordinates = GetParticleCoordinates(positions, particle_index);
        if(new_body)
        {
            // new body, so start from root
            new_body = false;
            node_index = NUMBER_OF_NODES;
            node_min_coordinates = (float3) { nodes_min_x_positions[node_index], nodes_min_y_positions[node_index], nodes_min_z_positions[node_index] };
            node_max_coordinates = (float3) { nodes_max_x_positions[node_index], nodes_max_y_positions[node_index], nodes_max_z_positions[node_index] };
            node_path = GetNodePath(node_min_coordinates, node_max_coordinates, particle_coordinates);
            if (node_path == -1)
            {
                return;
            }
        }

        int node_child_index = nodes_data[node_index * NUMBER_OF_CHILD_NODES + node_path];
        // follow path to leaf cell
        while (node_child_index >= NUMBER_OF_PARTICLES)
        {
            node_index = node_child_index;
            node_min_coordinates = (float3) { nodes_min_x_positions[node_index], nodes_min_y_positions[node_index], nodes_min_z_positions[node_index] };
            node_max_coordinates = (float3) { nodes_max_x_positions[node_index], nodes_max_y_positions[node_index], nodes_max_z_positions[node_index] };
            node_path = GetNodePath(node_min_coordinates, node_max_coordinates, particle_coordinates);
            if (node_path == -1)
            {
                return;
            }

            node_child_index = nodes_data[node_index * NUMBER_OF_CHILD_NODES + node_path];
        }

        if (node_child_index != LOCKED_NODE)
        {
            // try locking
            int locked_node_index = node_index * NUMBER_OF_CHILD_NODES + node_path;
            /*
             * Read the 32-bit value (referred to as old) stored at location pointed by p.
             * Compute (old == cmp) ? val : old
             * and store result at location pointed by p.
             * The function returns old.
             */
            if (node_child_index == atom_cmpxchg(&nodes_data[locked_node_index], node_child_index, LOCKED_NODE))
            {
                if (node_child_index == EMPTY_NODE)
                {
                    nodes_data[locked_node_index] = particle_index;
                }
                // node is a particle node
                else if(SameParticles(positions, particle_index, node_child_index))
                {
                    // Check if the two particle has same coordinates
                    nodes_data[locked_node_index] = node_child_index;

                    const int size = atomic_inc(out_of_tree_particle_counter);
                    out_of_tree_particles[size] = particle_index;
                }
                else
                {
                    int patch = -1;
                    const float3 node_particle_coordinates = GetParticleCoordinates(positions, node_child_index);

                    do
                    {
                        const int cell = atomic_dec(bottom) - 1;
                        if(cell < 0)
                        {
                            DEBUG_PRINT(("[BuildTree] cell < 0 (%d [%2.10f, %2.10f, %2.10f], %d [%2.10f, %2.10f, %2.10f])\n", particle_index, particle_coordinates.x, particle_coordinates.y, particle_coordinates.z, node_child_index, node_particle_coordinates.x, node_particle_coordinates.y, node_particle_coordinates.z));
                            return;
                        }
                        patch = max(patch, cell);

                        CreateCell(
                            nodes_data,
                            nodes_center_of_mass_mass,
                            nodes_min_x_positions,
                            nodes_max_x_positions,
                            nodes_min_y_positions,
                            nodes_max_y_positions,
                            nodes_min_z_positions,
                            nodes_max_z_positions,
                            node_particle_coordinates,
                            node_index,
                            cell
                        );

                        if (patch != cell)
                        {
                            nodes_data[node_index * NUMBER_OF_CHILD_NODES + node_path] = cell;
                        }

                        // current node child
                        node_min_coordinates = (float3) { nodes_min_x_positions[cell], nodes_min_y_positions[cell], nodes_min_z_positions[cell] };
                        node_max_coordinates = (float3) { nodes_max_x_positions[cell], nodes_max_y_positions[cell], nodes_max_z_positions[cell] };
                        node_path = GetNodePath(node_min_coordinates, node_max_coordinates, node_particle_coordinates);
                        nodes_data[cell * NUMBER_OF_CHILD_NODES + node_path] = node_child_index;

                        // next child
                        node_index = cell;
                        node_path = GetNodePath(node_min_coordinates, node_max_coordinates, particle_coordinates);
                        node_child_index = nodes_data[node_index * NUMBER_OF_CHILD_NODES + node_path];
                    } while(node_child_index > EMPTY_NODE);

                    // store current particle
                    nodes_data[node_index * NUMBER_OF_CHILD_NODES + node_path] = particle_index;

                    nodes_data[locked_node_index] = patch;
                }

                // move to next body
                particle_index += step_size;
                new_body = true;
            }
        }
    }
}

// ================================================================== //
__attribute__ ((reqd_work_group_size(WORKGROUP_SIZE, 1, 1)))
__kernel void SummarizeTree(
    __global int* nodes_data,
    __global int* _bottom,
    __global float* positions,
    __global float* masses,
    __global float* nodes_center_of_mass_x,
    __global float* nodes_center_of_mass_y,
    __global float* nodes_center_of_mass_mass,
    __global float* nodes_center_of_mass_z
)
{
    __local volatile int local_child[WORKGROUP_SIZE * NUMBER_OF_CHILD_NODES];
    int bottom = *_bottom;
    int step_size = get_local_size(0) * get_num_groups(0);

    int node = (bottom & (-WRAP_SIZE)) + get_global_id(0);
	if (node < bottom)
	{
        node += step_size;
    }

    int missing = 0;
    float mass;
	float center_mass, center_x, center_y, center_z;

    while (node <= NUMBER_OF_NODES)
    {
        if (missing == 0)
        {
			// new cell, so initialize
			center_mass = 0.0f;
			center_x = 0.0f;
			center_y = 0.0f;
            center_z = 0.0f;

			// gets incremented when cell is used
			int used_child_index = 0;

            for (int child_index = 0; child_index < NUMBER_OF_CHILD_NODES; child_index++)
            {
				int child = nodes_data[node * NUMBER_OF_CHILD_NODES + child_index];

                // is used
				if (child >= 0)
                {
					if (child_index != used_child_index)
                    {
						nodes_data[NUMBER_OF_CHILD_NODES * node + child_index] = -1;
						nodes_data[NUMBER_OF_CHILD_NODES * node + used_child_index] = child;
					}

					// Cache missing children
					local_child[WORKGROUP_SIZE * missing + get_local_id(0)] = child;

					mass = child < NUMBER_OF_PARTICLES ? masses[child] : nodes_center_of_mass_mass[child];

					missing++;

					if (mass >= 0.0f)
                    {
						// child is ready	
						missing--;

                        float x, y, z;
						if (child >= NUMBER_OF_PARTICLES)
                        {
                            x = nodes_center_of_mass_x[child];
                            y = nodes_center_of_mass_y[child];
                            z = nodes_center_of_mass_z[child];
						}
                        else
                        {
                            const float3 particle_coordinates = GetParticleCoordinates(positions, child);
                            x = particle_coordinates.x;
                            y = particle_coordinates.y;
                            z = particle_coordinates.z;
                        }

                        center_mass += mass;
                        center_x += x * mass;
                        center_y += y * mass;
                        center_z += z * mass;
					}
					used_child_index++;
				}
            }
        }

        if (missing != 0)
        {
			do
            {
				int child = local_child[(missing - 1) * WORKGROUP_SIZE + get_local_id(0)];
                mass = child < NUMBER_OF_PARTICLES ? masses[child] : nodes_center_of_mass_mass[child];

				// Body children can never be missing, so this is a cell
				if (mass >= 0.0f)
                {
					missing--;
                    float x, y, z;
					if (child >= NUMBER_OF_PARTICLES)
                    {
                        x = nodes_center_of_mass_x[child];
                        y = nodes_center_of_mass_y[child];
                        z = nodes_center_of_mass_z[child];
					}
                    else
                    {
                        const float3 particle_coordinates = GetParticleCoordinates(positions, child);
                        x = particle_coordinates.x;
                        y = particle_coordinates.y;
                        z = particle_coordinates.z;
                    }

					center_mass += mass;
                    center_x += x * mass;
                    center_y += y * mass;
                    center_z += z * mass;
				}
			} while ((mass >= 0.0f) && (missing != 0));
		}

        if (missing == 0)
        {
			mass = 1.0f / center_mass;

            nodes_center_of_mass_x[node] = center_x * mass;
            nodes_center_of_mass_y[node] = center_y * mass;
            nodes_center_of_mass_z[node] = center_z * mass;
			nodes_center_of_mass_mass[node] = center_mass;

            // next cell
			node += step_size;
		}
    }
}

// ================================================================== //
__kernel void CalculateForce(
    __global float* positions,
    __global float* masses,
    __global float* forces,
    __global int* nodes_data,
    __global int* out_of_tree_particle_counter,
    __global int* out_of_tree_particles,
    __global float* nodes_min_x_positions,
    __global float* nodes_max_x_positions,
    __global float* nodes_center_of_mass_mass,
    __global float* nodes_center_of_mass_x,
    __global float* nodes_center_of_mass_y,
    __global float* nodes_center_of_mass_z,
    const int normal_update,
    const int max_depth,
    __global int* _current_node,
    __global int* _node_path
)
{
    int current_node[5000];
    int node_path[5000];

    const int particle_index = get_global_id(0);
    const float3 particle_coordinates = GetParticleCoordinates(positions, particle_index);
    float3 force = { 0.0f, 0.0f, 0.0f };
    float gravitational_constant_mass = GRAVITATIONAL_CONSTANT;
    if(normal_update == 1)
    {
        gravitational_constant_mass *= masses[particle_index];
    }

    float3 child_coordinates = (float3) { nodes_center_of_mass_x[NUMBER_OF_NODES], nodes_center_of_mass_y[NUMBER_OF_NODES], nodes_center_of_mass_z[NUMBER_OF_NODES] };
    float x = particle_coordinates.x - child_coordinates.x;
    float y = particle_coordinates.y - child_coordinates.y;
    float z = particle_coordinates.z - child_coordinates.z;
    float distance = sqrt(x * x + y * y + z * z);
    float box_width = nodes_max_x_positions[NUMBER_OF_NODES] - nodes_min_x_positions[NUMBER_OF_NODES];

    if(box_width / distance <= THETA)
    {
        float child_mass = nodes_center_of_mass_mass[NUMBER_OF_NODES];
        float F = (gravitational_constant_mass * child_mass) / (distance * distance * distance);
        force.x += F * (child_coordinates.x - particle_coordinates.x);
        force.y += F * (child_coordinates.y - particle_coordinates.y);
        force.z += F * (child_coordinates.z - particle_coordinates.z);
    }
    else
    {
        const int depth_start_index = /* particle_index * max_depth */0;
        int depth = 0;
        current_node[depth_start_index + depth] = NUMBER_OF_NODES;
        node_path[depth_start_index + depth] = 0;
        while(depth >= 0)
        {
            if(node_path[depth_start_index + depth] == NUMBER_OF_CHILD_NODES)
            {
                depth--;
            }

            if(depth >= max_depth - 1)
            {
                DEBUG_PRINT(("[CalculateParticlePositionViaSolver] depth is to big\n"));
                node_path[depth_start_index + depth] = NUMBER_OF_CHILD_NODES;
                depth = -1;
            }
            else
            {
                while(node_path[depth_start_index + depth] < NUMBER_OF_CHILD_NODES)
                {
                    int child = nodes_data[current_node[depth_start_index + depth] * NUMBER_OF_CHILD_NODES + node_path[depth_start_index + depth]];
                    if(child == EMPTY_NODE)
                    {
                        node_path[depth_start_index + depth]++;
                    }
                    else if(child < NUMBER_OF_PARTICLES)
                    {
                        float3 child_coordinates = GetParticleCoordinates(positions, child);
                        float x = particle_coordinates.x - child_coordinates.x;
                        float y = particle_coordinates.y - child_coordinates.y;
                        float z = particle_coordinates.z - child_coordinates.z;
                        float distance = sqrt(x * x + y * y + z * z + LAMBDA);
                        if(distance > 0)
                        {
                            float child_mass = masses[child];
                            float F = (gravitational_constant_mass * child_mass) / (distance * distance * distance);
                            force.x += F * (child_coordinates.x - particle_coordinates.x);
                            force.y += F * (child_coordinates.y - particle_coordinates.y);
                            force.z += F * (child_coordinates.z - particle_coordinates.z);
                        }

                        node_path[depth_start_index + depth]++;
                    }
                    else
                    {
                        // it is a node
                        float3 child_coordinates = (float3) { nodes_center_of_mass_x[child], nodes_center_of_mass_y[child], nodes_center_of_mass_z[child] };
                        float x = particle_coordinates.x - child_coordinates.x;
                        float y = particle_coordinates.y - child_coordinates.y;
                        float z = particle_coordinates.z - child_coordinates.z;
                        float distance = sqrt(x * x + y * y + z * z);
                        float box_width = nodes_max_x_positions[child] - nodes_min_x_positions[child];
                        if(box_width / distance <= THETA)
                        {
                            float child_mass = nodes_center_of_mass_mass[child];
                            float F = (gravitational_constant_mass * child_mass) / (distance * distance * distance);
                            force.x += F * (child_coordinates.x - particle_coordinates.x);
                            force.y += F * (child_coordinates.y - particle_coordinates.y);
                            force.z += F * (child_coordinates.z - particle_coordinates.z);

                            node_path[depth_start_index + depth]++;
                        }
                        else
                        {                        
                            node_path[depth_start_index + depth]++;
                            depth++;
                            current_node[depth_start_index + depth] = child;
                            node_path[depth_start_index + depth] = 0;
                        }
                    }
                }
            }
        }
    }

    // TODO
    // __global int* out_of_tree_particle_counter,
    // __global int* out_of_tree_particles,

    const int particle_force_start_index = particle_index * 3;
    forces[particle_force_start_index] = force.x;
    forces[particle_force_start_index + 1] = force.y;
    forces[particle_force_start_index + 2] = force.z;
}


// ================================================================== //
__attribute__ ((reqd_work_group_size(WORKGROUP_SIZE, 1, 1)))
__kernel void SetParticleNodesAxis(
    __global int* nodes_data,
    __global int* _bottom,
    __global float* positions,
    __global float* nodes_min_x_positions,
    __global float* nodes_max_x_positions,
    __global float* nodes_min_y_positions,
    __global float* nodes_max_y_positions,
    __global float* nodes_min_z_positions,
    __global float* nodes_max_z_positions
)
{
    int bottom = *_bottom;
    int step_size = get_local_size(0) * get_num_groups(0);

    int node_index = (bottom & (-WRAP_SIZE)) + get_global_id(0);
	if (node_index < bottom)
	{
        node_index += step_size;
    }

    while (node_index <= NUMBER_OF_NODES)
    {
        const float3 node_min_coordinates = (float3) { nodes_min_x_positions[node_index], nodes_min_y_positions[node_index], nodes_min_z_positions[node_index] };
        const float3 node_max_coordinates = (float3) { nodes_max_x_positions[node_index], nodes_max_y_positions[node_index], nodes_max_z_positions[node_index] };
        const float middle_x = node_min_coordinates.x + (node_max_coordinates.x - node_min_coordinates.x) / 2;
        const float middle_y = node_min_coordinates.y + (node_max_coordinates.y - node_min_coordinates.y) / 2;
        const float middle_z = node_min_coordinates.z + (node_max_coordinates.z - node_min_coordinates.z) / 2;

        for(int i = 0; i < NUMBER_OF_CHILD_NODES; i++)
        {
            const int child = nodes_data[NUMBER_OF_CHILD_NODES * node_index + i];
            if(child > EMPTY_NODE && child < NUMBER_OF_PARTICLES)
            {
                const int cell = atomic_dec(_bottom) - 1;
                if(cell < 0)
                {
                    DEBUG_PRINT(("[SetParticleNodesAxis] cell < 0\n"));
                    return;
                }

                const float3 particle_coordinates = GetParticleCoordinates(positions, child);

                // X
                if(particle_coordinates.x <= middle_x)
                {
                    nodes_min_x_positions[cell] = node_min_coordinates.x;
                    nodes_max_x_positions[cell] = middle_x;
                }
                else
                {
                    nodes_min_x_positions[cell] = middle_x;
                    nodes_max_x_positions[cell] = node_max_coordinates.x;
                }

                // Y
                if(particle_coordinates.y <= middle_y)
                {
                    nodes_min_y_positions[cell] = node_min_coordinates.y;
                    nodes_max_y_positions[cell] = middle_y;
                }
                else
                {
                    nodes_min_y_positions[cell] = middle_y;
                    nodes_max_y_positions[cell] = node_max_coordinates.y;
                }

                // Z
                if(particle_coordinates.z <= middle_z)
                {
                    nodes_min_z_positions[cell] = node_min_coordinates.z;
                    nodes_max_z_positions[cell] = middle_z;
                }
                else
                {
                    nodes_min_z_positions[cell] = middle_z;
                    nodes_max_z_positions[cell] = node_max_coordinates.z;
                }
            }
        }
        node_index += step_size;
    }
}

// ================================================================== //
__kernel void CalculateParticlePosition(
    __global float* positions,
    __global float* velocities,
    __global float* masses,
    __global float* forces,
    volatile __global float* min_x,
    volatile __global float* max_x,
    volatile __global float* min_y,
    volatile __global float* max_y,
    volatile __global float* min_z,
    volatile __global float* max_z,
    float delta_time
)
{
    const int particle_index = get_global_id(0);
    const float particle_mass = masses[particle_index];
    const float3 force = GetParticleCoordinates(forces, particle_index);
    const float3 particle_coordinates = GetParticleCoordinates(positions, particle_index);
    const float3 particle_velocities = GetParticleCoordinates(velocities, particle_index);

    // aggregate acceleration
    float3 acceleration = force / particle_mass;

    // 4. integrate speed
    const int particle_pos_start_index = particle_index * 3;
    velocities[particle_pos_start_index] = particle_velocities.x + acceleration.x;
    velocities[particle_pos_start_index + 1] = particle_velocities.y + acceleration.y;
    velocities[particle_pos_start_index + 2] = particle_velocities.z + acceleration.z;

    // 5. integrate position
    const float x = particle_coordinates.x + velocities[particle_pos_start_index] * delta_time;
    const float y = particle_coordinates.y + velocities[particle_pos_start_index + 1] * delta_time;
    const float z = particle_coordinates.z + velocities[particle_pos_start_index + 2] * delta_time;

    positions[particle_pos_start_index] = x;
    positions[particle_pos_start_index + 1] = y;
    positions[particle_pos_start_index + 2] = z;

    fatomic_min(min_x, x);
    fatomic_max(max_x, x);

    fatomic_min(min_y, y);
    fatomic_max(max_y, y);

    fatomic_min(min_z, z);
    fatomic_max(max_z, z);
}

// ================================================================== //
__kernel void CalculateParticlePositionViaSolver(
    __global float* forces,
    __global float* state,
	__global float* step
)
{
    const int particle_index = get_global_id(0);
    const float3 force = GetParticleCoordinates(forces, particle_index);

    const int particle_pos_start_index = particle_index * 6;
    step[particle_pos_start_index] = state[particle_pos_start_index + 3];
    step[particle_pos_start_index + 1] = state[particle_pos_start_index + 4];
    step[particle_pos_start_index + 2] = state[particle_pos_start_index + 5];

    step[particle_pos_start_index + 3] = force.x;
    step[particle_pos_start_index + 4] = force.y;
    step[particle_pos_start_index + 5] = force.z;
}