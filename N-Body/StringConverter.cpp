#include "StringConverter.h"

std::string StringConverter::ISO_8859_2_TO_UTF_8(std::string string)
{
    std::string strOut;
    for (std::string::iterator it = string.begin(); it != string.end(); ++it)
    {
        uint8_t ch = *it;
        if (ch < 0x80)
        {
            strOut.push_back(ch);
        }
        /*
        else
        {
            strOut.push_back(0xc0 | ch >> 6);
            strOut.push_back(0x80 | (ch & 0x3f));
        }
        */
        else if (ch < 0x800) {
            strOut.push_back(192 | ch >> 6);
            strOut.push_back(128 | (ch & 63));
        }
        else {
            strOut.push_back(224 | ch >> 12);
            strOut.push_back(128 | (ch & 4095) >> 6);
            strOut.push_back(128 | (ch & 63));
        }
    }
    return strOut;
}