#include "BHTree_GPU.h"

BHTree_GPU::BHTree_GPU(const int& number_of_particles, const float& gravitational_constant, const float& theta, const float& lambda, const int& device_index) :
	Model(number_of_particles, gravitational_constant, theta, lambda, false),
	BHTreeBaseGPU(number_of_particles, gravitational_constant, theta, lambda, false, device_index)
{}

bool BHTree_GPU::InitGPU()
{
	BHTreeBaseGPU::InitGPU(source_code_file, min_x, max_x, min_y, max_y, this->GetPositions(), this->GetVelocities(), this->GetMasses());
	return gpu_inited;
}

void BHTree_GPU::Update(float delta_time)
{
	if (gpu_inited)
	{
		BuildTree(1);
		CalculateParticlePosition(8, delta_time);
		ReadBackPositions(this->GetPositions());
	}
}

void BHTree_GPU::UpdateViaSolver(float* state, float* step, bool skip)
{
	if (gpu_inited)
	{
		if (!skip)
		{
			this->CopyDataFromSolverState(state);
		}

		WriteIntoPositionsBuffer(this->GetPositions());
		ResetMinMaxBuffers(min_x, max_x, min_y, max_y);
		SetSolverBuffers(state, step);

		BuildTree();
		CalculateParticlePositionViaSolver();
		ReadBackPositions(this->GetPositions());

		ReadBackSolverSteps(step);
	}
}

std::vector<glm::vec3> BHTree_GPU::GetAxis()
{
	std::vector<glm::vec3> axis;
	if (gpu_inited)
	{
		int bottom;
		command_queue.enqueueReadBuffer(cl_bottom, CL_TRUE, 0, sizeof(int), &bottom);
		if (bottom > 0)
		{
			int vector_size = number_of_nodes + 1;
			size_t size = sizeof(float) * vector_size;

			std::vector<float> nodes_min_x_positions = std::vector<float>(vector_size);
			command_queue.enqueueReadBuffer(cl_nodes_min_x_positions, CL_TRUE, 0, size, &nodes_min_x_positions[0]);

			std::vector<float> nodes_max_x_positions = std::vector<float>(vector_size);
			command_queue.enqueueReadBuffer(cl_nodes_max_x_positions, CL_TRUE, 0, size, &nodes_max_x_positions[0]);

			std::vector<float> nodes_min_y_positions = std::vector<float>(vector_size);
			command_queue.enqueueReadBuffer(cl_nodes_min_y_positions, CL_TRUE, 0, size, &nodes_min_y_positions[0]);

			std::vector<float> nodes_max_y_positions = std::vector<float>(vector_size);
			command_queue.enqueueReadBuffer(cl_nodes_max_y_positions, CL_TRUE, 0, size, &nodes_max_y_positions[0]);

			for (int i = bottom; i < vector_size; i++)
			{
				axis.push_back(glm::vec3(nodes_min_x_positions[i], nodes_min_y_positions[i], 0));
				axis.push_back(glm::vec3(nodes_max_x_positions[i], nodes_max_y_positions[i], 0));
			}

			nodes_min_x_positions.clear();
			nodes_max_x_positions.clear();
			nodes_min_y_positions.clear();
			nodes_max_y_positions.clear();
		}
	}
	return axis;
}