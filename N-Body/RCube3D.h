#pragma once

#include "Model.h"

class RCube3D : public Model
{
public:
	RCube3D(const bool&, const int&, const float&, const float&, const float&);
	~RCube3D();
	virtual void Update(const float);
	virtual void UpdateViaSolver(float*, float*, bool = false);
	virtual std::vector<glm::vec3> GetAxis();
private:
	std::mutex m;

	bool center_of_mass_logic;
	int blocks_in_row;
	int blocks_in_row_pow;
	int number_of_blocks;											//	number of blocks

	std::vector<std::thread> myThreads;
	std::vector<std::vector<int>> block_neighbors_index;			// Block0 neighbors index inside block_neighbors_index[0]
	std::vector<std::vector<int>> block_particles;					// store that Particle1 and Particle2 in Block0
	std::vector<int> particle_block_index;							// Particle1 in Block0
	std::vector<Particle> block_center_of_mass;
	std::vector<float> blocks_x_positions;
	std::vector<float> blocks_y_positions;
	std::vector<float> blocks_z_positions;

	template <class _Fn, class... _Args>
	void RunThread(const int&, _Fn&&, _Args&&...);
	void SetBlockNeighbors(const int&);
	glm::vec3 GetBlockIndexes(const int&);
	void SetBlockPositions();
	void SetBlocksParticles();
	void SetParticlesData(int);
	std::vector<glm::vec3> GetBlockCoordinates(const int&);
	void SetBlockCenterOfMass(const int&);
	void CalculateParticlePosition(int, float);
	void CalculateParticlePositionWithCenterOfMass(int, float);
	void CalculateParticlePositionViaSolver(int, float*, float*);
	void CalculateParticlePositionWithCenterOfMassViaSolver(int, float*, float*);
	glm::vec3 CalculateBlockForceByParticle(const int&, const int&, const float&, const float&, const float&, const float& = 1);
};