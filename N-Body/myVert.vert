#version 150

// variables from VBO
in vec3 vs_in_pos;
in vec3 vs_in_col;

// values to be passed on in the pipeline
out vec3 vs_out_col;

const int offset_size = 1000;

// shader external parameters
uniform mat4 MVP;

uniform vec2 offsets[offset_size];

mat4 translate(float x, float y, float z)
{
	return mat4(vec4(1.0, 0.0, 0.0, 0.0),
                vec4(0.0, 1.0, 0.0, 0.0),
                vec4(0.0, 0.0, 1.0, 0.0),
                vec4(x, y, z, 1.0));
}

void main()
{
    vec2 offset = offsets[gl_InstanceID];
	gl_Position = MVP * (translate(offset.x, offset.y, 0.0f) * vec4(vs_in_pos, 1));
    vs_out_col = vs_in_col;
}