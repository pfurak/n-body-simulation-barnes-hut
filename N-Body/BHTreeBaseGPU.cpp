#include "BHTreeBaseGPU.h"

BHTreeBaseGPU::BHTreeBaseGPU(const int& number_of_particles, const float& gravitational_constant, const float& theta, const float& lambda, const bool& is_3d, const int& device_index) :
BaseGPU(number_of_particles, gravitational_constant, theta, lambda, is_3d, device_index)
{
    SetNumberOfNodes();
    global = MAX_COMPUTE_UNITS * WORK_GROUPS; // 16 * 16 = 256
    local = WORK_GROUPS;
    gpu_inited = false;

    if (is_3d)
    {
        SetAlgorithmName("BHTree 3D GPU");
    }
    else
    {
        SetAlgorithmName("BHTree 2D GPU");
    }
}

void BHTreeBaseGPU::SetNumberOfNodes()
{
    // int n = number_of_particles / MAX_COMPUTE_UNITS;
    // number of particles = MAX_COMPUTE_UNITS * WRAP_SIZE * WORK_GROUPS
    // TEMP
    // MAX_COMPUTE_UNITS = 50;
    // https://www.geeksforgeeks.org/count-divisors-n-on13/
    // int n = number_of_particles / 16;
    MAX_COMPUTE_UNITS = 16;
    WORK_GROUPS = 16;
    WRAP_SIZE = 16;
    /*        
    while (n > MAX_COMPUTE_UNITS * WORK_GROUPS)
    {
        MAX_COMPUTE_UNITS++;
        WORK_GROUPS++;
    }
    /*
    int n = MAX_COMPUTE_UNITS * WORK_GROUPS * WRAP_SIZE;
    while (n < number_of_particles)
    {
        MAX_COMPUTE_UNITS++;
        n = MAX_COMPUTE_UNITS * WORK_GROUPS * WRAP_SIZE;
    }
    
    // WRAP_SIZE--;
    */
    /*
    MAX_COMPUTE_UNITS = 40;
    WORK_GROUPS = 40;
    WRAP_SIZE = 40;
    */
    number_of_nodes = number_of_particles * 2;
    if (number_of_nodes < 1024 * MAX_COMPUTE_UNITS)
    {
        number_of_nodes = 1024 * MAX_COMPUTE_UNITS;
    }
        
    while ((number_of_nodes & (WRAP_SIZE - 1)) != 0)
    {
        number_of_nodes++;
    }
}

void BHTreeBaseGPU::InitGPU(const char* source_code_file, const float& min_x, const float& max_x, const float& min_y, const float& max_y, const float* positions, const float* velocities, const float* masses, const float& min_z, const float& max_z)
{
    std::stringstream ss;
    //ss << "-cl-std=CL1.1";
    ss << " -D NUMBER_OF_NODES=" << number_of_nodes;
    ss << " -D WORKGROUP_SIZE=" << local;
    ss << " -D NUM_WORK_GROUPS=" << WORK_GROUPS;
    ss << " -D WRAP_SIZE=" << WRAP_SIZE;
    ss << " -D MAX_DEPTH=64";

    std::vector<const char*> source_files;
    source_files.push_back(helper_cl);
    source_files.push_back(source_code_file);
    if (CreateProgarm(source_files, std::string(ss.str())))
    {
        try
        {
            // Make kernel
            kernel_reset_tree = cl::Kernel(cl_program, "ResetTree");
            kernel_build_tree = cl::Kernel(cl_program, "BuildTree");
            kernel_summarize_tree = cl::Kernel(cl_program, "SummarizeTree");
            kernel_calculate_force = cl::Kernel(cl_program, "CalculateForce");
            kernel_set_particle_nodes_axis = cl::Kernel(cl_program, "SetParticleNodesAxis");
            kernel_calculate_particle_position = cl::Kernel(cl_program, "CalculateParticlePosition");
            kernel_calculate_particle_position_via_solver = cl::Kernel(cl_program, "CalculateParticlePositionViaSolver");

            SetBaseBuffers(min_x, max_x, min_y, max_y, positions, velocities, masses, min_z, max_z);
            SetCLBuffers();
            SetKernelArguments();

            gpu_inited = true;
        }
        catch (cl::Error error)
        {
            std::stringstream ss;
            ss << "[Init GPU] " << error.what() << "(" << oclErrorString(error.err()) << ")";
            ss << "\nBuild options: " << cl_program.getBuildInfo<CL_PROGRAM_BUILD_OPTIONS>(device);
            Logger::LogError(ss.str().c_str());
            gpu_inited = false;
        }
    }
    else
    {
        gpu_inited = false;
    }
}

void BHTreeBaseGPU::SetCLBuffers()
{
    size_t size = sizeof(float) * (number_of_nodes + 1);
    cl_nodes_min_x_positions = cl::Buffer(context, cl_mem_flag, size);
    cl_nodes_max_x_positions = cl::Buffer(context, cl_mem_flag, size);
    cl_nodes_min_y_positions = cl::Buffer(context, cl_mem_flag, size);
    cl_nodes_max_y_positions = cl::Buffer(context, cl_mem_flag, size);
    if (is_3d)
    {
        cl_nodes_min_z_positions = cl::Buffer(context, cl_mem_flag, size);
        cl_nodes_max_z_positions = cl::Buffer(context, cl_mem_flag, size);
    }

    // ================================================================== //

    cl_nodes_center_of_mass_x = cl::Buffer(context, cl_mem_flag, size);
    cl_nodes_center_of_mass_y = cl::Buffer(context, cl_mem_flag, size);
    cl_nodes_center_of_mass_mass = cl::Buffer(context, cl_mem_flag, size);
    if (is_3d)
    {
        cl_nodes_center_of_mass_z = cl::Buffer(context, cl_mem_flag, size);
    }

    // ================================================================== //

    size = sizeof(int) * number_of_particles;
    cl_out_of_tree_particles = cl::Buffer(context, cl_mem_flag, size);

    // ================================================================== //

    size = sizeof(int) * (number_of_nodes + 1) * (is_3d ? 8 : 4);
    cl_nodes_data = cl::Buffer(context, cl_mem_flag, size);

    // ================================================================== //

    size = sizeof(int);
    cl_bottom = cl::Buffer(context, cl_mem_flag, size);
    cl_out_of_tree_particle_counter = cl::Buffer(context, cl_mem_flag, size);

    // ================================================================== //

    size = sizeof(float) * number_of_particles * (is_3d ? 3 : 2);
    cl_forces = cl::Buffer(context, cl_mem_flag, size);
}

void BHTreeBaseGPU::SetKernelArguments()
{
    kernel_reset_tree.setArg(0, cl_nodes_data);
    kernel_reset_tree.setArg(1, cl_bottom);
    kernel_reset_tree.setArg(2, cl_out_of_tree_particle_counter);
    kernel_reset_tree.setArg(3, cl_nodes_center_of_mass_mass);
    kernel_reset_tree.setArg(4, cl_min_x);
    kernel_reset_tree.setArg(5, cl_max_x);
    kernel_reset_tree.setArg(6, cl_nodes_min_x_positions);
    kernel_reset_tree.setArg(7, cl_nodes_max_x_positions);
    kernel_reset_tree.setArg(8, cl_min_y);
    kernel_reset_tree.setArg(9, cl_max_y);
    kernel_reset_tree.setArg(10, cl_nodes_min_y_positions);
    kernel_reset_tree.setArg(11, cl_nodes_max_y_positions);
    if (is_3d)
    {
        kernel_reset_tree.setArg(12, cl_min_z);
        kernel_reset_tree.setArg(13, cl_max_z);
        kernel_reset_tree.setArg(14, cl_nodes_min_z_positions);
        kernel_reset_tree.setArg(15, cl_nodes_max_z_positions);
    }


    // ================================================================== //

    kernel_build_tree.setArg(0, cl_nodes_data);
    kernel_build_tree.setArg(1, cl_bottom);
    kernel_build_tree.setArg(2, cl_positions);
    kernel_build_tree.setArg(3, cl_out_of_tree_particle_counter);
    kernel_build_tree.setArg(4, cl_out_of_tree_particles);
    kernel_build_tree.setArg(5, cl_nodes_center_of_mass_mass);
    kernel_build_tree.setArg(6, cl_nodes_min_x_positions);
    kernel_build_tree.setArg(7, cl_nodes_max_x_positions);
    kernel_build_tree.setArg(8, cl_nodes_min_y_positions);
    kernel_build_tree.setArg(9, cl_nodes_max_y_positions);
    if (is_3d)
    {
        kernel_build_tree.setArg(10, cl_nodes_min_z_positions);
        kernel_build_tree.setArg(11, cl_nodes_max_z_positions);
    }

    // ================================================================== //

    kernel_summarize_tree.setArg(0, cl_nodes_data);
    kernel_summarize_tree.setArg(1, cl_bottom);
    kernel_summarize_tree.setArg(2, cl_positions);
    kernel_summarize_tree.setArg(3, cl_masses);
    kernel_summarize_tree.setArg(4, cl_nodes_center_of_mass_x);
    kernel_summarize_tree.setArg(5, cl_nodes_center_of_mass_y);
    kernel_summarize_tree.setArg(6, cl_nodes_center_of_mass_mass);
    if (is_3d)
    {
        kernel_summarize_tree.setArg(7, cl_nodes_center_of_mass_z);
    }

    // ================================================================== //

    kernel_calculate_force.setArg(0, cl_positions);
    kernel_calculate_force.setArg(1, cl_masses);
    kernel_calculate_force.setArg(2, cl_forces);
    kernel_calculate_force.setArg(3, cl_nodes_data);
    kernel_calculate_force.setArg(4, cl_out_of_tree_particle_counter);
    kernel_calculate_force.setArg(5, cl_out_of_tree_particles);
    kernel_calculate_force.setArg(6, cl_nodes_min_x_positions);
    kernel_calculate_force.setArg(7, cl_nodes_max_x_positions);
    kernel_calculate_force.setArg(8, cl_nodes_center_of_mass_mass);
    kernel_calculate_force.setArg(9, cl_nodes_center_of_mass_x);
    kernel_calculate_force.setArg(10, cl_nodes_center_of_mass_y);
    if(is_3d)
    {
        kernel_calculate_force.setArg(11, cl_nodes_center_of_mass_z);
    }

    // ================================================================== //

    kernel_set_particle_nodes_axis.setArg(0, cl_nodes_data);
    kernel_set_particle_nodes_axis.setArg(1, cl_bottom);
    kernel_set_particle_nodes_axis.setArg(2, cl_positions);
    kernel_set_particle_nodes_axis.setArg(3, cl_nodes_min_x_positions);
    kernel_set_particle_nodes_axis.setArg(4, cl_nodes_max_x_positions);
    kernel_set_particle_nodes_axis.setArg(5, cl_nodes_min_y_positions);
    kernel_set_particle_nodes_axis.setArg(6, cl_nodes_max_y_positions);
    if (is_3d)
    {
        kernel_set_particle_nodes_axis.setArg(7, cl_nodes_min_z_positions);
        kernel_set_particle_nodes_axis.setArg(8, cl_nodes_max_z_positions);
    }

    // ================================================================== //

    kernel_calculate_particle_position.setArg(0, cl_positions);
    kernel_calculate_particle_position.setArg(1, cl_velocities);
    kernel_calculate_particle_position.setArg(2, cl_masses);
    kernel_calculate_particle_position.setArg(3, cl_forces);
    kernel_calculate_particle_position.setArg(4, cl_min_x);
    kernel_calculate_particle_position.setArg(5, cl_max_x);
    kernel_calculate_particle_position.setArg(6, cl_min_y);
    kernel_calculate_particle_position.setArg(7, cl_max_y);
    if (is_3d)
    {
        kernel_calculate_particle_position.setArg(8, cl_min_z);
        kernel_calculate_particle_position.setArg(9, cl_max_z);
    }

    // ================================================================== //

    kernel_calculate_particle_position_via_solver.setArg(0, cl_forces);
    kernel_calculate_particle_position_via_solver.setArg(1, cl_state);
    kernel_calculate_particle_position_via_solver.setArg(2, cl_step);
}

void BHTreeBaseGPU::BuildTree(const int& normal_update)
{
    RunClKernel("[Reset Tree]", kernel_reset_tree, 1);
    RunClKernel("[Build Tree]", kernel_build_tree, global, 0, local);
    RunClKernel("[Summarize Tree]", kernel_summarize_tree, global, 0, local);

    int bottom;
    command_queue.enqueueReadBuffer(cl_bottom, CL_TRUE, 0, sizeof(int), &bottom);
    const int max_depth = number_of_nodes - bottom;

    int arg_index = is_3d ? 12 : 11;
    kernel_calculate_force.setArg(arg_index++, normal_update);
    kernel_calculate_force.setArg(arg_index++, max_depth);
    RunClKernel("[Calculate Force]", kernel_calculate_force, global, 0, local);

    // Need to run it before update
    RunClKernel("[Set Particle Nodes Axis]", kernel_set_particle_nodes_axis, global, 0, local);
}

void BHTreeBaseGPU::CalculateParticlePosition(const int& index, const float& delta_time)
{
    kernel_calculate_particle_position.setArg(index, delta_time);
    RunClKernel("[Calculate Particle Position]", kernel_calculate_particle_position, number_of_particles);
}

void BHTreeBaseGPU::CalculateParticlePositionViaSolver()
{
    RunClKernel("[Calculate Particle Position Via Solver]", kernel_calculate_particle_position_via_solver, number_of_particles);
}