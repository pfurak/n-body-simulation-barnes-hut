#pragma once

// GLM
#include <glm/glm.hpp>

#include <math.h>
#include <vector>
#include "Structs.h"

class BHTreeNode3D
{
public:
	BHTreeNode3D(float, float);
	BHTreeNode3D(float, float, float, float, float, float);
	~BHTreeNode3D();
	void SetMinMaxCoordinates(float, float, float, float, float, float);
	void AddParticle(int, Particle);
	void SetCenterOfMass();
	Particle GetCenterOfMass();
	glm::vec3 GetForce(int, float, float, float, float, float);
	void ResetTree();
	std::vector<glm::vec3> GetAxis();
private:
	BHTreeNode3D* node[8];

	float min_x;
	float max_x;
	float min_y;
	float max_y;
	float min_z;
	float max_z;
	float box_width;

	int particle_index;
	Particle particle;
	Particle center_of_mass;

	static std::vector<Particle> out_of_tree_particles;
	static float theta;
	static float lambda;

	bool HasChild();
	int GetNodeIndex(const float, const float, const float);
	glm::vec3 CalculateForce(int, float, float, float, float, float);
	glm::vec3 CalculateAcceleration(Particle, const int&, float);
};

