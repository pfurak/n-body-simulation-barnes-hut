#pragma once

#include <math.h>

#include "BaseGPU.h"

class RCubeBaseGPU : public BaseGPU
{
public:
    RCubeBaseGPU(const int&, const float&, const float&, const float&, const bool&, const bool&, const int&);
    ~RCubeBaseGPU() {}
private:
    void SetCLBuffers();
    void SetKernelArguments();

    const char* helper_cl = "RCube_functions.cl";

protected:
    void InitGPU(const char*, const float& , const float& , const float& , const float& , const float*, const float*, const float*, const float& = NULL, const float& = NULL);

    void SetBlockNeighbours();
    void ResetVariables();
    void SetBlockPositions();
    void SetParticlesData();
    void SetBlockCenterOfMass();
    void CalculateParticlePositionWithCenterOfMass(const int&, const float&);
    void CalculateParticlePosition(const int&, const float&);
    void CalculateParticlePositionWithCenterOfMassViaSolver();
    void CalculateParticlePositionViaSolver();

    bool gpu_inited;
    bool center_of_mass_logic;

    int number_of_blocks;												//	number of blocks
    int blocks_in_row;
    int blocks_in_row_pow;

    cl::Kernel kernel_set_block_neighbors;
    cl::Kernel kernel_reset_variables;									//	reset volatile variables
    cl::Kernel kernel_set_block_positions;
    cl::Kernel kernel_set_particles_data;
    cl::Kernel kernel_set_block_center_of_mass;
    cl::Kernel kernel_calculate_particle_position_with_center_of_mass;
    cl::Kernel kernel_calculate_particle_position;
    cl::Kernel kernel_calculate_particle_position_with_center_of_mass_via_solver;
    cl::Kernel kernel_calculate_particle_position_via_solver;

    cl::Buffer cl_block_x_positions;				//	blocks possible X coordinates
    cl::Buffer cl_block_y_positions;				//	blocks possible Y coordinates
    cl::Buffer cl_block_z_positions;				//	blocks possible Z coordinates

    cl::Buffer cl_block_neighbors_index;			// Block0 neighbors index inside block_neighbors_index[0]
    cl::Buffer cl_block_neighbors_size;				// Block0 has N neighbors
    cl::Buffer cl_particle_block_index;				// Particle1 in Block0
    cl::Buffer cl_block_center_of_mass;
    cl::Buffer cl_block_particles;					// store that Particle1 and Particle2 in Block0
    cl::Buffer cl_block_particles_size;				// Block0 has N particle
};