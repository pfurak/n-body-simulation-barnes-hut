#!/bin/bash

clear

APP_NAME="N-Body"
ZIP_FILE=${APP_NAME}
HOME_PATH=~
APP_PATH="${HOME_PATH}/.local/share/applications/"
DEST_PATH=${APP_PATH}${APP_NAME}
DESKTOP_PATH=$(xdg-user-dir DESKTOP)

CHKROOT=`id|grep root`
if [ "$CHKROOT" != "" ]
then
    echo "Error: script is executed as root."
    echo "PLEASE DO NOT DO THIS!"
    echo "Run the script as the application owner"
    echo "Script aborted."
    exit 1
fi

######################################################################
# Print usage information
######################################################################
function usage () {
	echo install.sh
	echo Usage:
	echo "    install.sh <command>"
	echo "Commands:"
	echo "    install       Install the application."
	echo "    reinstall     Reinstall the application."
	echo "    uninstall     Uninstall the application."
}

######################################################################
# Install the application
######################################################################
function install() {
    if [ ! -e ${ZIP_FILE} ]
    then
        echo "Cannot find ${ZIP_FILE}"
    fi

    unzip -o ${ZIP_FILE} -d ${DEST_PATH}

    # Error check
    if [ "$?" != "0" ]
    then
        echo "Error returned from unzip, exiting..."
        date
        exit 2
    fi

    echo "[Desktop Entry]
    Encoding=UTF-8
    Version=1.0
    Type=Application
    Terminal=false
    Exec=${DEST_PATH}/nbody.sh
    Name=N-Body Simulation
    Icon=${DEST_PATH}/icon.png
    " > ${APP_PATH}${APP_NAME}.desktop

    gsettings set org.gnome.shell favorite-apps "$(gsettings get org.gnome.shell favorite-apps | sed s/.$//), '${APP_NAME}.desktop']"

    if [ -d "${DESKTOP_PATH}" ]
    then
        cp ${APP_PATH}${APP_NAME}.desktop ${DESKTOP_PATH}/${APP_NAME}.desktop
        cd ${DESKTOP_PATH}
        chmod a+x ${APP_NAME}.desktop
        gio set ${APP_NAME}.desktop metadata::trusted true
        # gnome-shell --replace & disown
        gtk-launch ${APP_NAME}.desktop
    fi
}

######################################################################
# Uninstall the application
######################################################################
function uninstall() {
    if [ -d ${DEST_PATH} ]
    then
        rm -rf ${DEST_PATH}
    fi

    if [ -e ${APP_PATH}${APP_NAME}.desktop ]
    then
        rm -rf ${APP_PATH}${APP_NAME}.desktop
    fi

    if [ -e ${DESKTOP_PATH}/${APP_NAME}.desktop ]
    then
        rm -rf ${DESKTOP_PATH}/${APP_NAME}.desktop
    fi
}

######################################################################
# Reinstall the application
######################################################################
function reinstall() {
    uninstall
    install
}

######################################################################
# Check the number of parameters, exit when <> 1
######################################################################
if [ $# -ne 1 ]
then
   usage
   exit 1
fi

######################################################################
# Install the application
######################################################################
if [ "$1" = "install" ]
then
   install
   exit 0
fi

######################################################################
# Uninstall the application
######################################################################
if [ "$1" = "uninstall" ]
then
   uninstall
   exit 0
fi

######################################################################
# Reinstall the application
######################################################################
if [ "$1" = "reinstall" ]
then
   reinstall
   exit 0
fi

usage
exit 1
