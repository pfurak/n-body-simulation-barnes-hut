#ifdef cl_amd_printf
    #pragma OPENCL EXTENSION cl_amd_printf : enable
#endif

#ifdef cl_intel_printf
    #pragma OPENCL EXTENSION cl_intel_printf : enable
#endif

#ifdef cl_khr_global_int32_base_atomics
    #pragma OPENCL EXTENSION cl_khr_global_int32_base_atomics : enable
#endif

#ifdef cl_khr_global_int32_extended_atomics
    #pragma OPENCL EXTENSION cl_khr_global_int32_extended_atomics : enable
#endif

#ifdef cl_khr_local_int32_base_atomics
    #pragma OPENCL EXTENSION cl_khr_local_int32_base_atomics : enable
#endif

// ================================================================== //

#ifdef DEBUG
    #define DEBUG_PRINT(x) printf x
#else
    #define DEBUG_PRINT(x) do {} while (0)
#endif

// ================================================================== //
// Function to perform the float atomic max
inline void fatomic_max(volatile __global float *source, const float operand)
{
    union
	{
        unsigned int intVal;
        float floatVal;
    } newVal;

    union
	{
        unsigned int intVal;
        float floatVal;
    } prevVal;

    do
	{
        prevVal.floatVal = *source;
        newVal.floatVal = max(prevVal.floatVal,operand);
    }
	while (atomic_cmpxchg((volatile __global unsigned int *)source, prevVal.intVal, newVal.intVal) != prevVal.intVal);
}

// ================================================================== //
// Function to perform the float atomic min
inline void fatomic_min(volatile __global float *source, const float operand)
{
    union
	{
        unsigned int intVal;
        float floatVal;
    } newVal;

    union
	{
        unsigned int intVal;
        float floatVal;
    } prevVal;

    do
	{
        prevVal.floatVal = *source;
        newVal.floatVal = min(prevVal.floatVal,operand);
    }
	while (atomic_cmpxchg((volatile __global unsigned int *)source, prevVal.intVal, newVal.intVal) != prevVal.intVal);
}

#ifdef IS_3D
    // ================================================================== //
    // To get positions and velocity
    inline float3 GetParticleCoordinates(const __global float* array, const int particle_index)
    {
        const int particle_pos_start_index = particle_index * 3;
        return (float3) { array[particle_pos_start_index], array[particle_pos_start_index + 1], array[particle_pos_start_index + 2] };
    }
#else
    // ================================================================== //
    // To get positions and velocity
    inline float2 GetParticleCoordinates(const __global float* array, const int particle_index)
    {
        const int particle_pos_start_index = particle_index * 2;
        return (float2) { array[particle_pos_start_index], array[particle_pos_start_index + 1] };
    }
#endif