#pragma once

#include "Model.h"

class BHTree : public Model
{
public:
    BHTree(const int&, const float&, const float&, const float&);
    ~BHTree();
    virtual void Update(const float);
    virtual void UpdateViaSolver(float*, float*, bool=false);
    virtual std::vector<glm::vec3> GetAxis();
private:
    BHTreeNode* root_node;
    
    void BuildTree();
    void UpdateParticlePositions(const float);
};