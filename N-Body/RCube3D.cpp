#include "RCube3D.h"

RCube3D::RCube3D(const bool& center_of_mass_logic, const int& number_of_particles, const float& gravitational_constant, const float& theta, const float& lambda) :
Model(number_of_particles, gravitational_constant, theta, lambda, true)
{
	this->center_of_mass_logic = center_of_mass_logic;

	blocks_in_row = pow(number_of_particles, 1.0 / 6);
	blocks_in_row_pow = blocks_in_row * blocks_in_row;
	number_of_blocks = blocks_in_row * blocks_in_row_pow;

	blocks_x_positions = std::vector<float>(blocks_in_row + 1, 0);
	blocks_y_positions = std::vector<float>(blocks_in_row + 1, 0);
	blocks_z_positions = std::vector<float>(blocks_in_row + 1, 0);

	block_particles = std::vector<std::vector<int>>(number_of_blocks);
	for (int i = 0; i < number_of_blocks; i++)
	{
		block_particles[i] = std::vector<int>();
	}

	if (center_of_mass_logic)
	{
		block_center_of_mass = std::vector<Particle>(number_of_blocks);
	}
	else
	{
		block_neighbors_index = std::vector<std::vector<int>>(number_of_blocks);
		for (int i = 0; i < number_of_blocks; i++)
		{
			block_neighbors_index[i] = std::vector<int>();
		}
		particle_block_index = std::vector<int>(number_of_particles, -1);
		RunThread(number_of_blocks, &RCube3D::SetBlockNeighbors);
	}
}

RCube3D::~RCube3D()
{
	myThreads.clear();

	blocks_x_positions.clear();
	blocks_y_positions.clear();
	blocks_z_positions.clear();

	for (int i = 0; i < block_particles.size(); i++)
	{
		block_particles[i].clear();
	}
	block_particles.clear();

	if (center_of_mass_logic)
	{
		block_center_of_mass.clear();
	}
	else
	{
		for (int i = 0; i < number_of_blocks; i++)
		{
			block_neighbors_index[i].clear();
		}
		block_neighbors_index.clear();
		particle_block_index.clear();
	}
}

template <class _Fn, class... _Args>
void RCube3D::RunThread(const int& limit, _Fn&& method, _Args&& ...params)
{
	if (limit < MAX_CPU_THREAD)
	{
		for (int i = 0; i < limit; i++)
		{
			myThreads.push_back(std::thread(method, this, i, std::forward<_Args>(params)...));
		}
		std::for_each(myThreads.begin(), myThreads.end(), std::mem_fn(&std::thread::join));
		myThreads.clear();
	}
	else
	{
		int thread_call_counter = 0;
		int j = 0;
		const int n = limit / MAX_CPU_THREAD;
		while (thread_call_counter < n)
		{
			for (int i = 0; i < MAX_CPU_THREAD; i++)
			{
				myThreads.push_back(std::thread(method, this, j, std::forward<_Args>(params)...));
				j++;
			}
			std::for_each(myThreads.begin(), myThreads.end(), std::mem_fn(&std::thread::join));
			myThreads.clear();

			thread_call_counter++;
		}

		const int m = limit % MAX_CPU_THREAD;
		if (m > 0)
		{
			for (int i = 0; i < m; i++)
			{
				myThreads.push_back(std::thread(method, this, j, std::forward<_Args>(params)...));
				j++;
			}
			std::for_each(myThreads.begin(), myThreads.end(), std::mem_fn(&std::thread::join));
			myThreads.clear();
		}
	}
}

void RCube3D::SetBlockNeighbors(const int& block_index)
{
	glm::vec3 block_indexes = GetBlockIndexes(block_index);
	const int x_index = block_indexes.x;
	const int y_index = block_indexes.y;
	const int z_index = block_indexes.z;

	if (x_index - 1 >= 0)
	{
		block_neighbors_index[block_index].push_back(block_index - 1);
	}

	if (x_index + 1 < blocks_in_row)
	{
		block_neighbors_index[block_index].push_back(block_index + 1);
	}

	if (y_index - 1 >= 0)
	{
		block_neighbors_index[block_index].push_back(block_index - blocks_in_row);
	}

	if (y_index + 1 < blocks_in_row)
	{
		block_neighbors_index[block_index].push_back(block_index + blocks_in_row);
	}

	if (z_index - 1 >= 0)
	{
		block_neighbors_index[block_index].push_back(block_index - blocks_in_row_pow);
	}

	if (z_index + 1 < blocks_in_row)
	{
		block_neighbors_index[block_index].push_back(block_index + blocks_in_row_pow);
	}
}

glm::vec3 RCube3D::GetBlockIndexes(const int& block_index)
{
	const int x_index = block_index % blocks_in_row;
	int y_index = block_index / blocks_in_row;
	if (y_index >= blocks_in_row)
	{
		y_index %= blocks_in_row;
	}
	const int z_index = block_index / blocks_in_row_pow;
	return glm::vec3(x_index, y_index, z_index);
}

void RCube3D::Update(float delta_time)
{
	SetBlockPositions();
	SetBlocksParticles();
	if (center_of_mass_logic)
	{
		RunThread(number_of_blocks, &RCube3D::SetBlockCenterOfMass);
		RunThread(number_of_particles, &RCube3D::CalculateParticlePositionWithCenterOfMass, delta_time);
	}
	else
	{
		RunThread(number_of_particles, &RCube3D::CalculateParticlePosition, delta_time);
	}
}

void RCube3D::UpdateViaSolver(float* state, float* step, bool skip)
{
	if (!skip)
	{
		this->CopyDataFromSolverState(state);
	}
	SetBlockPositions();
	SetBlocksParticles();
	if (center_of_mass_logic)
	{
		RunThread(number_of_blocks, &RCube3D::SetBlockCenterOfMass);
		RunThread(number_of_particles, &RCube3D::CalculateParticlePositionWithCenterOfMassViaSolver, state, step);
	}
	else
	{
		RunThread(number_of_particles, &RCube3D::CalculateParticlePositionViaSolver, state, step);
	}
}

void RCube3D::SetBlockPositions()
{
	/*                                
	 * |=========|=========|=========|====|max_y
	 * |         |         |         |    |
	 * |   B6    |   B7    |   B8    |    |
	 * |         |         |         |    |
	 * |=========|=========|=========|====|max_y (min_y of next block)
	 * |         |         |         |    |
	 * |   B3    |   B4    |   B5    |    |
	 * |         |         |         |    |
	 * |=========|=========|=========|====| max_y (min_y of next block)
	 * |         |         |         |   /
	 * |   B0    |   B1    |   B2    |  /
	 * |         |         |         | /
	 * |=========|=========|=========|/ min_y
	 * min_x  max_x   max_x  max_x
	 *      (min_x of next)
	 */

	const float x_step = (max_x - min_x) / blocks_in_row;
	const float y_step = (max_y - min_y) / blocks_in_row;
	const float z_step = (max_z - min_z) / blocks_in_row;

	blocks_x_positions[0] = min_x;
	blocks_y_positions[0] = min_y;
	blocks_z_positions[0] = min_z;
	for (int i = 1; i < blocks_in_row; i++)
	{
		blocks_x_positions[i] = blocks_x_positions[i - 1] + x_step;
		blocks_y_positions[i] = blocks_y_positions[i - 1] + y_step;
		blocks_z_positions[i] = blocks_z_positions[i - 1] + z_step;
	}
	blocks_x_positions[blocks_in_row] = max_x;
	blocks_y_positions[blocks_in_row] = max_y;
	blocks_z_positions[blocks_in_row] = max_z;
}

void RCube3D::SetBlocksParticles()
{
	for (int i = 0; i < number_of_blocks; i++)
	{
		block_particles[i].clear();
	}
	RunThread(number_of_particles, &RCube3D::SetParticlesData);
}

void RCube3D::SetParticlesData(int particle_index)
{
	const int particle_pos_index = particle_index * 3;
	float* positions = this->GetPositions();
	const float pos_x = positions[particle_pos_index];
	const float pos_y = positions[particle_pos_index + 1];
	const float pos_z = positions[particle_pos_index + 2];

	int block_index = 0;
	bool found = false;
	while (block_index < number_of_blocks && !found)
	{
		std::vector<glm::vec3> block_indexes = GetBlockCoordinates(block_index);
		glm::vec3 min_coordinates = block_indexes[0];
		glm::vec3 max_coordinates = block_indexes[1];
		if (
			min_coordinates.x <= pos_x && pos_x <= max_coordinates.x &&
			min_coordinates.y <= pos_y && pos_y <= max_coordinates.y &&
			min_coordinates.z <= pos_z && pos_z <= max_coordinates.z
		)
		{
			found = true;
		}
		else
		{
			block_index++;
		}
	}

	if (found)
	{
		m.lock();
		block_particles[block_index].push_back(particle_index);
		if (!center_of_mass_logic)
		{
			particle_block_index[particle_index] = block_index;
		}
		m.unlock();
	}
}

std::vector<glm::vec3> RCube3D::GetBlockCoordinates(const int& block_index)
{
	glm::vec3 block_indexes = GetBlockIndexes(block_index);
	const int x_index = block_indexes.x;
	const int y_index = block_indexes.y;
	const int z_index = block_indexes.z;

	std::vector<glm::vec3> indexes;
	indexes.push_back(glm::vec3(blocks_x_positions[x_index], blocks_y_positions[y_index], blocks_z_positions[z_index]));
	indexes.push_back(glm::vec3(blocks_x_positions[x_index + 1], blocks_y_positions[y_index + 1], blocks_z_positions[z_index + 1]));
	return indexes;
}

void RCube3D::SetBlockCenterOfMass(const int& block_index)
{
	Particle center_of_mass(0, 0, 0, 0);
	if (block_particles[block_index].size() > 0)
	{
		float* positions = this->GetPositions();
		float* masses = this->GetMasses();

		for (int i = 0; i < block_particles[block_index].size(); i++)
		{
			const int particle_index = block_particles[block_index][i];
			const int particle_pos_start_index = particle_index * 3;

			center_of_mass.mass += masses[particle_index];
			center_of_mass.x += positions[particle_pos_start_index] * masses[particle_index];
			center_of_mass.y += positions[particle_pos_start_index + 1] * masses[particle_index];
			center_of_mass.z += positions[particle_pos_start_index + 2] * masses[particle_index];
		}

		center_of_mass.x /= center_of_mass.mass;
		center_of_mass.y /= center_of_mass.mass;
		center_of_mass.z /= center_of_mass.mass;
	}
	block_center_of_mass[block_index] = center_of_mass;
}

void RCube3D::CalculateParticlePosition(int particle_index, float delta_time)
{
	const int block_index = particle_block_index[particle_index];
	if (block_index > -1)
	{
		float* positions = this->GetPositions();
		float* velocities = this->GetVelocities();
		float* masses = this->GetMasses();

		const int particle_pos_start_index = particle_index * 3;
		const float pos_x = positions[particle_pos_start_index];
		const float pos_y = positions[particle_pos_start_index + 1];
		const float pos_z = positions[particle_pos_start_index + 2];
		const float mass = masses[particle_index];

		glm::vec3 force = CalculateBlockForceByParticle(particle_index, block_index, pos_x, pos_y, pos_z, mass);
		for (int i = 0; i < block_neighbors_index[block_index].size(); i++)
		{
			force += CalculateBlockForceByParticle(particle_index, block_neighbors_index[block_index][i], pos_x, pos_y, pos_z, mass);
		}

		const glm::vec3 acc = force / mass;

		velocities[particle_pos_start_index] += acc.x;
		velocities[particle_pos_start_index + 1] += acc.y;
		velocities[particle_pos_start_index + 2] += acc.z;

		float x = pos_x + velocities[particle_pos_start_index] * delta_time;
		float y = pos_y + velocities[particle_pos_start_index + 1] * delta_time;
		float z = pos_z + velocities[particle_pos_start_index + 2] * delta_time;

		m.lock();
		this->SetParticlePositions(particle_index, x, y, z);
		particle_block_index[particle_index] = -1;
		m.unlock();
	}
}

void RCube3D::CalculateParticlePositionWithCenterOfMass(int particle_index, float delta_time)
{
	float* positions = this->GetPositions();
	float* velocities = this->GetVelocities();
	float* masses = this->GetMasses();

	const int particle_pos_start_index = particle_index * 2;
	const float pos_x = positions[particle_pos_start_index];
	const float pos_y = positions[particle_pos_start_index + 1];
	const float pos_z = positions[particle_pos_start_index + 2];

	const float mass = masses[particle_index];
	const float gravitational_constant_mass = gravitational_constant * mass;
	glm::vec3 force(0, 0, 0);

	for (int i = 0; i < number_of_blocks; i++)
	{
		if (block_particles[i].size() > 0)
		{
			Particle center_of_mass = block_center_of_mass[i];
			const float x = pos_x - center_of_mass.x;
			const float y = pos_y - center_of_mass.y;
			const float z = pos_z - center_of_mass.z;
			const float distance = sqrt(x * x + y * y + z * z);

			const int block_x_index = i % blocks_in_row;
			float box_width = blocks_x_positions[block_x_index + 1] - blocks_x_positions[block_x_index];
			if (box_width / distance <= theta)
			{
				const float F = (gravitational_constant_mass * center_of_mass.mass) / (distance * distance * distance);
				force.x += F * (center_of_mass.x - pos_x);
				force.y += F * (center_of_mass.y - pos_y);
				force.z += F * (center_of_mass.z - pos_z);
			}
			else
			{
				force += CalculateBlockForceByParticle(particle_index, i, pos_x, pos_y, pos_z, mass);
			}
		}
	}

	const glm::vec3 acc = force / mass;

	// 4. integrate speed
	velocities[particle_pos_start_index] += acc.x;
	velocities[particle_pos_start_index + 1] += acc.y;
	velocities[particle_pos_start_index + 2] += acc.z;

	// 5. integrate position
	float x = pos_x + velocities[particle_pos_start_index] * delta_time;
	float y = pos_y + velocities[particle_pos_start_index + 1] * delta_time;
	float z = pos_z + velocities[particle_pos_start_index + 2] * delta_time;

	m.lock();
	this->SetParticlePositions(particle_index, x, y, z);
	m.unlock();
}

void RCube3D::CalculateParticlePositionViaSolver(int particle_index, float* state, float* step)
{
	const int block_index = particle_block_index[particle_index];
	if (block_index > -1)
	{
		const int particle_pos_start_index = particle_index * 6;
		const float pos_x = state[particle_pos_start_index];
		const float pos_y = state[particle_pos_start_index + 1];
		const float pos_z = state[particle_pos_start_index + 2];

		glm::vec3 force = CalculateBlockForceByParticle(particle_index, block_index, pos_x, pos_y, pos_z);
		for (int i = 0; i < block_neighbors_index[block_index].size(); i++)
		{
			force += CalculateBlockForceByParticle(particle_index, block_neighbors_index[block_index][i], pos_x, pos_y, pos_z);
		}

		m.lock();
		step[particle_pos_start_index] = state[particle_pos_start_index + 3];
		step[particle_pos_start_index + 1] = state[particle_pos_start_index + 4];
		step[particle_pos_start_index + 2] = state[particle_pos_start_index + 5];

		step[particle_pos_start_index + 3] = force.x;
		step[particle_pos_start_index + 4] = force.y;
		step[particle_pos_start_index + 5] = force.z;
		particle_block_index[particle_index] = -1;
		m.unlock();
	}
}

void RCube3D::CalculateParticlePositionWithCenterOfMassViaSolver(int particle_index, float* state, float* step)
{
	const int particle_pos_start_index = particle_index * 6;
	const float pos_x = state[particle_pos_start_index];
	const float pos_y = state[particle_pos_start_index + 1];
	const float pos_z = state[particle_pos_start_index + 2];

	glm::vec3 force(0, 0, 0);

	for (int i = 0; i < number_of_blocks; i++)
	{
		if (block_particles[i].size() > 0)
		{
			Particle center_of_mass = block_center_of_mass[i];
			const float x = pos_x - center_of_mass.x;
			const float y = pos_y - center_of_mass.y;
			const float z = pos_z - center_of_mass.z;
			const float distance = sqrt(x * x + y * y + z * z);

			const int block_x_index = i % blocks_in_row;
			float box_width = blocks_x_positions[block_x_index + 1] - blocks_x_positions[block_x_index];
			if (box_width / distance <= theta)
			{
				const float F = (gravitational_constant * center_of_mass.mass) / (distance * distance * distance);
				force.x += F * (center_of_mass.x - pos_x);
				force.y += F * (center_of_mass.y - pos_y);
				force.z += F * (center_of_mass.y - pos_z);
			}
			else
			{
				force += CalculateBlockForceByParticle(particle_index, i, pos_x, pos_y, pos_z);
			}
		}
	}

	m.lock();
	step[particle_pos_start_index] = state[particle_pos_start_index + 3];
	step[particle_pos_start_index + 1] = state[particle_pos_start_index + 4];
	step[particle_pos_start_index + 2] = state[particle_pos_start_index + 5];

	step[particle_pos_start_index + 3] = force.x;
	step[particle_pos_start_index + 4] = force.y;
	step[particle_pos_start_index + 5] = force.z;
	m.unlock();
}

glm::vec3 RCube3D::CalculateBlockForceByParticle(const int& current_particle_index, const int& block_index, const float& pos_x, const float& pos_y, const float& pos_z, const float& mass)
{
	float* positions = this->GetPositions();
	float* masses = this->GetMasses();
	const float gravitational_constant_mass = gravitational_constant * mass;
	glm::vec3 force(0, 0, 0);
	for (int i = 0; i < block_particles[block_index].size(); i++)
	{
		const int particle_index = block_particles[block_index][i];
		if (current_particle_index == particle_index)
		{
			continue;
		}
		const int particle_pos_start_index = particle_index * 3;

		const float x = pos_x - positions[particle_pos_start_index];
		const float y = pos_y - positions[particle_pos_start_index + 1];
		const float z = pos_z - positions[particle_pos_start_index + 2];
		const float distance = sqrt(x * x + y * y + z * z + lambda);
		if (distance > 0)
		{
			const float F = (gravitational_constant_mass * masses[particle_index]) / (distance * distance * distance);
			force.x += F * (positions[particle_pos_start_index] - pos_x);
			force.y += F * (positions[particle_pos_start_index + 1] - pos_y);
			force.z += F * (positions[particle_pos_start_index + 2] - pos_z);
		}
	}
	return force;
}

std::vector<glm::vec3> RCube3D::GetAxis()
{
	std::vector<glm::vec3> axis;
	for (int i = 0; i < number_of_blocks; i++)
	{
		std::vector<glm::vec3> block_indexes = GetBlockCoordinates(i);
		axis.push_back(block_indexes[0]);
		axis.push_back(block_indexes[1]);
	}
	return axis;
}