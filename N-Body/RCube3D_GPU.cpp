#include "RCube3D_GPU.h"

RCube3D_GPU::RCube3D_GPU(const bool& center_of_mass_logic, const int& number_of_particles, const float& gravitational_constant, const float& theta, const float& lambda, const int& device_index) :
Model(number_of_particles, gravitational_constant, theta, lambda, true),
RCubeBaseGPU(number_of_particles, gravitational_constant, theta, lambda, true, center_of_mass_logic, device_index)
{}

bool RCube3D_GPU::InitGPU()
{
	RCubeBaseGPU::InitGPU(source_code_file, min_x, max_x, min_y, max_y, this->GetPositions(), this->GetVelocities(), this->GetMasses(), min_z, max_z);
	if (gpu_inited)
	{
		if (!center_of_mass_logic)
		{
			SetBlockNeighbours();
		}
	}
	return gpu_inited;
}

void RCube3D_GPU::Update(float delta_time)
{
	if (gpu_inited)
	{
		ResetVariables();
		SetBlockPositions();
		SetParticlesData();
		if (center_of_mass_logic)
		{
			SetBlockCenterOfMass();
			CalculateParticlePositionWithCenterOfMass(13, delta_time);
		}
		else
		{
			CalculateParticlePosition(14, delta_time);
		}
		ReadBackPositions(this->GetPositions());
	}
}

void RCube3D_GPU::UpdateViaSolver(float* state, float* step, bool skip)
{
	if (gpu_inited)
	{
		if (!skip)
		{
			this->CopyDataFromSolverState(state);
		}
		WriteIntoPositionsBuffer(this->GetPositions());
		ResetMinMaxBuffers(min_x, max_x, min_y, max_y, min_z, max_z);
		SetSolverBuffers(state, step);

		ResetVariables();
		SetBlockPositions();
		SetParticlesData();
		if (center_of_mass_logic)
		{
			SetBlockCenterOfMass();
			CalculateParticlePositionWithCenterOfMassViaSolver();
		}
		else
		{
			CalculateParticlePositionViaSolver();
		}

		ReadBackPositions(this->GetPositions());
		ReadBackSolverSteps(step);
	}
}

std::vector<glm::vec3> RCube3D_GPU::GetAxis()
{
	std::vector<glm::vec3> axis;

	if (gpu_inited)
	{
		const int vector_size = blocks_in_row + 1;
		const size_t size = sizeof(float) * vector_size;
		std::vector<float> block_x_positions = std::vector<float>(vector_size);
		command_queue.enqueueReadBuffer(cl_block_x_positions, CL_TRUE, 0, size, &block_x_positions[0]);

		std::vector<float> block_y_positions = std::vector<float>(vector_size);
		command_queue.enqueueReadBuffer(cl_block_y_positions, CL_TRUE, 0, size, &block_y_positions[0]);

		std::vector<float> block_z_positions = std::vector<float>(vector_size);
		command_queue.enqueueReadBuffer(cl_block_z_positions, CL_TRUE, 0, size, &block_z_positions[0]);


		for (int i = 0; i < number_of_blocks; i++)
		{
			const int x_index = i % blocks_in_row;
			int y_index = i / blocks_in_row;
			if (y_index >= blocks_in_row)
			{
				y_index %= blocks_in_row;
			}
			const int z_index = i / blocks_in_row_pow;

			axis.push_back(glm::vec3(block_x_positions[x_index], block_y_positions[y_index], block_z_positions[z_index]));
			axis.push_back(glm::vec3(block_x_positions[x_index + 1], block_y_positions[y_index + 1], block_z_positions[z_index + 1]));
		}
	}

	return axis;
}
