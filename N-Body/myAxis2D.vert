#version 150

// values to be passed on in the pipeline
out vec3 vs_out_col;

uniform mat4 MVP;

const int offset_size = 500;
uniform vec3 min_coordinates[offset_size];
uniform vec3 max_coordinates[offset_size];

const vec3 color = vec3(0, 1, .2);

void main()
{
    vec3 min_offset = min_coordinates[gl_VertexID / 8];
    vec3 max_offset = max_coordinates[gl_VertexID / 8];

	int current_position = gl_VertexID % 8;
	if(current_position == 0 || current_position == 7)
	{
		gl_Position = MVP * vec4(min_offset.x, min_offset.y, 0, 1);
	}
	else if(current_position == 1 || current_position == 2)
	{
		gl_Position = MVP * vec4(max_offset.x, min_offset.y, 0, 1);
	}
	else if(current_position == 3 || current_position == 4)
	{
		gl_Position = MVP * vec4(max_offset.x, max_offset.y, 0, 1);
	}
	else /*if(current_position == 5 || current_position == 6)*/
	{
		gl_Position = MVP * vec4(min_offset.x, max_offset.y, 0, 1);
	}

	vs_out_col = color;
}