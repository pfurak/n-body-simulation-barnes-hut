// ================================================================== //
__kernel void SetBlockNeighbors(
	__global int* block_neighbors_index,
	__global int* block_neighbors_size
)
{
	const int max_neighbors = 6;
	int neighbors_counter = 0;
	const int block_index = get_global_id(0);
	const int start_index = block_index * max_neighbors;

	float3 indexes = GetBlockIndexes(block_index);
	
	const int x_index = indexes.x;
	const int y_index = indexes.y;
	const int z_index = indexes.z;

	if (x_index - 1 >= 0)
	{
		block_neighbors_index[start_index + neighbors_counter] = block_index - 1;
		neighbors_counter++;
	}

	if (x_index + 1 < BLOCKS_IN_ROW)
	{
		block_neighbors_index[start_index + neighbors_counter] = block_index + 1;
		neighbors_counter++;
	}

	if (y_index - 1 >= 0)
	{
		block_neighbors_index[start_index + neighbors_counter] = block_index - BLOCKS_IN_ROW;
		neighbors_counter++;
	}

	if (y_index + 1 < BLOCKS_IN_ROW)
	{
		block_neighbors_index[start_index + neighbors_counter] = block_index + BLOCKS_IN_ROW;
		neighbors_counter++;
	}

	if (z_index - 1 >= 0)
	{
		block_neighbors_index[start_index + neighbors_counter] = block_index - BLOCKS_IN_ROW_POW;
		neighbors_counter++;
	}

	if (z_index + 1 < BLOCKS_IN_ROW)
	{
		block_neighbors_index[start_index + neighbors_counter] = block_index + BLOCKS_IN_ROW_POW;
		neighbors_counter++;
	}

	block_neighbors_size[block_index] = neighbors_counter;
}

// ================================================================== //
__kernel void SetBlockPositions(
	__global float* min_x,
	__global float* max_x,
	__global float* min_y,
	__global float* max_y,
	__global float* block_x_positions,
	__global float* block_y_positions,
	__global float* min_z,
	__global float* max_z,
	__global float* block_z_positions
)
{
	const float x_step = (*max_x - *min_x) / BLOCKS_IN_ROW;
	const float y_step = (*max_y - *min_y) / BLOCKS_IN_ROW;
	const float z_step = (*max_z - *min_z) / BLOCKS_IN_ROW;

	block_x_positions[0] = *min_x;
	block_y_positions[0] = *min_y;
	block_z_positions[0] = *min_z;
	for (int i = 1; i < BLOCKS_IN_ROW; i++)
	{
		block_x_positions[i] = block_x_positions[i - 1] + x_step;
		block_y_positions[i] = block_y_positions[i - 1] + y_step;
		block_z_positions[i] = block_z_positions[i - 1] + z_step;
	}
	block_x_positions[BLOCKS_IN_ROW] = *max_x;
	block_y_positions[BLOCKS_IN_ROW] = *max_y;
	block_z_positions[BLOCKS_IN_ROW] = *max_z;
}

// ================================================================== //
__kernel void SetParticlesData(
	__global float* positions,
	__global float* block_x_positions,
	__global float* block_y_positions,
	__global int* block_particles,
	volatile __global int* block_particles_size,
	__global int* particle_block_index,
	__global float* block_z_positions
)
{
	const int particle_index = get_global_id(0);
	const float3 particle_coordinates = GetParticleCoordinates(positions, particle_index);

	int block_index = 0;
	bool found = false;
	while (block_index < NUMBER_OF_BLOCKS && !found)
	{
		float3 indexes = GetBlockIndexes(block_index);
		const int x_index = indexes.x;
		const int y_index = indexes.y;
		const int z_index = indexes.z;
		
		const float block_min_x = block_x_positions[x_index];
		const float block_max_x = block_x_positions[x_index + 1];
		
		const float block_min_y = block_y_positions[y_index];
		const float block_max_y = block_y_positions[y_index + 1];

		const float block_min_z = block_z_positions[z_index];
		const float block_max_z = block_z_positions[z_index + 1];
		if (
			block_min_x <= particle_coordinates.x && particle_coordinates.x <= block_max_x &&
			block_min_y <= particle_coordinates.y && particle_coordinates.y <= block_max_y &&
			block_min_z <= particle_coordinates.z && particle_coordinates.z <= block_max_z
		)
		{
			found = true;
		}
		else
		{
			block_index++;
		}
	}

	if (found)
	{
		const int block_data_start_index = block_index * NUMBER_OF_PARTICLES;
		const int block_size = atomic_inc(&block_particles_size[block_index]); // it's always return the old value
		block_particles[block_data_start_index + block_size] = particle_index;
		particle_block_index[particle_index] = block_index;
	}
}

// ================================================================== //
__kernel void SetBlockCenterOfMass(
	__global float* positions,
	__global float* masses,
	__global float* block_center_of_mass,
	__global int* block_particles,
	__global int* block_particles_size
)
{
	const int block_index = get_global_id(0);
	const int block_particles_start_index = block_index * NUMBER_OF_PARTICLES;
	float3 coords = { 0.0f, 0.0f, 0.0f };
	float mass = 0;
	
	if(block_particles_size[block_index] > 0)
	{
		for(int i = 0; i < block_particles_size[block_index]; i++)
		{
			const int particle_index = block_particles[block_particles_start_index + i];
			const float3 particle_coordinates = GetParticleCoordinates(positions, particle_index);

			mass += masses[particle_index];
			coords += particle_coordinates * masses[particle_index];
		}

		coords /= mass;
	}
	
	const int block_center_of_mass_start_index = block_index * 4;
	block_center_of_mass[block_center_of_mass_start_index] = coords.x;
	block_center_of_mass[block_center_of_mass_start_index + 1] = coords.y;
	block_center_of_mass[block_center_of_mass_start_index + 2] = coords.z;
	block_center_of_mass[block_center_of_mass_start_index + 3] = mass;
}

// ================================================================== //
__kernel void CalculateParticlePositionWithCenterOfMass(
	__global float* positions,
	__global float* velocities,
	__global float* masses,
	__global float* block_center_of_mass,
	__global float* block_x_positions,
	__global float* min_x,
	__global float* max_x,
	__global float* min_y,
	__global float* max_y,
	__global int* block_particles_size,
	__global int* block_particles,
	__global float* min_z,
	__global float* max_z,
	float delta_time
)
{
	const int particle_index = get_global_id(0);
	
	const float3 particle_coordinates = GetParticleCoordinates(positions, particle_index);
	const float mass = masses[particle_index];
	
	const float gravitational_constant_mass = GRAVITATIONAL_CONSTANT * mass;
	float3 force = { 0.0f, 0.0f, 0.0f };

	for (int i = 0; i < NUMBER_OF_BLOCKS; i++)
	{
		if (block_particles_size[i] > 0)
		{
			const int block_center_of_mass_index = i * 4;
			const float center_of_mass_x = block_center_of_mass[block_center_of_mass_index];
			const float center_of_mass_y = block_center_of_mass[block_center_of_mass_index + 1];
			const float center_of_mass_z = block_center_of_mass[block_center_of_mass_index + 2];
			const float center_of_mass_mass = block_center_of_mass[block_center_of_mass_index + 3];
		
			const float x = particle_coordinates.x - center_of_mass_x;
			const float y = particle_coordinates.y - center_of_mass_y;
			const float z = particle_coordinates.z - center_of_mass_z;
			const float distance = sqrt(x * x + y * y + z * z);

			const int block_x_index = i % BLOCKS_IN_ROW;
			float box_width = block_x_positions[block_x_index + 1] - block_x_positions[block_x_index];
			if (box_width / distance <= THETA)
			{
				const float F = (gravitational_constant_mass * center_of_mass_mass) / (distance * distance * distance);
				force.x += F * (center_of_mass_x - particle_coordinates.x);
				force.y += F * (center_of_mass_y - particle_coordinates.y);
				force.z += F * (center_of_mass_z - particle_coordinates.z);
			}
			else
			{
				force += CalculateBlockForceByParticle(
					positions,
					masses,
					block_particles_size,
					block_particles,
					particle_index,
					i,
					particle_coordinates.x,
					particle_coordinates.y,
					particle_coordinates.z,
					gravitational_constant_mass
				);
			}
		}
	}

	// aggregate acceleration
	const float3 acceleration = force / mass;

	// 4. integrate speed
	const int particle_pos_start_index = particle_index * 3;
	velocities[particle_pos_start_index] += acceleration.x;
	velocities[particle_pos_start_index + 1] += acceleration.y;
	velocities[particle_pos_start_index + 2] += acceleration.z;

	// 5. integrate position
	float x = particle_coordinates.x + velocities[particle_pos_start_index] * delta_time;
	float y = particle_coordinates.y + velocities[particle_pos_start_index + 1] * delta_time;
	float z = particle_coordinates.z + velocities[particle_pos_start_index + 2] * delta_time;

	positions[particle_pos_start_index] = x;
	positions[particle_pos_start_index + 1] = y;
	positions[particle_pos_start_index + 2] = z;

	fatomic_min(min_x, x);
	fatomic_max(max_x, x);

	fatomic_min(min_y, y);
	fatomic_max(max_y, y);

	fatomic_min(min_z, z);
	fatomic_max(max_z, z);
}

// ================================================================== //
__kernel void CalculateParticlePosition(
	__global int* particle_block_index,
	__global float* positions,
	__global float* velocities,
	__global float* masses,
	__global int* block_neighbors_size,
	__global int* block_neighbors_index,
	__global float* min_x,
	__global float* max_x,
	__global float* min_y,
	__global float* max_y,
	__global int* block_particles_size,
	__global int* block_particles,
	__global float* min_z,
	__global float* max_z,
	float delta_time
)
{
	const int particle_index = get_global_id(0);
	const int block_index = particle_block_index[particle_index];
	
	if (block_index > -1)
	{
		const float3 particle_coordinates = GetParticleCoordinates(positions, particle_index);
		const float mass = masses[particle_index];
		const float gravitational_constant_mass = GRAVITATIONAL_CONSTANT * mass;

		float3 force = CalculateBlockForceByParticle(
			positions,
			masses,
			block_particles_size,
			block_particles,
			particle_index,
			block_index,
			particle_coordinates.x,
			particle_coordinates.y,
			particle_coordinates.z,
			gravitational_constant_mass
		);

		const int block_neighbors_start_index = block_index * 6;
		for (int i = 0; i < block_neighbors_size[block_index]; i++)
		{
			force += CalculateBlockForceByParticle(
				positions,
				masses,
				block_particles_size,
				block_particles,
				particle_index,
				block_neighbors_index[block_neighbors_start_index + i],
				particle_coordinates.x,
				particle_coordinates.y,
				particle_coordinates.z,
				gravitational_constant_mass
			);
		}

		// aggregate acceleration
		const float3 acceleration = force / mass;

		// 4. integrate speed
		const int particle_pos_start_index = particle_index * 3;
		velocities[particle_pos_start_index] += acceleration.x;
		velocities[particle_pos_start_index + 1] += acceleration.y;
		velocities[particle_pos_start_index + 2] += acceleration.z;

		// 5. integrate position
		float x = particle_coordinates.x + velocities[particle_pos_start_index] * delta_time;
		float y = particle_coordinates.y + velocities[particle_pos_start_index + 1] * delta_time;
		float z = particle_coordinates.z + velocities[particle_pos_start_index + 2] * delta_time;

		positions[particle_pos_start_index] = x;
		positions[particle_pos_start_index + 1] = y;
		positions[particle_pos_start_index + 2] = z;

		fatomic_min(min_x, x);
		fatomic_max(max_x, x);

		fatomic_min(min_y, y);
		fatomic_max(max_y, y);

		fatomic_min(min_z, z);
		fatomic_max(max_z, z);
		
		particle_block_index[particle_index] = -1;
	}
}

// ================================================================== //
__kernel void CalculateParticlePositionWithCenterOfMassViaSolver(
	__global float* positions,
	__global float* masses,
	__global int* block_particles_size,
	__global int* block_particles,
	__global float* block_center_of_mass,
	__global float* block_x_positions,
	__global float* state,
	__global float* step
)
{
	const int particle_index = get_global_id(0);
	const int particle_pos_start_index = particle_index * 6;
	const float pos_x = state[particle_pos_start_index];
	const float pos_y = state[particle_pos_start_index + 1];
	const float pos_z = state[particle_pos_start_index + 2];

	float3 force = { 0.0f, 0.0f, 0.0f };

	for (int i = 0; i < NUMBER_OF_BLOCKS; i++)
	{
		if (block_particles_size[i] > 0)
		{
			const int block_center_of_mass_start_index = i * 4;
			const float block_center_of_mass_x = block_center_of_mass[block_center_of_mass_start_index];
			const float block_center_of_mass_y = block_center_of_mass[block_center_of_mass_start_index + 1];
			const float block_center_of_mass_z = block_center_of_mass[block_center_of_mass_start_index + 2];
			const float block_center_of_mass_mass = block_center_of_mass[block_center_of_mass_start_index + 3];

			const float x = pos_x - block_center_of_mass_x;
			const float y = pos_y - block_center_of_mass_y;
			const float z = pos_z - block_center_of_mass_z;
			const float distance = sqrt(x * x + y * y + z * z);

			const int block_x_index = i % BLOCKS_IN_ROW;
			float box_width = block_x_positions[block_x_index + 1] - block_x_positions[block_x_index];
			if (box_width / distance <= THETA)
			{
				const float F = (GRAVITATIONAL_CONSTANT * block_center_of_mass_mass) / (distance * distance * distance);
				force.x += F * (block_center_of_mass_x - pos_x);
				force.y += F * (block_center_of_mass_y - pos_y);
				force.z += F * (block_center_of_mass_z - pos_z);
			}
			else
			{
				force += CalculateBlockForceByParticle(
					positions,
					masses,
					block_particles_size,
					block_particles,
					particle_index,
					i,
					pos_x,
					pos_y,
					pos_z,
					GRAVITATIONAL_CONSTANT
				);
			}
		}
	}

	step[particle_pos_start_index] = state[particle_pos_start_index + 3];
	step[particle_pos_start_index + 1] = state[particle_pos_start_index + 4];
	step[particle_pos_start_index + 2] = state[particle_pos_start_index + 5];

	step[particle_pos_start_index + 3] = force.x;
	step[particle_pos_start_index + 4] = force.y;
	step[particle_pos_start_index + 5] = force.z;
}

// ================================================================== //
__kernel void CalculateParticlePositionViaSolver(
	__global float* positions,
	__global float* masses,
	__global int* block_particles_size,
	__global int* block_particles,
	__global int* particle_block_index,
	__global int* block_neighbors_size,
	__global int* block_neighbors_index,
	__global float* state,
	__global float* step
)
{
	const int particle_index = get_global_id(0);
	const int block_index = particle_block_index[particle_index];
	if (block_index > -1)
	{
		const int particle_pos_start_index = particle_index * 6;
		const float pos_x = state[particle_pos_start_index];
		const float pos_y = state[particle_pos_start_index + 1];
		const float pos_z = state[particle_pos_start_index + 2];
		float3 force = CalculateBlockForceByParticle(
			positions,
			masses,
			block_particles_size,
			block_particles,
			particle_index,
			block_index,
			pos_x,
			pos_y,
			pos_z,
			GRAVITATIONAL_CONSTANT
		);
		
		const int block_neighbors_start_index = block_index * 6;
		for (int i = 0; i < block_neighbors_size[block_index]; i++)
		{
			force += CalculateBlockForceByParticle(
				positions,
				masses,
				block_particles_size,
				block_particles,
				particle_index,
				block_neighbors_index[block_neighbors_start_index + i],
				pos_x,
				pos_y,
				pos_z,
				GRAVITATIONAL_CONSTANT
			);
		}

		step[particle_pos_start_index] = state[particle_pos_start_index + 3];
		step[particle_pos_start_index + 1] = state[particle_pos_start_index + 4];
		step[particle_pos_start_index + 2] = state[particle_pos_start_index + 5];

		step[particle_pos_start_index + 3] = force.x;
		step[particle_pos_start_index + 4] = force.y;
		step[particle_pos_start_index + 5] = force.z;

		particle_block_index[particle_index] = -1;
	}
}