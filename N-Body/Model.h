#pragma once

// GLM
#include <glm/glm.hpp>

#include <vector>
#include <thread>
#include <algorithm>
#include <functional>
#include <mutex>
#include <math.h>

#include "BHTreeNode.h"
#include "BHTreeNode3D.h"
#include "Structs.h"

#ifdef _WINDOWS_
//#define MAX_CPU_THREAD 1550
#define MAX_CPU_THREAD 32
#else
#define MAX_CPU_THREAD 2500
#endif

class Model
{
public:
    Model(const int&, const float&, const float&, const float&, bool);
    virtual ~Model();
    void RegisterParticle(float, float, float, float, float, float, float);
    float* GetPositions();
    float* GetVelocities();
    void SetParticlePositions(const int&, float, float, float = 0.0f);
    void SetParticleVelocities(const int&, float, float, float = 0.0f);
    virtual bool InitGPU() { return true; }
    virtual void Update(const float) = 0;
    virtual void UpdateViaSolver(float*, float*, bool=false) {};
    virtual std::vector<glm::vec3> GetAxis() = 0;
private:
    float* positions;
    float* velocities;
    float* masses;
    int particle_counter;

    void SetMinMaxCoordinates(float, float, float = 0.0f);
protected:
    int number_of_particles;
    
    float gravitational_constant;
    float theta;
    float lambda;

    bool is_3d;

    float min_x;
    float max_x;
    float min_y;
    float max_y;
    float min_z;
    float max_z;

    float* GetMasses();
    void CopyDataFromSolverState(float*);
};