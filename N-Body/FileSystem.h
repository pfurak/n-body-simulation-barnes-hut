#pragma once

#include <string>
#include <sstream>

#ifdef _WIN32
	#ifndef __WINDOWS__
		#define __WINDOWS__ 1
	#endif
#endif

#if (defined(linux) || defined(__linux) || defined(__linux__))
	#ifndef __LINUX__
		#define __LINUX__ 1
	#endif
#endif

#ifdef __WINDOWS__
	#include <windows.h>
#endif

#ifdef __LINUX__
	#include <linux/limits.h>
	#include <unistd.h>
	#include <sys/stat.h>
#endif

#ifdef __APPLE__
	#include <mach-o/dyld.h>
#endif

#include "Logger.h"

class FileSystem
{
public:
	static std::string app_path;
	static std::string bin_path;
	static std::string print_screen_path;
	static void SetPathVariables();
	static int CreateDir(const std::string&);
	static void OpenWindowExplorer(const std::string&);

private:
	static std::string GetAppPath();
};

