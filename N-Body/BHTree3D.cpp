#include "BHTree3D.h"

BHTree3D::BHTree3D(const int& number_of_particles, const float& gravitational_constant, const float& theta, const float& lambda) :
Model(number_of_particles, gravitational_constant, theta, lambda, true)
{
	root_node = new BHTreeNode3D(theta, lambda);
}

BHTree3D::~BHTree3D()
{
	root_node->ResetTree();
	delete root_node;
	root_node = NULL;
}

void BHTree3D::Update(const float delta_time)
{
	BuildTree();
	UpdateParticlePositions(delta_time);
}

void BHTree3D::UpdateViaSolver(float* state, float* step, bool skip)
{
	if (!skip)
	{
		this->CopyDataFromSolverState(state);
	}

	BuildTree();
	for (int i = 0; i < number_of_particles; i++)
	{
		const int start_index = i * 6;
		glm::vec3 force = root_node->GetForce(i, state[start_index], state[start_index + 1], state[start_index + 2], 1, gravitational_constant);
		step[start_index] = state[start_index + 3];
		step[start_index + 1] = state[start_index + 4];
		step[start_index + 2] = state[start_index + 5];
			
		step[start_index + 3] = force.x;
		step[start_index + 4] = force.y;
		step[start_index + 5] = force.z;
	}
}

void BHTree3D::BuildTree()
{
	root_node->ResetTree();
	float* positions = this->GetPositions();
	float* masses = this->GetMasses();
	root_node->SetMinMaxCoordinates(min_x, max_x, min_y, max_y, min_z, max_z);
	for (int i = 0; i < number_of_particles; i++)
	{
		const int particle_start_position = i * 3;
		root_node->AddParticle(i, Particle(positions[particle_start_position], positions[particle_start_position + 1], positions[particle_start_position + 2], masses[i]));
	}
	root_node->SetCenterOfMass();
}

void BHTree3D::UpdateParticlePositions(const float delta_time)
{
	float* positions = this->GetPositions();
	float* velocities = this->GetVelocities();
	float* masses = this->GetMasses();

	for (int i = 0; i < number_of_particles; i++)
	{
		const int particle_start_position = i * 3;
		glm::vec3 force = root_node->GetForce(i, positions[particle_start_position], positions[particle_start_position + 1], positions[particle_start_position + 2], masses[i], gravitational_constant);
		const glm::vec3 acc = force / masses[i];
		
		velocities[particle_start_position] += acc.x;
		velocities[particle_start_position + 1] += acc.y;
		velocities[particle_start_position + 2] += acc.z;

		const float x = positions[particle_start_position] + velocities[particle_start_position] * delta_time;
		const float y = positions[particle_start_position + 1] + velocities[particle_start_position + 1] * delta_time;
		const float z = positions[particle_start_position + 2] + velocities[particle_start_position + 2] * delta_time;

		this->SetParticlePositions(i, x, y, z);
	}
}

std::vector<glm::vec3> BHTree3D::GetAxis()
{
	return root_node->GetAxis();
}