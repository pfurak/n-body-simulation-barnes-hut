#pragma once
#pragma warning(disable : 4996)

#include <fstream>
#include <iomanip>
#include <ctime>
#include <string>
#include "StringConverter.h"
#include "FileSystem.h"

class Logger
{
public:
	static void LogError(const char*);
private:
	static const std::string log_file;
};

