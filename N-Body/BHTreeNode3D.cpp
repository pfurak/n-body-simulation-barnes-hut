#include "BHTreeNode3D.h"

std::vector<Particle> BHTreeNode3D::out_of_tree_particles;
float BHTreeNode3D::theta;
float BHTreeNode3D::lambda;

BHTreeNode3D::BHTreeNode3D(float theta, float lambda)
{
	BHTreeNode3D::theta = theta;
	BHTreeNode3D::lambda = lambda;

	particle_index = -1;

	for (int i = 0; i < 8; i++)
	{
		node[i] = NULL;
	}
}

BHTreeNode3D::BHTreeNode3D(float min_x, float max_x, float min_y, float max_y, float min_z, float max_z)
{
	this->min_x = min_x;
	this->max_x = max_x;
	this->min_y = min_y;
	this->max_y = max_y;
	this->min_z = min_z;
	this->max_z = max_z;

	box_width = max_x - min_x;
	particle_index = -1;

	for (int i = 0; i < 8; i++)
	{
		node[i] = NULL;
	}
}

BHTreeNode3D::~BHTreeNode3D()
{
	for (int i = 0; i < 8; i++)
	{
		if (node[i] != NULL)
		{
			delete node[i];
			node[i] = NULL;
		}
	}
}

void BHTreeNode3D::SetMinMaxCoordinates(float min_x, float max_x, float min_y, float max_y, float min_z, float max_z)
{
	this->min_x = min_x;
	this->max_x = max_x;
	this->min_y = min_y;
	this->max_y = max_y;
	this->min_z = min_z;
	this->max_z = max_z;

	box_width = max_x - min_x;
}

bool BHTreeNode3D::HasChild()
{
	return (
		node[0] != NULL ||
		node[1] != NULL ||
		node[2] != NULL ||
		node[3] != NULL ||
		node[4] != NULL ||
		node[5] != NULL ||
		node[6] != NULL ||
		node[7] != NULL
		);
}

void BHTreeNode3D::AddParticle(int particle_index, Particle particle)
{
	/*
	 * |=========|=========|===* max_y
	 * |         |         |   |
	 * |   NW    |   NE    |   |
	 * |  node1  |  node2  |   |
	 * |=========|=========|===*max_y (min_y of next block)
	 * |         |         |   |
	 * |   SW    |   SE    |  /
	 * |  node3  |  node4  | /
	 * |=========|=========|/ min_y
	 * min_x   max_x     max_x
	 *      (min_x of next)
	 */

	if (!HasChild() && this->particle_index == -1)
	{
		this->particle_index = particle_index;
		this->particle = particle;
	}
	else
	{
		int node_index;

		// if my child doesn't stored in a child leaf
		// because this is the last leaf of tree, and I need to store my particle into a new leaf
		if (this->particle_index > -1)
		{
			if(particle.x == this->particle.x && particle.y == this->particle.y && particle.z == this->particle.z)
			{
				// skip this particle, or we get stack overflow
				out_of_tree_particles.push_back(particle);
				return;
			}
			
			// First, get node index and add current particle into the new node
			node_index = GetNodeIndex(this->particle.x, this->particle.y, this->particle.z);
			if (node_index > -1)
			{
				node[node_index]->AddParticle(this->particle_index, this->particle);
				this->particle_index = -1;
			}
		}

		// Second, get node index and add new particle into one of the new nodes
		node_index = GetNodeIndex(particle.x, particle.y, particle.z);
		if (node_index > -1)
		{
			node[node_index]->AddParticle(particle_index, particle);
		}
	}
}

int BHTreeNode3D::GetNodeIndex(const float particle_x, const float particle_y, const float particle_z)
{
	const float x_step = (max_x - min_x) / 2;
	float middle_x = min_x + x_step;

	const float y_step = (max_y - min_y) / 2;
	float middle_y = min_y + y_step;

	const float z_step = (max_z - min_z) / 2;
	float middle_z = min_z + z_step;

	if(min_z <= particle_z && particle_z <= middle_z)
	{
		if (min_x <= particle_x && particle_x <= middle_x)
		{
			// NW (index: 0)
			if (middle_y <= particle_y && particle_y <= max_y)
			{
				if (node[0] == NULL)
				{
					node[0] = new BHTreeNode3D(min_x, middle_x, middle_y, max_y, min_z, middle_z);
				}

				return 0;
			}

			// SW (index: 2)
			if (min_y <= particle_y && particle_y <= middle_y)
			{
				if (node[2] == NULL)
				{
					node[2] = new BHTreeNode3D(min_x, middle_x, min_y, middle_y, min_z, middle_z);
				}

				return 2;
			}
		}

		if (middle_x <= particle_x && particle_x <= max_x)
		{
			// NE (index: 1)
			if (middle_y <= particle_y && particle_y <= max_y)
			{
				if (node[1] == NULL)
				{
					node[1] = new BHTreeNode3D(middle_x, max_x, middle_y, max_y, min_z, middle_z);
				}

				return 1;
			}

			// SE (index: 3)
			if (min_y <= particle_y && particle_y <= middle_y)
			{
				if (node[3] == NULL)
				{
					node[3] = new BHTreeNode3D(middle_x, max_x, min_y, middle_y, min_z, middle_z);
				}

				return 3;
			}
		}
	}
	
	if (middle_z <= particle_z && particle_z <= max_z)
	{
		if (min_x <= particle_x && particle_x <= middle_x)
		{
			// NW (index: 4)
			if (middle_y <= particle_y && particle_y <= max_y)
			{
				if (node[4] == NULL)
				{
					node[4] = new BHTreeNode3D(min_x, middle_x, middle_y, max_y, middle_z, max_z);
				}

				return 4;
			}

			// SW (index: 6)
			if (min_y <= particle_y && particle_y <= middle_y)
			{
				if (node[6] == NULL)
				{
					node[6] = new BHTreeNode3D(min_x, middle_x, min_y, middle_y, middle_z, max_z);
				}

				return 6;
			}
		}

		if (middle_x <= particle_x && particle_x <= max_x)
		{
			// NE (index: 5)
			if (middle_y <= particle_y && particle_y <= max_y)
			{
				if (node[5] == NULL)
				{
					node[5] = new BHTreeNode3D(middle_x, max_x, middle_y, max_y, middle_z, max_z);
				}

				return 5;
			}

			// SE (index: 7)
			if (min_y <= particle_y && particle_y <= middle_y)
			{
				if (node[7] == NULL)
				{
					node[7] = new BHTreeNode3D(middle_x, max_x, min_y, middle_y, middle_z, max_z);
				}

				return 7;
			}
		}
	}

	return -1;
}

void BHTreeNode3D::SetCenterOfMass()
{
	center_of_mass = Particle(0, 0, 0, 0);
	for (int i = 0; i < 8; i++)
	{
		if (node[i] != NULL)
		{
			Particle quad_node_center_of_mass = node[i]->GetCenterOfMass();

			center_of_mass.mass += quad_node_center_of_mass.mass;
			center_of_mass.x += quad_node_center_of_mass.x * quad_node_center_of_mass.mass;
			center_of_mass.y += quad_node_center_of_mass.y * quad_node_center_of_mass.mass;
			center_of_mass.z += quad_node_center_of_mass.z * quad_node_center_of_mass.mass;
		}
	}
	center_of_mass.x /= center_of_mass.mass;
	center_of_mass.y /= center_of_mass.mass;
	center_of_mass.z /= center_of_mass.mass;
}

Particle BHTreeNode3D::GetCenterOfMass()
{
	if (particle_index > -1)
	{
		return particle;
	}

	SetCenterOfMass();
	return center_of_mass;
}

glm::vec3 BHTreeNode3D::CalculateForce(int particle_index, float pos_x, float pos_y, float pos_z, float mass, float gravitational_constant)
{
	glm::vec3 force(0, 0, 0);
	if (this->particle_index > -1)
	{
		if (this->particle_index != particle_index)
		{
			const float x = pos_x - particle.x;
			const float y = pos_y - particle.y;
			const float z = pos_z - particle.z;
			const float distance = sqrt(x * x + y * y + z * z + lambda);
			if (distance > 0)
			{
				const float F = (gravitational_constant * mass * particle.mass) / (distance * distance * distance);
				force.x = F * (particle.x - pos_x);
				force.y = F * (particle.y - pos_y);
				force.z = F * (particle.z - pos_z);
			}
		}
	}
	else
	{
		const float x = pos_x - center_of_mass.x;
		const float y = pos_y - center_of_mass.y;
		const float z = pos_z - center_of_mass.z;
		const float distance = sqrt(x * x + y * y + z * z);
		if (box_width / distance <= theta)
		{
			const float F = (gravitational_constant * mass * center_of_mass.mass) / (distance * distance * distance);
			force.x = F * (center_of_mass.x - pos_x);
			force.y = F * (center_of_mass.y - pos_y);
			force.z = F * (center_of_mass.z - pos_z);
		}
		else
		{
			for (int i = 0; i < 8; i++)
			{
				if (node[i] != NULL)
				{
					force += node[i]->CalculateForce(particle_index, pos_x, pos_y, pos_z, mass, gravitational_constant);
				}
			}
		}
	}

	return force;
}

glm::vec3 BHTreeNode3D::CalculateAcceleration(Particle current_particle, const int& index, float gravitational_constant)
{
	glm::vec3 acc(0, 0, 0);
	if (current_particle.x == out_of_tree_particles[index].x && current_particle.y == out_of_tree_particles[index].y && current_particle.z == out_of_tree_particles[index].z)
	{
		return acc;
	}

	const float x = current_particle.x - out_of_tree_particles[index].x;
	const float y = current_particle.y - out_of_tree_particles[index].y;
	const float z = current_particle.z - out_of_tree_particles[index].z;
	const float distance = sqrt(x * x + y * y + z * z + lambda);
	if (distance > 0)
	{
		const float F = gravitational_constant * out_of_tree_particles[index].mass / (distance * distance * distance);
		acc.x = F * (out_of_tree_particles[index].x - current_particle.x);
		acc.y = F * (out_of_tree_particles[index].y - current_particle.y);
		acc.z = F * (out_of_tree_particles[index].z - current_particle.z);
	}

	return acc;
}

glm::vec3 BHTreeNode3D::GetForce(int particle_index, float pos_x, float pos_y, float pos_z, float mass, float gravitational_constant)
{
	glm::vec3 force = CalculateForce(particle_index, pos_x, pos_y, pos_z, mass, gravitational_constant);
	if (out_of_tree_particles.size() > 0)
	{
		Particle current_particle(pos_x, pos_y, mass, pos_z);
		for (int i = 0; i < out_of_tree_particles.size(); i++)
		{
			force += CalculateAcceleration(current_particle, i, gravitational_constant);
		}
	}
	return force;
}

void BHTreeNode3D::ResetTree()
{
	if (out_of_tree_particles.size() > 0)
	{
		out_of_tree_particles.clear();
	}

	for (int i = 0; i < 8; i++)
	{
		if (node[i] != NULL)
		{
			delete node[i];
			node[i] = NULL;
		}
	}
}

std::vector<glm::vec3> BHTreeNode3D::GetAxis()
{
	std::vector<glm::vec3> axis;
	axis.push_back(glm::vec3(min_x, min_y, min_z));
	axis.push_back(glm::vec3(max_x, max_y, max_z));

	for (int i = 0; i < 8; i++)
	{
		if (node[i] != NULL)
		{
			std::vector<glm::vec3> node_axis = node[i]->GetAxis();
			axis.insert(axis.end(), node_axis.begin(), node_axis.end());
		}
	}

	return axis;
}