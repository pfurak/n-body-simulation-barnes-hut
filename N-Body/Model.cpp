#include "Model.h"

Model::Model(const int& number_of_particles, const float& gravitational_constant, const float& theta, const float& lambda, bool is_3d)
{
    this->number_of_particles = number_of_particles;
    this->gravitational_constant = gravitational_constant;
    this->theta = theta;
    this->lambda = lambda;
    this->is_3d = is_3d;
    const int n = is_3d ? 3 : 2;

    positions = new float[number_of_particles * n];
    velocities = new float[number_of_particles * n];
    masses = new float[number_of_particles];

    particle_counter = 0;
    min_x = max_x = min_y = max_y = min_z = max_z = 0.0f; // blackhole coordinates
}

Model::~Model()
{
    if (positions != NULL)
    {
        delete[] positions;
        positions = NULL;
    }

    if (velocities != NULL)
    {
        delete[] velocities;
        velocities = NULL;
    }

    if (masses != NULL)
    {
        delete[] masses;
        masses = NULL;
    }
}

void Model::RegisterParticle(float x, float y, float z, float mass, float velocity_x, float velocity_y, float velocity_z)
{
    SetParticlePositions(particle_counter, x, y, z);
    SetParticleVelocities(particle_counter, velocity_x, velocity_y, velocity_z);

    masses[particle_counter] = mass;
    particle_counter++;
}

void Model::SetMinMaxCoordinates(float x, float y, float z)
{
    if(x < min_x) min_x = x;
    if(x > max_x) max_x = x;

    if(y < min_y) min_y = y;
    if(y > max_y) max_y = y;

    if(z < min_z) min_z = z;
    if(z > max_z) max_z = z;
}

float* Model::GetPositions()
{
    return positions;
}

float* Model::GetVelocities()
{
    return velocities;
}

float* Model::GetMasses()
{
    return masses;
}

void Model::CopyDataFromSolverState(float* state)
{
    if(is_3d)
    {
        const int dim = number_of_particles * 6;
		for (int i = 0; i < dim; i += 6)
		{
			const int particle_index = i / 6;
			SetParticlePositions(particle_index, state[i], state[i + 1], state[i + 2]);
			SetParticleVelocities(particle_index, state[i + 3], state[i + 4], state[i + 5]);
		}
    }
    else
    {
        const int dim = number_of_particles * 4;
		for (int i = 0; i < dim; i += 4)
		{
			const int particle_index = i / 4;
			SetParticlePositions(particle_index, state[i], state[i + 1]);
			SetParticleVelocities(particle_index, state[i + 2], state[i + 3]);
		}
    }
}

void Model::SetParticlePositions(const int& particle_index, float x, float y, float z)
{
    const int particle_start_index = particle_index * (is_3d ? 3 : 2);
    positions[particle_start_index] = x;
    positions[particle_start_index + 1] = y;
    if (is_3d)
    {
        positions[particle_start_index + 2] = z;
    }
    SetMinMaxCoordinates(x, y, z);
}

void Model::SetParticleVelocities(const int& particle_index, float x, float y, float z)
{
    const int particle_start_index = particle_index * (is_3d ? 3 : 2);
    velocities[particle_start_index] = x;
    velocities[particle_start_index + 1] = y;
    if (is_3d)
    {
        velocities[particle_start_index + 2] = z;
    }
}