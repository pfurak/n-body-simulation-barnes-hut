#include "Logger.h"

const std::string Logger::log_file = "error.log";

void Logger::LogError(const char* message)
{
	const std::time_t now = std::time(nullptr);
	std::string file = FileSystem::app_path + log_file;
	std::ofstream MyFile(file.c_str(), std::ios_base::app);
#ifdef _WIN32
	MyFile << std::put_time(std::localtime(&now), "%F %T") << "\t" << StringConverter::ISO_8859_2_TO_UTF_8(std::string(message)).c_str() << "\n";
#else
	MyFile << std::put_time(std::localtime(&now), "%F %T") << "\t" << message << "\n";
#endif
	
	MyFile.close();
}