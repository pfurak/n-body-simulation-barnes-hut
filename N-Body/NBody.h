#pragma once

// GLEW
#include <GL/glew.h>

// SDL
#include <SDL.h>
#include <SDL_opengl.h>

// GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform2.hpp>

#ifdef __APPLE__
#include <OpenGL/glu.h>
#elif __LINUX__
#include <GL/glu.h>
#include <sys/stat.h>
#else
#include <GL/GLU.h>
#endif

// imGUI
#include "imgui/imgui.h"

#include <iostream>
#include <fstream>
#include <sstream>

#if defined(__APPLE__) || defined(__LINUX__)
	#include <filesystem>
#endif

#ifdef __APPLE__
#include <mach-o/dyld.h>
#endif
#include <string>
#include <vector>

#include "SimulationController.h"
#include "BaseGPU.h"
#include "Logger.h"
#include "Camera.h"
#include "Orb.h"
#include "Generator.h"


class NBody
{
public:
	NBody();
	~NBody();

	bool Init();

	void Clean();
	void StartSimulation();
	void Update();
	void Render();

	void KeyboardDown(SDL_KeyboardEvent&, bool);
	void KeyboardUp(SDL_KeyboardEvent&, bool);
	void MouseMove(SDL_MouseMotionEvent&);
	void MouseDown(SDL_MouseButtonEvent&, bool);
	void MouseUp(SDL_MouseButtonEvent&);
	void MouseWheel(SDL_MouseWheelEvent&, bool);
	void Resize(int width, int height);
	void SetWindowDimension(int, int);
private:
	const int instanced_draw_max_size = 1000;								//	max size of offsets array in vertex shader
	const int max_number_of_particles = 1000000;							//	maximum number of particles
	const int min_number_of_particles = 100;								//	minimum number of particles
	const float body_radius = 0.1;											//	radius of a body
	const float blackhole_mass = 1000000.00f;								//	the maximum mass of a particle

	const float default_grav_constant = 6.67428e-11;
	const float parsec_to_meter = 3.08567758129e16;
	const float mass_of_sun = 1.988435e30;
	const float days_in_a_year = 365.25;
	const int seconds_in_a_day = 86400;

	int window_width;
	int window_height;
	int delta_time_speed;												//	simulation speed
	int number_of_particles;											//	number of particles
	int instanced_draw_particles;
	int instanced_draw_mod_particles;
	int particle_draw_multiplier;

	const float lambda = 0.01f;
	float theta = 0.9f;
	float gravitational_constant;										//	gravitational constant
	float min_x;														//	min x coordinate value
	float max_x;														//	max x coordinate value
	float min_y;														//	min y coordinate value
	float max_y;														//	max y coortinate value
	float min_z;														//	min z coordinate value
	float max_z;														//	max z coortinate value
	
	std::string bodies_input_text_buffer;								// number of particles string buffer (used by imGUI)
	std::string gravitational_constant_input_text_buffer;				// gravitational constant string buffer (used by imGUI)
	std::string theta_input_text_buffer;								// theta string buffer (used by imGUI)

	std::vector<const char*> randomizations;
	int current_randomization_index;
	int new_randomization_index;

	int current_algorithm_index;
	int new_algorithm_index;

	std::vector<const char*> velocities_logic;
	int current_velocity_logic_index;
	int new_velocity_logic_index;
	int current_device_index;
	int new_device_index;
	bool center_of_mass_logic;
	bool use_solver;
	bool render_GPU_information;
	bool render_imgui;
	bool render_axis;
	bool paused;

	SimulationController* simulation_controller;

	Camera camera;														//	camera object
	Generator generator;
	Orb orb;															//	body object
	Vertex* vertex;														//	vertex array
	GLushort* buffer;													//	buffer array
	int vertex_size;													//	vertex size
	int buffer_size;													//	buffer size
	

	void InitGLProgram();
	void GeneratePoints();												//	calls when simulation start
	void SetMinMaxCoordinates(float, float, float);						//	calls when simulation start
	void RegisterParticle(float, float, float, float, float, float, float);
	void RenderParticles();												//	calls in every render
	void RenderImGui();													//	calls in every render
	void RenderAxis(glm::mat4);
	void SetDefaultSettings();
	void StopSimulation();
	void ReStartSimulation();
	void PrintScreen();
	void InvertSurfaceVertically(SDL_Surface*);
protected:
	//	OpenGL
	//	necessary variables for shaders
	GLuint m_programID; 		//	shader program ID
	GLuint sphere_vao; 			//	vertex array object ID
	GLuint sphere_vbo; 			//	vertex buffer object ID
	GLuint sphere_buffer;  		//	index buffer object ID

	//	transformation matrixes
	glm::mat4 m_matWorld;
	glm::mat4 m_matView;
	glm::mat4 m_matProj;

	// location of matrices in shaders
	GLuint m_loc_mvp;			//	location of positions MVP array in shader (m_matWorld * m_matView * m_matProj)
	GLuint m_loc_offset;		//	location of positions offset array in shader

	GLuint axis_program;
	GLuint axis_mvp;
	GLuint axis_min_coordinates;		//	location of positions offset array in shader
	GLuint axis_max_coordinates;		//	location of positions offset array in shader
};
