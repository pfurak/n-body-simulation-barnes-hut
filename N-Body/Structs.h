#ifndef STRUCTS_H
#define STRUCTS_H

#pragma once

struct Vertex
{
	glm::vec3 p;
	glm::vec3 c;
};

struct Particle
{
public:
	float x;
	float y;
	float z;
	float mass;

	Particle() {}

	Particle(float x, float y, float mass)
	{
		this->x = x;
		this->y = y;
		this->mass = mass;
	}

	Particle(float x, float y, float z, float mass)
	{
		this->x = x;
		this->y = y;
		this->z = z;
		this->mass = mass;
	}
};

struct Algorithm
{
public:
	const char* name;
	bool is_3d;
	bool is_gpu;

	Algorithm(const char* name, bool is_3d, bool is_gpu)
	{
		this->name = name;
		this->is_3d = is_3d;
		this->is_gpu = is_gpu;
	}
};

#endif