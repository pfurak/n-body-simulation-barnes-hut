# OpenCL Barnes Hut algorithms only tested on Intel integrated graphics card

# Build on Windows (tested on Win10)

Download VS from this [link](https://visualstudio.microsoft.com/downloads/) and install that

Run the following command in cmd:

_subst t: path_

Open the projekt with N-Body.sln

Use ALT + ENTER to open the Properties window

The project must be configured to look for OpenGL and OpenCL data on the T drive

- Configuration Properties
    - Debugging
        - Environment: **PATH=T:\OGLPack\bin\x86;%PATH%**
    - VC++ Directories
        - Include Directories **T:\OGLPack\include;T:\OCLPack\include;$(IncludePath)**
        - Library Directories: **T:\OGLPack\lib\x86;T:\OCLPack\lib\Win32;$(LibraryPath)**
    - C/C++
        - All Options
            - Additional Include Directories: **T:\OCLPack\include\;T:\OGLPack\include;\%(AdditionalIncludeDirectories)**
            - Preprocessor Definitions: **WIN32;WINDOWS_IGNORE_PACKING_MISMATCH;_DEBUG;_CONSOLE;%(PreprocessorDefinitions)**
            - Program Database File Name: **\$(TEMP)vc\$(PlatformToolsetVersion).pdb**
    - Linker
        - All Options
            - Additional Dependencies: **OpenCL.lib;SDL2.lib;SDL2main.lib;SDL2_image.lib;glew32.lib;opengl32.lib;%(AdditionalDependencies)**
            - Additional Library Directories: **T:\OCLPack\lib\\$(Platform);T:\OGLPack\lib\x86;\%(AdditionalLibraryDirectories)**
            - Generate Program Database File: **\$(TEMP)\$(TargetName).pdb**
            - SubSystem: **Windows (/SUBSYSTEM:WINDOWS)**

The project should be in Debug mode x86

---
# Build on MAC (tested on macOS Big Sur v11.5.1)
Install the following packages:

_brew install sdl2_

_brew install sdl2_image_

_brew install glew_

_cd  ./N-Body_

_make -f makefile_macos_

---
# Build on Linux (tested on Ubuntu 20.04)

Install the following packages:

_apt-get install zip_

_apt-get install libglm-dev_

_apt-get install libsdl2-dev_

_apt-get install libsdl2-image-dev_

_apt-get install libglew-dev_

_apt-get install intel-opencl-icd_

_ln -s /usr/lib/x86_64-linux-gnu/libOpenCL.so.1 /usr/lib/libOpenCL.so_

or

_ln -s /usr/lib/x86_64-linux-gnu/libOpenCL.so.1.0.0 /usr/lib/libOpenCL.so_

Install the graphics card ([nVidia](https://linuxconfig.org/how-to-install-the-nvidia-drivers-on-ubuntu-20-04-focal-fossa-linux/))

Run the following command to find the devices:

_ubuntu-drivers devices_

Find the recommended device and install that. eg.:

_apt-get install nvidia-driver-470_

_cd  ./N-Body_

_make -f makefile_linux_